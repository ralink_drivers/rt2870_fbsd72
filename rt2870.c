
/*-
 * Copyright (c) 2009-2010 Alexander Egorenkov <egorenar@gmail.com>
 * Copyright (c) 2009 Damien Bergamini <damien.bergamini@free.fr>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "rt2870_softc.h"
#include "rt2870_reg.h"
#include "rt2870_eeprom.h"
#include "rt2870_ucode.h"
#include "rt2870_rxwi.h"
#include "rt2870_rxinfo.h"
#include "rt2870_txwi.h"
#include "rt2870_txinfo.h"
#include "rt2870_read_eeprom.h"
#include "rt2870_io.h"
#include "rt2870_rf.h"
#include "rt2870_led.h"
#include "rt2870_debug.h"

/*
 * Defines and macros
 */

#define USB_PRODUCT_LINKSYS4_WUSB600N			0x0071
#define USB_PRODUCT_DLINK2_DWA140				0x3c09
#define USB_PRODUCT_DLINK2_DWA160AREVB			0x3c11
#define USB_PRODUCT_ASUS_RT2770F				0x1742
#define USB_PRODUCT_RALINK_RT2770				0x2770

#define RT2870_USB_CONFIG_NO					1
#define RT2870_USB_IFACE_INDEX					0

#define RT2870_USB_REQ_MAC_READ_MULTI			0x07

/* packet length + Rx wireless info + Rx info */
#define RT2870_RX_DESC_SIZE						\
	(sizeof(uint32_t) + sizeof(struct rt2870_rxwi) + sizeof(struct rt2870_rxinfo))

/* Tx info + Tx wireless info + max padding */
#define RT2870_TX_DESC_SIZE						\
	(sizeof(struct rt2870_txinfo) + sizeof(struct rt2870_txwi) + 11)

#define RT2870_MAX_AGG_SIZE						3840

#define RT2870_USB_RX_BULK_BUFLEN				(2048 * 12)

#define RT2870_NOISE_FLOOR						-95

#define RT2870_RATE_IS_OFDM(rate)				((rate) >= 12 && (rate) != 22)

#define RT2870_ACK_SIZE							14

#define IEEE80211_HAS_ADDR4(wh)					\
	(((wh)->i_fc[1] & IEEE80211_FC1_DIR_MASK) == IEEE80211_FC1_DIR_DSTODS)

#define	RT2870_MS(_v, _f)						(((_v) & _f) >> _f##_S)
#define	RT2870_SM(_v, _f)						(((_v) << _f##_S) & _f)

#define RT2870_USB_XFER_TIMEOUT					5000

#define RT2870_TX_WATCHDOG_TIMEOUT				5

#define RT2870_WCID_RESERVED					0xff
#define RT2870_WCID_MCAST						0xf7

/*
 * Data structures and types
 */

struct rt2870_cmd_argv_newstate
{
	enum ieee80211_state nstate;
	int arg;
};

struct rt2870_cmd_argv_nodecleanup
{
	uint8_t staid;
};

struct rt2870_cmd_argv_newassoc
{
	int isnew;
	uint8_t macaddr[IEEE80211_ADDR_LEN];
};

struct rt2870_cmd_argv_updatebeacon
{
	int what;
};

struct rt2870_cmd_argv_keyset
{
	enum ieee80211_opmode opmode;
	struct ieee80211_key key;
	uint8_t staid;
};

struct rt2870_cmd_argv_keydelete
{
	enum ieee80211_opmode opmode;
	struct ieee80211_key key;
};

/*
 * Static function prototypes
 */

static int rt2870_find_usb_endpoints(struct rt2870_softc *sc);

static void rt2870_init_channels(struct rt2870_softc *sc);

static void rt2870_init_channels_ht40(struct rt2870_softc *sc);

static void rt2870_init_locked(void *priv);

static void rt2870_init(void *priv);

static int rt2870_init_bbp(struct rt2870_softc *sc);

static void rt2870_stop_locked(void *priv);

static void rt2870_stop(void *priv);

static void rt2870_start(struct ifnet *ifp);

static int rt2870_ioctl(struct ifnet *ifp, u_long cmd, caddr_t data);

static int rt2870_reset(struct ifnet *ifp);

static int rt2870_newstate(struct ieee80211com *ic,
	enum ieee80211_state nstate, int arg);

static void rt2870_scan_start(struct ieee80211com *ic);

static void rt2870_scan_end(struct ieee80211com *ic);

static void rt2870_set_channel(struct ieee80211com *ic);

static void rt2870_newassoc(struct ieee80211_node *ni, int isnew);

static void rt2870_updateslot(struct ifnet *ifp);

static int rt2870_wme_update(struct ieee80211com *ic);

static void rt2870_update_beacon(struct ieee80211com *ic, int what);

static void rt2870_key_update_begin(struct ieee80211com *ic);

static void rt2870_key_update_end(struct ieee80211com *ic);

static int rt2870_key_set(struct ieee80211com *ic,
	const struct ieee80211_key *k, const uint8_t mac[IEEE80211_ADDR_LEN]);

static int rt2870_key_delete(struct ieee80211com *ic,
	const struct ieee80211_key *k);

static int rt2870_raw_xmit(struct ieee80211_node *ni, struct mbuf *m,
	const struct ieee80211_bpf_params *params);

static int rt2870_media_change(struct ifnet *ifp);

static struct ieee80211_node *rt2870_node_alloc(struct ieee80211_node_table *nt);

static void rt2870_node_cleanup(struct ieee80211_node *ni);

static void rt2870_recv_action(struct ieee80211_node *ni,
	const uint8_t *frm, const uint8_t *efrm);

static int rt2870_send_action(struct ieee80211_node *ni,
	int category, int action, uint16_t args[4]);

static int rt2870_addba_response(struct ieee80211_node *ni,
	struct ieee80211_tx_ampdu *tap,
	int status, int baparamset, int batimeout);

static void rt2870_addba_stop(struct ieee80211_node *ni,
	struct ieee80211_tx_ampdu *tap);

static int rt2870_send_bar(struct ieee80211_node *ni,
	struct ieee80211_tx_ampdu *tap, ieee80211_seq seqno);

static void rt2870_amrr_update_iter_func(void *arg, struct ieee80211_node *ni);

static void rt2870_periodic(void *arg);

static void rt2870_tx_watchdog(void *arg);

static int rt2870_staid_alloc(struct rt2870_softc *sc, int aid);

static void rt2870_staid_delete(struct rt2870_softc *sc, int staid);

static int rt2870_do_async(struct rt2870_softc *sc,
	void (*cb)(struct rt2870_softc *sc, void *priv),
	void *arg, int len);

static void rt2870_newstate_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_node_cleanup_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_scan_start_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_scan_end_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_set_channel_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_newassoc_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_updateslot_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_update_beacon_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_wme_update_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_key_update_begin_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_key_update_end_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_key_set_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_key_delete_cb(struct rt2870_softc *sc, void *arg);

static void rt2870_asic_set_bssid(struct rt2870_softc *sc,
	const uint8_t *bssid);

static void rt2870_asic_set_macaddr(struct rt2870_softc *sc,
	const uint8_t *addr);

static void rt2870_asic_enable_tsf_sync(struct rt2870_softc *sc);

static void rt2870_asic_disable_tsf_sync(struct rt2870_softc *sc);

static void rt2870_asic_enable_mrr(struct rt2870_softc *sc);

static void rt2870_asic_set_txpreamble(struct rt2870_softc *sc);

static void rt2870_asic_set_basicrates(struct rt2870_softc *sc);

static void rt2870_asic_update_rtsthreshold(struct rt2870_softc *sc);

static void rt2870_asic_update_txpower(struct rt2870_softc *sc);

static void rt2870_asic_update_promisc(struct rt2870_softc *sc);

static void rt2870_asic_updateprot(struct rt2870_softc *sc);

static void rt2870_asic_updateslot(struct rt2870_softc *sc);

static void rt2870_asic_wme_update(struct rt2870_softc *sc);

static void rt2870_asic_update_beacon(struct rt2870_softc *sc);

static void rt2870_asic_clear_keytables(struct rt2870_softc *sc);

static void rt2870_asic_add_ba_session(struct rt2870_softc *sc,
	uint8_t wcid, int tid);

static void rt2870_asic_del_ba_session(struct rt2870_softc *sc,
	uint8_t wcid, int tid);

static int rt2870_beacon_alloc(struct rt2870_softc *sc);

static uint8_t rt2870_rxrate(struct rt2870_rxwi *rxwi);

static uint8_t rt2870_maxrssi_rxpath(struct rt2870_softc *sc,
	const struct rt2870_rxwi *rxwi);

static int8_t rt2870_rssi2dbm(struct rt2870_softc *sc,
	uint8_t rssi, uint8_t rxpath);

static uint8_t rt2870_rate2mcs(uint8_t rate);

static int rt2870_ackrate(struct ieee80211com *ic, int rate);

static uint16_t rt2870_txtime(int len, int rate, uint32_t flags);

static void rt2870_rx_frame(struct rt2870_softc *sc,
	uint8_t *buf, uint32_t dmalen);

static int rt2870_tx_frame(struct rt2870_softc *sc,
	struct mbuf *m, struct ieee80211_node *ni, int qid);

static int rt2870_tx_raw(struct rt2870_softc *sc,
	struct mbuf *m, struct ieee80211_node *ni,
	const struct ieee80211_bpf_params *params);

static void rt2870_rx_intr(usbd_xfer_handle xfer,
	usbd_private_handle priv, usbd_status status);

static void rt2870_tx_intr(usbd_xfer_handle xfer,
	usbd_private_handle priv, usbd_status status);

static void rt2870_rx_done_task(void *context, int pending);

static void rt2870_tx_done_task(void *context, int pending);

static void rt2870_periodic_task(void *context, int pending);

static void rt2870_cmd_task(void *context, int pending);

static int rt2870_rx_eof(struct rt2870_softc *sc, int limit);

static void rt2870_tx_eof(struct rt2870_softc *sc,
	struct rt2870_softc_tx_ring *ring);

static void rt2870_update_stats(struct rt2870_softc *sc);

static void rt2870_bbp_tuning(struct rt2870_softc *sc);

static void rt2870_watchdog(struct rt2870_softc *sc);

static void rt2870_drain_fifo_stats(struct rt2870_softc *sc);

static void rt2870_update_raw_counters(struct rt2870_softc *sc);

static int rt2870_alloc_rx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_rx_ring *ring);

static void rt2870_reset_rx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_rx_ring *ring);

static void rt2870_free_rx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_rx_ring *ring);

static int rt2870_alloc_tx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_tx_ring *ring, int qid);

static void rt2870_reset_tx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_tx_ring *ring);

static void rt2870_free_tx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_tx_ring *ring);

static void rt2870_reset_cmd_ring(struct rt2870_softc *sc,
	struct rt2870_softc_cmd_ring *ring);

static void rt2870_sysctl_attach(struct rt2870_softc *sc);

/*
 * Static variables
 */

static const struct usb_devno rt2870_usb_devno[] =
{
	{ USB_VENDOR_LINKSYS4, USB_PRODUCT_LINKSYS4_WUSB600N },		/* Linksys WUSB600N */
	{ USB_VENDOR_DLINK2, USB_PRODUCT_DLINK2_DWA140 },			/* D-Link DWA-140 */
	{ USB_VENDOR_DLINK2, USB_PRODUCT_DLINK2_DWA160AREVB },		/* D-Link DWA-160A Rev. B */
	{ USB_VENDOR_ASUS, USB_PRODUCT_ASUS_RT2770F },				/* Asus RT2770F */
	{ USB_VENDOR_RALINK, USB_PRODUCT_RALINK_RT2770 },			/* Ralink RT2770 */
};

static const struct
{
	uint32_t reg;
	uint32_t val;
} rt2870_def_mac[] =
{
	{ RT2870_REG_PBF_BCN_OFFSET0, 			0xf8f0e8e0 },
	{ RT2870_REG_PBF_BCN_OFFSET1, 			0x6f77d0c8 },
	{ RT2870_REG_LEGACY_BASIC_RATE, 		0x0000013f },
	{ RT2870_REG_HT_BASIC_RATE, 			0x00008003 },
	{ RT2870_REG_SYS_CTRL, 					0x00000000 },
	{ RT2870_REG_RX_FILTER_CFG, 			0x00017f97 },
	{ RT2870_REG_BKOFF_SLOT_CFG, 			0x00000209 },
	{ RT2870_REG_TX_SW_CFG0, 				0x00000000 },
	{ RT2870_REG_TX_SW_CFG1, 				0x00080606 },
	{ RT2870_REG_TX_LINK_CFG, 				0x00001020 },
	{ RT2870_REG_TX_TIMEOUT_CFG, 			0x000a2090 },
	{ RT2870_REG_MAX_LEN_CFG, 				(1 << 12) | RT2870_MAX_AGG_SIZE },
	{ RT2870_REG_LED_CFG, 					0x7f031e46 },
	{ RT2870_REG_PBF_MAX_PCNT, 				0x1f3fbf9f },
	{ RT2870_REG_TX_RTY_CFG, 				0x47d01f0f },
	{ RT2870_REG_AUTO_RSP_CFG, 				0x00000013 },
	{ RT2870_REG_TX_CCK_PROT_CFG, 			0x05740003 },
	{ RT2870_REG_TX_OFDM_PROT_CFG, 			0x05740003 },
	{ RT2870_REG_PBF_CFG, 					0x00f40006 },
	{ RT2870_REG_TX_MM40_PROT_CFG, 			0x03f44084 },
	{ RT2870_REG_SCHDMA_WPDMA_GLO_CFG, 		0x00000030 },
	{ RT2870_REG_TX_GF20_PROT_CFG, 			0x01744004 },
	{ RT2870_REG_TX_GF40_PROT_CFG, 			0x03f44084 },
	{ RT2870_REG_TX_MM20_PROT_CFG, 			0x01744004 },
	{ RT2870_REG_TX_TXOP_CTRL_CFG, 			0x0000583f },
	{ RT2870_REG_TX_RTS_CFG, 				0x00092b20 },
	{ RT2870_REG_TX_EXP_ACK_TIME, 			0x002400ca },
	{ RT2870_REG_HCCAPSMP_TXOP_HLDR_ET, 	0x00000002 },
	{ RT2870_REG_XIFS_TIME_CFG, 			0x33a41010 },
	{ RT2870_REG_PWR_PIN_CFG, 				0x00000003 },
	{ RT2870_REG_SCHDMA_WMM_AIFSN_CFG,		0x00002273 },
	{ RT2870_REG_SCHDMA_WMM_CWMIN_CFG,		0x00002344 },
	{ RT2870_REG_SCHDMA_WMM_CWMAX_CFG,		0x000034aa },
};

#define RT2870_DEF_MAC_SIZE		(sizeof(rt2870_def_mac) / sizeof(rt2870_def_mac[0]))

static const struct
{
	uint8_t	reg;
	uint8_t	val;
} rt2870_def_bbp[] =
{
	{ 65,	0x2c },
	{ 66,	0x38 },
	{ 69,	0x12 },
	{ 70,	0x0a },
	{ 73,	0x10 },
	{ 81,	0x37 },
	{ 82,	0x62 },
	{ 83,	0x6a },
	{ 84,	0x99 },
	{ 86,	0x00 },
	{ 91,	0x04 },
	{ 92,	0x00 },
	{ 103,	0x00 },
	{ 105,	0x05 },
};

#define RT2870_DEF_BBP_SIZE		(sizeof(rt2870_def_bbp) / sizeof(rt2870_def_bbp[0]))

SYSCTL_NODE(_hw, OID_AUTO, rt2870, CTLFLAG_RD, 0, "RT2870 driver parameters");

static int rt2870_tx_stbc = 1;
SYSCTL_INT(_hw_rt2870, OID_AUTO, tx_stbc, CTLFLAG_RW, &rt2870_tx_stbc, 0, "RT2870 Tx STBC");
TUNABLE_INT("hw.rt2870.tx_stbc", &rt2870_tx_stbc);

#ifdef RT2870_DEBUG
static int rt2870_debug = 0;
SYSCTL_INT(_hw_rt2870, OID_AUTO, debug, CTLFLAG_RW, &rt2870_debug, 0, "rt2870 debug level");
TUNABLE_INT("hw.rt2870.debug", &rt2870_debug);
#endif

/*
 * rt2870_probe
 */
static int rt2870_probe(device_t dev)
{
	struct usb_attach_arg *uaa;

	uaa = device_get_ivars(dev);

	if (uaa->iface != NULL)
		return UMATCH_NONE;

	return (usb_lookup(rt2870_usb_devno, uaa->vendor, uaa->product) != NULL) ?
		UMATCH_VENDOR_PRODUCT : UMATCH_NONE;
}

/*
 * rt2870_attach
 */
static int rt2870_attach(device_t dev)
{
	struct rt2870_softc *sc;
	struct usb_attach_arg *uaa;
	usbd_status usb_error;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	int error, ntries, i;

	sc = device_get_softc(dev);
	uaa = device_get_ivars(dev);

	sc->dev = dev;
	sc->usb_dev = uaa->device;

	sc->tx_stbc = rt2870_tx_stbc;

#ifdef RT2870_DEBUG
	sc->debug = rt2870_debug;

	SYSCTL_ADD_INT(device_get_sysctl_ctx(dev),
		SYSCTL_CHILDREN(device_get_sysctl_tree(dev)), OID_AUTO,
		"debug", CTLFLAG_RW, &sc->debug, 0, "rt2870 debug level");
#endif

	RT2870_DPRINTF(sc, RT2870_DEBUG_ANY,
		"%s: attaching\n",
		device_get_nameunit(sc->dev));

	if (usbd_set_config_no(sc->usb_dev, RT2870_USB_CONFIG_NO, 0) != 0)
	{
		printf("%s: could not set USB configuration no\n",
			device_get_nameunit(sc->dev));
		return ENXIO;
	}

	usb_error = usbd_device2interface_handle(sc->usb_dev,
		RT2870_USB_IFACE_INDEX, &sc->usb_iface);
	if (usb_error != 0)
	{
		printf("%s: could not get USB interface handle\n",
			device_get_nameunit(sc->dev));
		return ENXIO;
	}

	error = rt2870_find_usb_endpoints(sc);
	if (error != 0)
		return error;

	for (ntries = 0; ntries < 100; ntries++)
	{
		sc->mac_rev = rt2870_io_mac_read(sc, RT2870_REG_MAC_CSR0);
		if (sc->mac_rev != 0x00000000 && sc->mac_rev != 0xffffffff)
			break;

		DELAY(10);
	}

	if (ntries == 100)
	{
		printf("%s: timeout waiting for NIC to initialize\n",
			device_get_nameunit(sc->dev));
		return ENXIO;
	}

	rt2870_read_eeprom(sc);

	printf("%s: MAC/BBP RT2870 (rev 0x%08x), RF %s\n",
	    device_get_nameunit(sc->dev), sc->mac_rev,
		rt2870_rf_name(sc->rf_rev));

	sc->flags |= RT2870_SOFTC_FLAGS_VALID;

	/* clear key tables */

	rt2870_asic_clear_keytables(sc);

	/* open Tx and Rx USB bulk pipes */

	for (i = 0; i < sc->usb_endpoints - 1; i++)
	{
		error = usbd_open_pipe(sc->usb_iface, sc->tx_ring[i].usb_ep,
			USBD_EXCLUSIVE_USE, &sc->tx_ring[i].usb_pipe);
		if (error != 0)
		{
			printf("%s: could not open Tx pipe #%d: %s\n",
				device_get_nameunit(sc->dev), i, usbd_errstr(error));
			goto fail;
		}
	}

	error = usbd_open_pipe(sc->usb_iface, sc->rx_ring.usb_ep,
		USBD_EXCLUSIVE_USE, &sc->rx_ring.usb_pipe);
	if (error != 0)
	{
		printf("%s: could not open Rx pipe: %s\n",
			device_get_nameunit(sc->dev), usbd_errstr(error));
		goto fail;
	}

	/* allocate Tx and Rx rings */

	for (i = 0; i < sc->usb_endpoints - 1; i++)
	{
		error = rt2870_alloc_tx_ring(sc, &sc->tx_ring[i], i);
		if (error != 0)
		{
			printf("%s: could not allocate Tx ring #%d\n",
				device_get_nameunit(sc->dev), i);
			goto fail;
		}
	}

	if (sc->usb_endpoints == (RT2870_SOFTC_TX_RING_COUNT + 1))
		sc->tx_ring_mgtqid = 5;
	else
		sc->tx_ring_mgtqid = 0;

	error = rt2870_alloc_rx_ring(sc, &sc->rx_ring);
	if (error != 0)
	{
		printf("%s: could not allocate Rx ring\n",
			device_get_nameunit(sc->dev));
		goto fail;
	}

	rt2870_reset_cmd_ring(sc, &sc->cmd_ring);

	callout_init(&sc->periodic_ch, 0);
	callout_init(&sc->tx_watchdog_ch, 0);

	ifp = sc->ifp = if_alloc(IFT_ETHER);
	if (ifp == NULL)
	{
		printf("%s: could not if_alloc()\n",
			device_get_nameunit(sc->dev));
		error = ENOMEM;
		goto fail;
	}

	ifp->if_softc = sc;

	if_initname(ifp, "rt2870", device_get_unit(sc->dev));

	ifp->if_flags = IFF_BROADCAST | IFF_SIMPLEX | IFF_MULTICAST |
		IFF_NEEDSGIANT;

	ifp->if_init = rt2870_init;
	ifp->if_ioctl = rt2870_ioctl;
	ifp->if_start = rt2870_start;

	IFQ_SET_MAXLEN(&ifp->if_snd, IFQ_MAXLEN);
	ifp->if_snd.ifq_drv_maxlen = IFQ_MAXLEN;
	IFQ_SET_READY(&ifp->if_snd);

	ic = &sc->ic;

	ic->ic_ifp = ifp;

	ic->ic_phytype = IEEE80211_T_HT;
	ic->ic_opmode = IEEE80211_M_STA;
	ic->ic_state = IEEE80211_S_INIT;

	ic->ic_caps = IEEE80211_C_MONITOR |
		IEEE80211_C_IBSS |
		IEEE80211_C_AHDEMO |
		IEEE80211_C_HOSTAP |
		IEEE80211_C_WDS |
		IEEE80211_C_BGSCAN |
		IEEE80211_C_TXPMGT |
	    IEEE80211_C_SHPREAMBLE |
	    IEEE80211_C_SHSLOT |
		IEEE80211_C_TXFRAG |
		IEEE80211_C_BURST |
		IEEE80211_C_WME |
		IEEE80211_C_WEP |
		IEEE80211_C_TKIP |
		IEEE80211_C_AES_CCM |
		IEEE80211_C_WPA;

	ic->ic_htcaps = IEEE80211_HTC_HT |
		IEEE80211_HTC_AMSDU |					/* A-MSDU Tx */
		IEEE80211_HTC_AMPDU |					/* A-MPDU Tx */
		IEEE80211_HTCAP_MAXAMSDU_3839 |			/* max. A-MSDU Rx length */
		IEEE80211_HTCAP_CHWIDTH40 |				/* HT 40MHz channel width */
		IEEE80211_HTCAP_GREENFIELD |			/* HT greenfield */
		IEEE80211_HTCAP_SHORTGI20 |				/* HT 20MHz short GI */
		IEEE80211_HTCAP_SHORTGI40 |				/* HT 40MHz short GI */
		IEEE80211_HTCAP_SMPS_OFF; 				/* MIMO power save disabled */

	/* spatial streams */

	if (sc->nrxpath == 2)
		ic->ic_htcaps |= IEEE80211_HTCAP_RXSTBC_2STREAM;
	else if (sc->nrxpath == 3)
		ic->ic_htcaps |= IEEE80211_HTCAP_RXSTBC_3STREAM;
	else
		ic->ic_htcaps |= IEEE80211_HTCAP_RXSTBC_1STREAM;

	if (sc->ntxpath > 1)
		ic->ic_htcaps |= IEEE80211_HTCAP_TXSTBC;

	/* delayed BA */

	if (sc->mac_rev != 0x28600100)
		ic->ic_htcaps |= IEEE80211_HTCAP_DELBA;

	/* init channels */

	ic->ic_nchans = 0;
	ic->ic_regdomain = 0;
	ic->ic_countrycode = CTRY_DEFAULT;
	ic->ic_location = 0;

	rt2870_init_channels(sc);

	rt2870_init_channels_ht40(sc);

	IEEE80211_ADDR_COPY(ic->ic_myaddr, sc->mac_addr);

	ieee80211_ifattach(ic);

	sc->newstate = ic->ic_newstate;
	ic->ic_newstate = rt2870_newstate;

	ic->ic_reset = rt2870_reset;
	ic->ic_node_alloc = rt2870_node_alloc;

	sc->node_cleanup = ic->ic_node_cleanup;
	ic->ic_node_cleanup = rt2870_node_cleanup;

	ic->ic_scan_start = rt2870_scan_start;
	ic->ic_scan_end = rt2870_scan_end;
	ic->ic_set_channel = rt2870_set_channel;
	ic->ic_newassoc = rt2870_newassoc;
	ic->ic_updateslot = rt2870_updateslot;
	ic->ic_wme.wme_update = rt2870_wme_update;
	ic->ic_update_beacon = rt2870_update_beacon;
	ic->ic_crypto.cs_key_update_begin = rt2870_key_update_begin;
	ic->ic_crypto.cs_key_update_end = rt2870_key_update_end;
	ic->ic_crypto.cs_key_set = rt2870_key_set;
	ic->ic_crypto.cs_key_delete = rt2870_key_delete;
	ic->ic_raw_xmit = rt2870_raw_xmit;

	sc->recv_action = ic->ic_recv_action;
	ic->ic_recv_action = rt2870_recv_action;

	sc->send_action = ic->ic_send_action;
	ic->ic_send_action = rt2870_send_action;

	sc->addba_response = ic->ic_addba_response;
	ic->ic_addba_response = rt2870_addba_response;

	sc->addba_stop = ic->ic_addba_stop;
	ic->ic_addba_stop = rt2870_addba_stop;

	/* overwrite Rx A-MPDU factor */

	ic->ic_ampdu_rxmax = IEEE80211_HTCAP_MAXRXAMPDU_32K;
	ic->ic_ampdu_density = IEEE80211_HTCAP_MPDUDENSITY_NA;
	ic->ic_ampdu_limit = ic->ic_ampdu_rxmax;

	/* hardware requires padding between 802.11 frame header and body */

	ic->ic_flags |= IEEE80211_F_WME | IEEE80211_F_DATAPAD | IEEE80211_F_DOTH;

	ic->ic_flags_ext |= IEEE80211_FEXT_SWBMISS;

	ic->ic_max_aid = RT2870_SOFTC_STAID_COUNT;

	rt2870_amrr_init(&sc->amrr, ic,
		sc->ntxpath,
		RT2870_AMRR_MIN_SUCCESS_THRESHOLD,
		RT2870_AMRR_MAX_SUCCESS_THRESHOLD,
		500);

	bpfattach2(ifp, DLT_IEEE802_11_RADIO,
	    sizeof(struct ieee80211_frame) + IEEE80211_RADIOTAP_HDRLEN, 
	    &sc->drvbpf);

	sc->rxtap_len = sizeof(sc->rxtapu);
	sc->rxtap.ihdr.it_len = htole16(sc->rxtap_len);
	sc->rxtap.ihdr.it_present = htole32(RT2870_SOFTC_RX_RADIOTAP_PRESENT);

	sc->txtap_len = sizeof(sc->txtapu);
	sc->txtap.ihdr.it_len = htole16(sc->txtap_len);
	sc->txtap.ihdr.it_present = htole32(RT2870_SOFTC_TX_RADIOTAP_PRESENT);

	/* init task queue */

	TASK_INIT(&sc->rx_done_task, 0, rt2870_rx_done_task, sc);
	TASK_INIT(&sc->tx_done_task, 0, rt2870_tx_done_task, sc);
	TASK_INIT(&sc->periodic_task, 0, rt2870_periodic_task, sc);
	TASK_INIT(&sc->cmd_task, 0, rt2870_cmd_task, sc);

	sc->rx_process_limit = 100;

	sc->taskqueue = taskqueue_create("rt2870_taskq", M_NOWAIT,
	    taskqueue_thread_enqueue, &sc->taskqueue);

	taskqueue_start_threads(&sc->taskqueue, 1, PI_NET, "%s taskq",
	    device_get_nameunit(sc->dev));

	ieee80211_media_init(ic, rt2870_media_change, ieee80211_media_status);

	rt2870_sysctl_attach(sc);

	if (bootverbose)
		ieee80211_announce(ic);

	usbd_add_drv_event(USB_EVENT_DRIVER_ATTACH, sc->usb_dev, sc->dev);

	return 0;

fail:

	/* close Tx and Rx USB bulk pipes */

	if (sc->rx_ring.usb_pipe != NULL)
	{
		usbd_abort_pipe(sc->rx_ring.usb_pipe);
		usbd_close_pipe(sc->rx_ring.usb_pipe);
		sc->rx_ring.usb_pipe = NULL;
	}

	for (i = 0; i < sc->usb_endpoints - 1; i++)
	{
		if (sc->tx_ring[i].usb_pipe != NULL)
		{
			usbd_abort_pipe(sc->tx_ring[i].usb_pipe);
			usbd_close_pipe(sc->tx_ring[i].usb_pipe);
			sc->tx_ring[i].usb_pipe = NULL;
		}
	}

	/* free Tx and Rx rings */

	for (i = 0; i < sc->usb_endpoints - 1; i++)
		rt2870_free_tx_ring(sc, &sc->tx_ring[i]);
	
	rt2870_free_rx_ring(sc, &sc->rx_ring);

	return error;
}

/*
 * rt2870_detach
 */
static int rt2870_detach(device_t dev)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	int i;

	if (!device_is_attached(dev))
		return 0;

	sc = device_get_softc(dev);
	ic = &sc->ic;
	ifp = ic->ic_ifp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_ANY,
		"%s: detaching\n",
		device_get_nameunit(sc->dev));

	sc->flags &= ~RT2870_SOFTC_FLAGS_VALID;

	sc->tx_timer = 0;

	ifp->if_drv_flags &= ~(IFF_DRV_RUNNING | IFF_DRV_OACTIVE);

	ieee80211_new_state(ic, IEEE80211_S_INIT, -1);

	callout_stop(&sc->periodic_ch);
	callout_stop(&sc->tx_watchdog_ch);

	taskqueue_drain(sc->taskqueue, &sc->rx_done_task);
	taskqueue_drain(sc->taskqueue, &sc->tx_done_task);
	taskqueue_drain(sc->taskqueue, &sc->periodic_task);
	taskqueue_drain(sc->taskqueue, &sc->cmd_task);

	/* close Tx and Rx USB bulk pipes */

	if (sc->rx_ring.usb_pipe != NULL)
	{
		usbd_abort_pipe(sc->rx_ring.usb_pipe);
		usbd_close_pipe(sc->rx_ring.usb_pipe);
		sc->rx_ring.usb_pipe = NULL;
	}

	for (i = 0; i < sc->usb_endpoints - 1; i++)
	{
		if (sc->tx_ring[i].usb_pipe != NULL)
		{
			usbd_abort_pipe(sc->tx_ring[i].usb_pipe);
			usbd_close_pipe(sc->tx_ring[i].usb_pipe);
			sc->tx_ring[i].usb_pipe = NULL;
		}
	}

	/* free Tx and Rx rings */

	for (i = 0; i < sc->usb_endpoints - 1; i++)
		rt2870_free_tx_ring(sc, &sc->tx_ring[i]);
	
	rt2870_free_rx_ring(sc, &sc->rx_ring);

	bpfdetach(ifp);

	ieee80211_ifdetach(ic);

	if_free(ifp);

	taskqueue_free(sc->taskqueue);

	usbd_add_drv_event(USB_EVENT_DRIVER_DETACH, sc->usb_dev, sc->dev);

	return 0;
}

/*
 * rt2870_find_usb_endpoints
 */
static int rt2870_find_usb_endpoints(struct rt2870_softc *sc)
{
	usb_interface_descriptor_t *id;
	usb_endpoint_descriptor_t *ed;
	int i, j;

	id = usbd_get_interface_descriptor(sc->usb_iface);

	sc->usb_endpoints = id->bNumEndpoints;
	if ((sc->usb_endpoints != (RT2870_SOFTC_TX_RING_COUNT + 1)) &&
		(sc->usb_endpoints != (RT2870_SOFTC_TX_RING_COUNT - 2 + 1)))
	{
		printf("%s: wrong number of USB endpoints=%d\n",
			device_get_nameunit(sc->dev), sc->usb_endpoints);
		return ENXIO;
	}

	sc->rx_ring.usb_ep = -1;

	for (i = 0; i < RT2870_SOFTC_TX_RING_COUNT; i++)
		sc->tx_ring[i].usb_ep = -1;

	for (i = 0, j = 0; i < id->bNumEndpoints; i++)
	{
		ed = usbd_interface2endpoint_descriptor(sc->usb_iface, i);
		if (ed == NULL)
		{
			printf("%s: no endpoint descriptor #%d for USB interface\n",
				device_get_nameunit(sc->dev), i);
			return ENXIO;
		}

		if (UE_GET_DIR(ed->bEndpointAddress) == UE_DIR_IN &&
			UE_GET_XFERTYPE(ed->bmAttributes) == UE_BULK)
		{
			sc->rx_ring.usb_ep = ed->bEndpointAddress;
		}
		else if (UE_GET_DIR(ed->bEndpointAddress) == UE_DIR_OUT &&
			UE_GET_XFERTYPE(ed->bmAttributes) == UE_BULK)
		{
			if (j < RT2870_SOFTC_TX_RING_COUNT)
				sc->tx_ring[j++].usb_ep = ed->bEndpointAddress;
			else
				sc->tx_ring[RT2870_SOFTC_TX_RING_COUNT - 1].usb_ep =
					ed->bEndpointAddress;
		}
	}

	if ((sc->rx_ring.usb_ep == -1) ||
		((j != RT2870_SOFTC_TX_RING_COUNT) && (j != RT2870_SOFTC_TX_RING_COUNT - 2)))
	{
		printf("%s: missing USB endpoints\n",
			device_get_nameunit(sc->dev));
		return ENXIO;
	}

	return 0;
}

/*
 * rt2870_init_channels
 */
static void rt2870_init_channels(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	struct ieee80211_channel *c;
	int i, flags;

	ic = &sc->ic;

	/* set supported channels for 2GHz band */

	for (i = 1; i <= 14; i++)
	{
		c = &ic->ic_channels[ic->ic_nchans++];
		flags = IEEE80211_CHAN_B;

		c->ic_freq = ieee80211_ieee2mhz(i, flags);
		c->ic_ieee = i;
		c->ic_flags = flags;

		c = &ic->ic_channels[ic->ic_nchans++];
		flags = IEEE80211_CHAN_B | IEEE80211_CHAN_HT20;

		c->ic_freq = ieee80211_ieee2mhz(i, flags);
		c->ic_ieee = i;
		c->ic_flags = flags;

		c = &ic->ic_channels[ic->ic_nchans++];
		flags = IEEE80211_CHAN_G;

		c->ic_freq = ieee80211_ieee2mhz(i, flags);
		c->ic_ieee = i;
		c->ic_flags = flags;

		c = &ic->ic_channels[ic->ic_nchans++];
		flags = IEEE80211_CHAN_G | IEEE80211_CHAN_HT20;

		c->ic_freq = ieee80211_ieee2mhz(i, flags);
		c->ic_ieee = i;
		c->ic_flags = flags;
	}

	/* set supported channels for 5GHz band */

	if (sc->rf_rev == RT2870_EEPROM_RF_2850 ||
		sc->rf_rev == RT2870_EEPROM_RF_2750)
	{
		for (i = 36; i <= 64; i += 4)
		{
			c = &ic->ic_channels[ic->ic_nchans++];
			flags = IEEE80211_CHAN_A;

			c->ic_freq = ieee80211_ieee2mhz(i, flags);
			c->ic_ieee = i;
			c->ic_flags = flags;

			c = &ic->ic_channels[ic->ic_nchans++];
			flags = IEEE80211_CHAN_A | IEEE80211_CHAN_HT20;

			c->ic_freq = ieee80211_ieee2mhz(i, flags);
			c->ic_ieee = i;
			c->ic_flags = flags;
		}

		for (i = 100; i <= 140; i += 4)
		{
			c = &ic->ic_channels[ic->ic_nchans++];
			flags = IEEE80211_CHAN_A;

			c->ic_freq = ieee80211_ieee2mhz(i, flags);
			c->ic_ieee = i;
			c->ic_flags = flags;

			c = &ic->ic_channels[ic->ic_nchans++];
			flags = IEEE80211_CHAN_A | IEEE80211_CHAN_HT20;

			c->ic_freq = ieee80211_ieee2mhz(i, flags);
			c->ic_ieee = i;
			c->ic_flags = flags;
		}

		for (i = 149; i <= 165; i += 4)
		{
			c = &ic->ic_channels[ic->ic_nchans++];
			flags = IEEE80211_CHAN_A;

			c->ic_freq = ieee80211_ieee2mhz(i, flags);
			c->ic_ieee = i;
			c->ic_flags = flags;

			c = &ic->ic_channels[ic->ic_nchans++];
			flags = IEEE80211_CHAN_A | IEEE80211_CHAN_HT20;

			c->ic_freq = ieee80211_ieee2mhz(i, flags);
			c->ic_ieee = i;
			c->ic_flags = flags;
		}
	}
}

/*
 * rt2870_init_channels_ht40
 */
static void rt2870_init_channels_ht40(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	struct ieee80211_channel *c, *cent, *ext;
	int i, flags;

	ic = &sc->ic;

	/* set supported channels for 2GHz band */

	for (i = 1; i <= 14; i++)
	{
		flags = IEEE80211_CHAN_G | IEEE80211_CHAN_HT40;

		/* find the center channel */

		cent = ieee80211_find_channel_byieee(ic, i,
			flags & ~IEEE80211_CHAN_HT);
		if (cent == NULL)
		{
			printf("%s: skip channel %d, could not find center channel\n",
				device_get_nameunit(sc->dev), i);
			continue;
		}

		/* find the extension channel */

		ext = ieee80211_find_channel(ic, cent->ic_freq + 20,
			flags & ~IEEE80211_CHAN_HT);
		if (ext == NULL)
		{
			printf("%s: skip channel %d, could not find extension channel\n",
				device_get_nameunit(sc->dev), i);
			continue;
		}

		c = &ic->ic_channels[ic->ic_nchans++];

		*c = *cent;
		c->ic_extieee = ext->ic_ieee;
		c->ic_flags &= ~IEEE80211_CHAN_HT;
		c->ic_flags |= IEEE80211_CHAN_HT40U;

		c = &ic->ic_channels[ic->ic_nchans++];

		*c = *ext;
		c->ic_extieee = cent->ic_ieee;
		c->ic_flags &= ~IEEE80211_CHAN_HT;
		c->ic_flags |= IEEE80211_CHAN_HT40D;
	}

	/* set supported channels for 5GHz band */

	if (sc->rf_rev == RT2870_EEPROM_RF_2850 ||
		sc->rf_rev == RT2870_EEPROM_RF_2750)
	{
		for (i = 36; i <= 64; i += 4)
		{
			flags = IEEE80211_CHAN_A | IEEE80211_CHAN_HT40;

			/* find the center channel */

			cent = ieee80211_find_channel_byieee(ic, i,
				flags & ~IEEE80211_CHAN_HT);
			if (cent == NULL)
			{
				printf("%s: skip channel %d, could not find center channel\n",
					device_get_nameunit(sc->dev), i);
				continue;
			}

			/* find the extension channel */

			ext = ieee80211_find_channel(ic, cent->ic_freq + 20,
				flags & ~IEEE80211_CHAN_HT);
			if (ext == NULL)
			{
				printf("%s: skip channel %d, could not find extension channel\n",
					device_get_nameunit(sc->dev), i);
				continue;
			}

			c = &ic->ic_channels[ic->ic_nchans++];

			*c = *cent;
			c->ic_extieee = ext->ic_ieee;
			c->ic_flags &= ~IEEE80211_CHAN_HT;
			c->ic_flags |= IEEE80211_CHAN_HT40U;

			c = &ic->ic_channels[ic->ic_nchans++];

			*c = *ext;
			c->ic_extieee = cent->ic_ieee;
			c->ic_flags &= ~IEEE80211_CHAN_HT;
			c->ic_flags |= IEEE80211_CHAN_HT40D;
		}

		for (i = 100; i <= 140; i += 4)
		{
			flags = IEEE80211_CHAN_A | IEEE80211_CHAN_HT40;

			/* find the center channel */

			cent = ieee80211_find_channel_byieee(ic, i,
				flags & ~IEEE80211_CHAN_HT);
			if (cent == NULL)
			{
				printf("%s: skip channel %d, could not find center channel\n",
					device_get_nameunit(sc->dev), i);
				continue;
			}

			/* find the extension channel */

			ext = ieee80211_find_channel(ic, cent->ic_freq + 20,
				flags & ~IEEE80211_CHAN_HT);
			if (ext == NULL)
			{
				printf("%s: skip channel %d, could not find extension channel\n",
					device_get_nameunit(sc->dev), i);
				continue;
			}

			c = &ic->ic_channels[ic->ic_nchans++];

			*c = *cent;
			c->ic_extieee = ext->ic_ieee;
			c->ic_flags &= ~IEEE80211_CHAN_HT;
			c->ic_flags |= IEEE80211_CHAN_HT40U;

			c = &ic->ic_channels[ic->ic_nchans++];

			*c = *ext;
			c->ic_extieee = cent->ic_ieee;
			c->ic_flags &= ~IEEE80211_CHAN_HT;
			c->ic_flags |= IEEE80211_CHAN_HT40D;
		}

		for (i = 149; i <= 165; i += 4)
		{
			flags = IEEE80211_CHAN_A | IEEE80211_CHAN_HT40;

			/* find the center channel */

			cent = ieee80211_find_channel_byieee(ic, i,
				flags & ~IEEE80211_CHAN_HT);
			if (cent == NULL)
			{
				printf("%s: skip channel %d, could not find center channel\n",
					device_get_nameunit(sc->dev), i);
				continue;
			}

			/* find the extension channel */

			ext = ieee80211_find_channel(ic, cent->ic_freq + 20,
				flags & ~IEEE80211_CHAN_HT);
			if (ext == NULL)
			{
				printf("%s: skip channel %d, could not find extension channel\n",
					device_get_nameunit(sc->dev), i);
				continue;
			}

			c = &ic->ic_channels[ic->ic_nchans++];

			*c = *cent;
			c->ic_extieee = ext->ic_ieee;
			c->ic_flags &= ~IEEE80211_CHAN_HT;
			c->ic_flags |= IEEE80211_CHAN_HT40U;

			c = &ic->ic_channels[ic->ic_nchans++];

			*c = *ext;
			c->ic_extieee = cent->ic_ieee;
			c->ic_flags &= ~IEEE80211_CHAN_HT;
			c->ic_flags |= IEEE80211_CHAN_HT40D;
		}
	}
}

/*
 * rt2870_init_locked
 */
static void rt2870_init_locked(void *priv)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct rt2870_softc_rx_data *data;
	int ntries, error, i;
	uint32_t tmp, stacnt[6];

	sc = priv;
	ic = &sc->ic;
	ifp = ic->ic_ifp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_ANY,
		"%s: initializing\n",
		device_get_nameunit(sc->dev));

	if (!(sc->flags & RT2870_SOFTC_FLAGS_UCODE_LOADED))
	{
		RT2870_DPRINTF(sc, RT2870_DEBUG_ANY,
			"%s: loading 8051 microcode\n",
			device_get_nameunit(sc->dev));

		error = rt2870_io_mcu_load_ucode(sc, rt2870_ucode, sizeof(rt2870_ucode));
		if (error != 0)
		{
			printf("%s: could not load 8051 microcode\n",
				device_get_nameunit(sc->dev));
			goto fail;
		}

		RT2870_DPRINTF(sc, RT2870_DEBUG_ANY,
			"%s: 8051 microcode was successfully loaded\n",
			device_get_nameunit(sc->dev));

		sc->flags |= RT2870_SOFTC_FLAGS_UCODE_LOADED;
	}

	/* wait while DMA engine is busy */

	for (ntries = 0; ntries < 100; ntries++)
	{
		tmp = rt2870_io_mac_read(sc, RT2870_REG_SCHDMA_WPDMA_GLO_CFG);
		if (!(tmp & (RT2870_REG_TX_DMA_BUSY | RT2870_REG_RX_DMA_BUSY)))
			break;

		DELAY(1000);
	}

	if (ntries == 100)
	{
		printf("%s: timeout waiting for DMA engine\n",
			device_get_nameunit(sc->dev));
		goto fail;
	}

	tmp &= 0xff0;
	tmp |= RT2870_REG_TX_WB_DDONE;

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_WPDMA_GLO_CFG, tmp);

	/* PBF hardware reset */

	tmp = rt2870_io_mac_read(sc, RT2870_REG_PBF_SYS_CTRL);

	tmp &= ~(1 << 13);

	rt2870_io_mac_write(sc, RT2870_REG_PBF_SYS_CTRL, tmp);

	rt2870_io_mac_write(sc, RT2870_REG_SYS_CTRL,
		RT2870_REG_MAC_SRST | RT2870_REG_BBP_HRST);

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_USB_DMA_CFG, 0);

	rt2870_io_mcu_reset(sc);

	rt2870_io_mac_write(sc, RT2870_REG_SYS_CTRL, 0);

	/* init Tx power per rate */

	for (i = 0; i < RT2870_SOFTC_TXPOW_RATE_COUNT; i++)
	{
		if (sc->txpow_rate_20mhz[i] == 0xffffffff)
			continue;

		rt2870_io_mac_write(sc, RT2870_REG_TX_PWR_CFG(i),
			sc->txpow_rate_20mhz[i]);
	}

	for (i = 0; i < RT2870_DEF_MAC_SIZE; i++)
		rt2870_io_mac_write(sc, rt2870_def_mac[i].reg,
			rt2870_def_mac[i].val);

	/* wait while MAC is busy */

	for (ntries = 0; ntries < 100; ntries++)
	{
		if (!(rt2870_io_mac_read(sc, RT2870_REG_STATUS_CFG) &
			(RT2870_REG_STATUS_TX_BUSY | RT2870_REG_STATUS_RX_BUSY)))
			break;

		DELAY(1000);
	}

	if (ntries == 100)
	{
		printf("%s: timeout waiting for MAC\n",
			device_get_nameunit(sc->dev));
		goto fail;
	}

	/* clear Host to MCU mailbox */

	rt2870_io_mac_write(sc, RT2870_REG_H2M_MAILBOX_BBP_AGENT, 0);
	rt2870_io_mac_write(sc, RT2870_REG_H2M_MAILBOX, 0);

	DELAY(1000);

	error = rt2870_init_bbp(sc);
	if (error != 0)
		goto fail;

	/* set up maximum buffer sizes */

	tmp = (1 << 12) | RT2870_MAX_AGG_SIZE;

	if (sc->mac_rev >= 0x28720200 && sc->mac_rev < 0x30700200)
	{
		tmp &= 0xfff;
		tmp |= 0x2000;
	}

	rt2870_io_mac_write(sc, RT2870_REG_MAX_LEN_CFG, tmp);

	/* set mac address */

	IEEE80211_ADDR_COPY(ic->ic_myaddr, IF_LLADDR(ifp));

	rt2870_asic_set_macaddr(sc, ic->ic_myaddr);

	/* clear statistic registers */

	rt2870_io_mac_read_multi(sc, RT2870_REG_RX_STA_CNT0,
		stacnt, sizeof(stacnt));

	/* send LEDs operating mode to microcontroller */

	rt2870_io_mcu_cmd(sc, RT2870_IO_MCU_CMD_LED1,
		RT2870_REG_H2M_TOKEN_NO_INTR, sc->led_off[0]);
	rt2870_io_mcu_cmd(sc, RT2870_IO_MCU_CMD_LED2,
		RT2870_REG_H2M_TOKEN_NO_INTR, sc->led_off[1]);
	rt2870_io_mcu_cmd(sc, RT2870_IO_MCU_CMD_LED3,
		RT2870_REG_H2M_TOKEN_NO_INTR, sc->led_off[2]);

	/* write vendor-specific BBP values (from EEPROM) */

	for (i = 0; i < RT2870_SOFTC_BBP_EEPROM_COUNT; i++)
	{
		if (sc->bbp_eeprom[i].reg == 0x00 ||
			sc->bbp_eeprom[i].reg == 0xff)
			continue;

		rt2870_io_bbp_write(sc, sc->bbp_eeprom[i].reg,
			sc->bbp_eeprom[i].val);
	}

	/* disable non-existing Rx chains */

	tmp = rt2870_io_bbp_read(sc, 3);

	tmp &= ~((1 << 4) | (1 << 3));

	if (sc->nrxpath == 3)
		tmp |= (1 << 4);
	else if (sc->nrxpath == 2)
		tmp |= (1 << 3);

	rt2870_io_bbp_write(sc, 3, tmp);

	/* disable non-existing Tx chains */

	tmp = rt2870_io_bbp_read(sc, 1);

	tmp &= ~((1 << 4) | (1 << 3));

	if (sc->ntxpath == 2)
		tmp |= (1 << 4);

	rt2870_io_bbp_write(sc, 1, tmp);

	/* set current channel */

	rt2870_rf_set_chan(sc, ic->ic_curchan);

	/* turn radio LED on */

	rt2870_led_cmd(sc, RT2870_LED_CMD_RADIO_ON);

	rt2870_io_mcu_cmd(sc, RT2870_IO_MCU_CMD_BOOT,
		RT2870_REG_H2M_TOKEN_NO_INTR, 0);

	/* set RTS threshold */

	rt2870_asic_update_rtsthreshold(sc);

	/* set Tx power */

	rt2870_asic_update_txpower(sc);

	/* set up protection mode */

	sc->tx_ampdu_sessions = 0;

	rt2870_asic_updateprot(sc);

	/* clear beacon frame space (entries = 8, entry size = 512) */

	rt2870_io_mac_set_region_4(sc, RT2870_REG_BEACON_BASE(0), 0, 1024);

	/* enable Tx/Rx DMA engine */

	tmp = rt2870_io_mac_read(sc, RT2870_REG_SCHDMA_USB_CYC_CFG);

	tmp &= 0xffffff00;
	tmp |= 0x1e;

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_USB_CYC_CFG, tmp);

	if ((sc->mac_rev & 0xffff) != 0x0101)
		rt2870_io_mac_write(sc, RT2870_REG_TX_TXOP_CTRL_CFG, 0x583f);

	rt2870_io_mac_write(sc, RT2870_REG_SYS_CTRL, RT2870_REG_TX_ENABLE);

	for (ntries = 0; ntries < 200; ntries++)
	{
		tmp = rt2870_io_mac_read(sc, RT2870_REG_SCHDMA_WPDMA_GLO_CFG);
		if (!(tmp & (RT2870_REG_TX_DMA_BUSY | RT2870_REG_RX_DMA_BUSY)))
			break;

		DELAY(1000);
	}

	if (ntries == 200)
	{
		printf("%s: timeout waiting for DMA engine\n",
			device_get_nameunit(sc->dev));
		goto fail;
	}

	DELAY(50);

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_WMM_TXOP0_CFG, 0);
	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_WMM_TXOP1_CFG,
		(48 << 16) | 96);

	tmp |= RT2870_REG_TX_WB_DDONE |
		RT2870_REG_RX_DMA_ENABLE |
		RT2870_REG_TX_DMA_ENABLE;

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_WPDMA_GLO_CFG, tmp);

	tmp = RT2870_REG_USB_DMA_TX_ENABLE |
		RT2870_REG_USB_DMA_RX_ENABLE |
		RT2870_REG_USB_DMA_RX_AGG_ENABLE |
		/* Rx agg limit in unit of 1024 byte */
		((RT2870_USB_RX_BULK_BUFLEN / 1024 - 3) << RT2870_REG_USB_DMA_RX_AGG_LIMIT_SHIFT) |
		/* Rx agg timeout in unit of 33ns */
		0x80;

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_USB_DMA_CFG, tmp);

	/* set Rx filter */

	tmp = RT2870_REG_RX_FILTER_DROP_CRC_ERR |
		RT2870_REG_RX_FILTER_DROP_PHY_ERR;

	if (ic->ic_opmode != IEEE80211_M_MONITOR)
	{
		tmp |= RT2870_REG_RX_FILTER_DROP_DUPL |
			RT2870_REG_RX_FILTER_DROP_CTS |
			RT2870_REG_RX_FILTER_DROP_BA |
			RT2870_REG_RX_FILTER_DROP_ACK |
			RT2870_REG_RX_FILTER_DROP_VER_ERR |
			RT2870_REG_RX_FILTER_DROP_CTRL_RSV |
			RT2870_REG_RX_FILTER_DROP_CFACK |
			RT2870_REG_RX_FILTER_DROP_CFEND;

		if (ic->ic_opmode == IEEE80211_M_STA)
			tmp |= RT2870_REG_RX_FILTER_DROP_RTS |
				RT2870_REG_RX_FILTER_DROP_PSPOLL;

		if (!(ifp->if_flags & IFF_PROMISC))
			tmp |= RT2870_REG_RX_FILTER_DROP_UC_NOME;
	}

	rt2870_io_mac_write(sc, RT2870_REG_RX_FILTER_CFG, tmp);

	rt2870_io_mac_write(sc, RT2870_REG_SYS_CTRL,
		RT2870_REG_RX_ENABLE | RT2870_REG_TX_ENABLE);

	/* clear garbage interrupts */

	tmp = rt2870_io_mac_read(sc, 0x1300);

	taskqueue_unblock(sc->taskqueue);

	/* init Tx and Rx rings */

	for(i = 0; i < sc->usb_endpoints - 1; i++)
		rt2870_reset_tx_ring(sc, &sc->tx_ring[i]);

	rt2870_reset_rx_ring(sc, &sc->rx_ring);

	/* start up the receive pipe */

	for (i = 0; i < RT2870_SOFTC_RX_RING_DATA_COUNT; i++)
	{
		data = &sc->rx_ring.data[i];

		STAILQ_INSERT_TAIL(&sc->rx_ring.active, data, next);

		usbd_setup_xfer(data->xfer, sc->rx_ring.usb_pipe, sc, data->buf,
			RT2870_USB_RX_BULK_BUFLEN, USBD_SHORT_XFER_OK | USBD_NO_COPY,
			USBD_NO_TIMEOUT, rt2870_rx_intr);

		usbd_transfer(data->xfer);
	}

	ifp->if_drv_flags &= ~IFF_DRV_OACTIVE;
	ifp->if_drv_flags |= IFF_DRV_RUNNING;

	if (ic->ic_opmode != IEEE80211_M_MONITOR)
	{
		if (ic->ic_roaming != IEEE80211_ROAMING_MANUAL)
			ieee80211_new_state(ic, IEEE80211_S_SCAN, -1);
	}
	else
	{
		ieee80211_new_state(ic, IEEE80211_S_RUN, -1);
	}

	sc->periodic_round = 0;

	callout_reset(&sc->periodic_ch, hz / 10, rt2870_periodic, sc);

	return;

fail:

	rt2870_stop_locked(sc);
}

/*
 * rt2870_init
 */
static void rt2870_init(void *priv)
{
	struct rt2870_softc *sc;

	sc = priv;

	rt2870_init_locked(sc);
}

/*
 * rt2870_init_bbp
 */
static int rt2870_init_bbp(struct rt2870_softc *sc)
{
	int ntries, i;
	uint8_t tmp;

	for (ntries = 0; ntries < 20; ntries++)
	{
		tmp = rt2870_io_bbp_read(sc, 0);
		if (tmp != 0x00 && tmp != 0xff)
			break;
	}

	if (tmp == 0x00 || tmp == 0xff)
	{
		printf("%s: timeout waiting for BBP to wakeup\n",
			device_get_nameunit(sc->dev));
		return ETIMEDOUT;
	}

	for (i = 0; i < RT2870_DEF_BBP_SIZE; i++)
		rt2870_io_bbp_write(sc, rt2870_def_bbp[i].reg,
			rt2870_def_bbp[i].val);

	if ((sc->mac_rev & 0xffff) != 0x0101)
		rt2870_io_bbp_write(sc, 84, 0x19);

	return 0;
}

/*
 * rt2870_stop
 */
static void rt2870_stop_locked(void *priv)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	uint32_t tmp;
	int i;

	sc = priv;
	ic = &sc->ic;
	ifp = ic->ic_ifp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_ANY,
		"%s: stopping\n",
		device_get_nameunit(sc->dev));

	sc->tx_timer = 0;

	ifp->if_drv_flags &= ~(IFF_DRV_RUNNING | IFF_DRV_OACTIVE);

	ieee80211_new_state(ic, IEEE80211_S_INIT, -1);

	callout_stop(&sc->periodic_ch);
	callout_stop(&sc->tx_watchdog_ch);

	taskqueue_block(sc->taskqueue);

	taskqueue_drain(sc->taskqueue, &sc->rx_done_task);
	taskqueue_drain(sc->taskqueue, &sc->tx_done_task);
	taskqueue_drain(sc->taskqueue, &sc->periodic_task);
	taskqueue_drain(sc->taskqueue, &sc->cmd_task);

	/* clear key tables */

	rt2870_asic_clear_keytables(sc);

	/* disable Tx/Rx */

	tmp = rt2870_io_mac_read(sc, RT2870_REG_SYS_CTRL);

	tmp &= ~(RT2870_REG_RX_ENABLE | RT2870_REG_TX_ENABLE);

	rt2870_io_mac_write(sc, RT2870_REG_SYS_CTRL, tmp);

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_USB_DMA_CFG, 0);

	/* reset adapter */

	rt2870_io_mac_write(sc, RT2870_REG_SYS_CTRL,
		RT2870_REG_MAC_SRST | RT2870_REG_BBP_HRST);
	rt2870_io_mac_write(sc, RT2870_REG_SYS_CTRL, 0);

	/* abort any pending transfers */

	if (sc->rx_ring.usb_pipe != NULL)
		usbd_abort_pipe(sc->rx_ring.usb_pipe);

	for (i = 0; i < sc->usb_endpoints - 1; i++)
		if (sc->tx_ring[i].usb_pipe != NULL)
			usbd_abort_pipe(sc->tx_ring[i].usb_pipe);

	if (sc->beacon_mbuf != NULL)
	{
		m_free(sc->beacon_mbuf);
		sc->beacon_mbuf = NULL;
	}

	rt2870_reset_cmd_ring(sc, &sc->cmd_ring);
}

/*
 * rt2870_stop
 */
static void rt2870_stop(void *priv)
{
	struct rt2870_softc *sc;

	sc = priv;

	rt2870_stop_locked(sc);
}

/*
 * rt2870_start
 */
static void rt2870_start(struct ifnet *ifp)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ieee80211_node *ni;
	struct ether_header *eh;
	struct mbuf *m;
	int qid;

	sc = ifp->if_softc;
	ic = &sc->ic;

	if (!(ifp->if_drv_flags & IFF_DRV_RUNNING))
		return;

	for (;;)
	{
		IF_POLL(&ic->ic_mgtq, m);
		if (m != NULL)
		{
			if (sc->tx_ring[sc->tx_ring_mgtqid].queued >= RT2870_SOFTC_TX_RING_DATA_COUNT)
			{
				RT2870_DPRINTF(sc, RT2870_DEBUG_TX,
					"%s: if_start: Tx ring with qid=%d is full\n",
					device_get_nameunit(sc->dev), sc->tx_ring_mgtqid);

				ifp->if_drv_flags |= IFF_DRV_OACTIVE;

				sc->tx_data_queue_full[sc->tx_ring_mgtqid]++;

				break;
			}

			IF_DEQUEUE(&ic->ic_mgtq, m);
			if (m == NULL)
				break;

			ni = (struct ieee80211_node *) m->m_pkthdr.rcvif;

			m->m_pkthdr.rcvif = NULL;

			if (bpf_peers_present(ic->ic_rawbpf))
				bpf_mtap(ic->ic_rawbpf, m);

			if (rt2870_tx_frame(sc, m, ni, sc->tx_ring_mgtqid) != 0)
				break;
		}
		else
		{
			if (ic->ic_state != IEEE80211_S_RUN)
				break;

			IFQ_DRV_DEQUEUE(&ifp->if_snd, m);
			if (m == NULL)
				break;

			if (ic->ic_flags & IEEE80211_F_SCAN)
				ieee80211_cancel_scan(ic);

			if (m->m_len < sizeof(struct ether_header) &&
				!(m = m_pullup(m, sizeof (struct ether_header))))
				continue;

			eh = mtod(m, struct ether_header *);

			ni = ieee80211_find_txnode(ic, eh->ether_dhost);
			if (ni == NULL)
			{
				RT2870_DPRINTF(sc, RT2870_DEBUG_TX,
					"%s: if_start: could not find Tx node\n",
					device_get_nameunit(sc->dev));

				m_freem(m);

				continue;
			}

			ieee80211_classify(ic, m, ni);

			qid = M_WME_GETAC(m);

			if (sc->tx_ring[qid].queued >= RT2870_SOFTC_TX_RING_DATA_COUNT)
			{
				RT2870_DPRINTF(sc, RT2870_DEBUG_TX,
					"%s: if_start: Tx ring with qid=%d is full\n",
					device_get_nameunit(sc->dev), qid);

				m_freem(m);
				ieee80211_free_node(ni);

				ifp->if_drv_flags |= IFF_DRV_OACTIVE;
				ifp->if_oerrors++;

				sc->tx_data_queue_full[qid]++;

				break;
			}

			BPF_MTAP(ifp, m);

			m = ieee80211_encap(ic, m, ni);
			if (m == NULL)
			{
				ieee80211_free_node(ni);

				ifp->if_oerrors++;

				continue;
			}

			if (bpf_peers_present(ic->ic_rawbpf))
				bpf_mtap(ic->ic_rawbpf, m);

			if (rt2870_tx_frame(sc, m, ni, qid) != 0)
			{
				ieee80211_free_node(ni);

				ifp->if_drv_flags |= IFF_DRV_OACTIVE;
				ifp->if_oerrors++;

				break;
			}
		}

		sc->tx_timer = RT2870_TX_WATCHDOG_TIMEOUT;

		ic->ic_lastdata = ticks;

		callout_reset(&sc->tx_watchdog_ch, hz, rt2870_tx_watchdog, sc);
	}
}

/*
 * rt2870_ioctl
 */
static int rt2870_ioctl(struct ifnet *ifp, u_long cmd, caddr_t data)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	int error;

	sc = ifp->if_softc;
	ic = &sc->ic;

	error = 0;

	switch (cmd)
	{
		case SIOCSIFFLAGS:
			if (ifp->if_flags & IFF_UP)
			{
				if (ifp->if_drv_flags & IFF_DRV_RUNNING)
				{
					if ((ifp->if_flags ^ sc->if_flags) & IFF_PROMISC)
						rt2870_asic_update_promisc(sc);
				}
				else
				{
					rt2870_init_locked(sc);
				}
			}
			else
			{
				if (ifp->if_drv_flags & IFF_DRV_RUNNING)
					rt2870_stop_locked(sc);
			}

			sc->if_flags = ifp->if_flags;
		break;

		default:
			error = ieee80211_ioctl(ic, cmd, data);
	}

	if (error == ENETRESET)
	{
		if ((ifp->if_flags & IFF_UP) &&
			(ifp->if_drv_flags & IFF_DRV_RUNNING) &&
			(ic->ic_roaming != IEEE80211_ROAMING_MANUAL))
		{
			rt2870_stop_locked(sc);
			rt2870_init_locked(sc);
		}

		error = 0;
	}

	return error;
}

/*
 * rt2870_reset
 */
static int rt2870_reset(struct ifnet *ifp)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;

	sc = ifp->if_softc;
	ic = &sc->ic;

	if (ic->ic_opmode != IEEE80211_M_MONITOR)
		return ENETRESET;

	rt2870_rf_set_chan(sc, ic->ic_curchan);

	return 0;
}

/*
 * rt2870_newstate
 */
static int rt2870_newstate(struct ieee80211com *ic,
	enum ieee80211_state nstate, int arg)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;
	struct rt2870_cmd_argv_newstate cmd_argv;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	cmd_argv.nstate = nstate;
	cmd_argv.arg = arg;

	rt2870_do_async(sc, rt2870_newstate_cb, &cmd_argv,
		sizeof(struct rt2870_cmd_argv_newstate));

	return 0;
}

/*
 * rt2870_scan_start
 */
static void rt2870_scan_start(struct ieee80211com *ic)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	rt2870_do_async(sc, rt2870_scan_start_cb, NULL, 0);
}

/*
 * rt2870_scan_end
 */
static void rt2870_scan_end(struct ieee80211com *ic)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	rt2870_do_async(sc, rt2870_scan_end_cb, NULL, 0);
}

/*
 * rt2870_set_channel
 */
static void rt2870_set_channel(struct ieee80211com *ic)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	rt2870_do_async(sc, rt2870_set_channel_cb, NULL, 0);
}

/*
 * rt2870_newassoc
 */
static void rt2870_newassoc(struct ieee80211_node *ni, int isnew)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct rt2870_cmd_argv_newassoc cmd_argv;

	ic = ni->ni_ic;
	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	cmd_argv.isnew = isnew;
	memcpy(cmd_argv.macaddr, ni->ni_macaddr, IEEE80211_ADDR_LEN);

	rt2870_do_async(sc, rt2870_newassoc_cb, &cmd_argv,
		sizeof(struct rt2870_cmd_argv_newassoc));
}

/*
 * rt2870_updateslot
 */
static void rt2870_updateslot(struct ifnet *ifp)
{
	struct rt2870_softc *sc;

	sc = ifp->if_softc;

	rt2870_do_async(sc, rt2870_updateslot_cb, NULL, 0);
}

/*
 * rt2870_wme_update
 */
static int rt2870_wme_update(struct ieee80211com *ic)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	rt2870_do_async(sc, rt2870_wme_update_cb, NULL, 0);

	return 0;
}

/*
 * rt2870_update_beacon
 */
static void rt2870_update_beacon(struct ieee80211com *ic, int what)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;
	struct rt2870_cmd_argv_updatebeacon cmd_argv;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	cmd_argv.what = what;

	rt2870_do_async(sc, rt2870_update_beacon_cb, &cmd_argv,
		sizeof(struct rt2870_cmd_argv_updatebeacon));
}

/*
 * rt2870_key_update_begin
 */
static void rt2870_key_update_begin(struct ieee80211com *ic)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	rt2870_do_async(sc, rt2870_key_update_begin_cb, NULL, 0);
}

/*
 * rt2870_key_update_end
 */
static void rt2870_key_update_end(struct ieee80211com *ic)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	rt2870_do_async(sc, rt2870_key_update_end_cb, NULL, 0);
}

/*
 * rt2870_key_set
 */
static int rt2870_key_set(struct ieee80211com *ic,
	const struct ieee80211_key *k, const uint8_t mac[IEEE80211_ADDR_LEN])
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;
	struct ieee80211_node *ni;
	struct rt2870_softc_node *rni;
	struct rt2870_cmd_argv_keyset cmd_argv;

	if (k->wk_cipher->ic_cipher != IEEE80211_CIPHER_WEP &&
		k->wk_cipher->ic_cipher != IEEE80211_CIPHER_TKIP &&
		k->wk_cipher->ic_cipher != IEEE80211_CIPHER_AES_CCM)
		return EINVAL;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	cmd_argv.opmode = ic->ic_opmode;

	memcpy(&cmd_argv.key, k, sizeof(struct ieee80211_key));

	if (!(k->wk_flags & IEEE80211_KEY_GROUP))
	{
		ni = ieee80211_find_node(&ic->ic_sta, mac);
		rni = (struct rt2870_softc_node *) ni;

		cmd_argv.staid = rni->staid;

		ieee80211_free_node(ni);
	}

	rt2870_do_async(sc, rt2870_key_set_cb, &cmd_argv,
		sizeof(struct rt2870_cmd_argv_keyset));

	return 1;
}

/*
 * rt2870_key_delete
 */
static int rt2870_key_delete(struct ieee80211com *ic,
	const struct ieee80211_key *k)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;
	struct rt2870_cmd_argv_keydelete cmd_argv;

	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	cmd_argv.opmode = ic->ic_opmode;

	memcpy(&cmd_argv.key, k, sizeof(struct ieee80211_key));

	rt2870_do_async(sc, rt2870_key_delete_cb, &cmd_argv,
		sizeof(struct rt2870_cmd_argv_keydelete));

	return 1;
}

/*
 * rt2870_raw_xmit
 */
static int rt2870_raw_xmit(struct ieee80211_node *ni, struct mbuf *m,
	const struct ieee80211_bpf_params *params)
{
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct rt2870_softc *sc;

	ic = ni->ni_ic;
	ifp = ic->ic_ifp;
	sc = ifp->if_softc;

	return 0;
}

/*
 * rt2870_media_change
 */
static int rt2870_media_change(struct ifnet *ifp)
{
	struct rt2870_softc *sc;
	int error;

	sc = ifp->if_softc;

	error = ieee80211_media_change(ifp);
	if (error != ENETRESET)
		return error;

	if ((ifp->if_flags & IFF_UP) && (ifp->if_drv_flags & IFF_DRV_RUNNING))
	{
		rt2870_stop(sc);
		rt2870_init(sc);
	}

	return 0;
}

/*
 * rt2870_node_alloc
 */
static struct ieee80211_node *rt2870_node_alloc(struct ieee80211_node_table *nt)
{
	return malloc(sizeof(struct rt2870_softc_node),
		M_80211_NODE, M_NOWAIT | M_ZERO);
}

/*
 * rt2870_node_cleanup
 */
static void rt2870_node_cleanup(struct ieee80211_node *ni)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct ieee80211_rx_ampdu *rx_ampdu;
	struct rt2870_softc_node *rni;
	struct rt2870_cmd_argv_nodecleanup cmd_argv;
	int i;

	ic = ni->ni_ic;
	ifp = ic->ic_ifp;
	sc = ifp->if_softc;
	rni = (struct rt2870_softc_node *) ni;

	RT2870_DPRINTF(sc, RT2870_DEBUG_NODE,
		"%s: node cleanup: macaddr=%s, associd=0x%04x, staid=0x%02x\n",
		device_get_nameunit(sc->dev), ether_sprintf(ni->ni_macaddr),
		ni->ni_associd, rni->staid);

	if (rni->staid != 0)
	{
		cmd_argv.staid = rni->staid;

		rni->staid = 0;

		for (i = 0; i < WME_NUM_TID; i++)
		{
			rx_ampdu = &ni->ni_rx_ampdu[i];

			if (rx_ampdu->rxa_flags & IEEE80211_AGGR_XCHGPEND)
			{
				if (sc->rx_ampdu_sessions > 0)
					sc->rx_ampdu_sessions--;
				else
					printf("%s: number of A-MPDU Rx sessions cannot be negative\n",
						device_get_nameunit(sc->dev));
			}
		}

		rt2870_do_async(sc, rt2870_node_cleanup_cb,
			&cmd_argv, sizeof(struct rt2870_cmd_argv_nodecleanup));
	}

	sc->node_cleanup(ni);
}

/*
 * rt2870_recv_action
 */
static void rt2870_recv_action(struct ieee80211_node *ni,
	const uint8_t *frm, const uint8_t *efrm)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct ieee80211_rx_ampdu *rx_ampdu;
	struct rt2870_softc_node *rni;
	const struct ieee80211_action *ia;
	uint16_t baparamset;
	int tid;
	uint8_t wcid;

	ic = ni->ni_ic;
	ifp = ic->ic_ifp;
	sc = ifp->if_softc;
	rni = (struct rt2870_softc_node *) ni;

	ia = (const struct ieee80211_action *) frm;

	if (ia->ia_category == IEEE80211_ACTION_CAT_BA)
	{
		switch (ia->ia_action)
		{
			/* IEEE80211_ACTION_BA_DELBA */
			case IEEE80211_ACTION_BA_DELBA:
				baparamset = LE_READ_2(frm + 2);
				tid = RT2870_MS(baparamset, IEEE80211_BAPS_TID);
				rx_ampdu = &ni->ni_rx_ampdu[tid];
				wcid = rni->staid;

				RT2870_DPRINTF(sc, RT2870_DEBUG_BA,
					"%s: received DELBA request: associd=0x%04x, staid=0x%02x, tid=%d\n",
					device_get_nameunit(sc->dev), ni->ni_associd, rni->staid, tid);

				if (rni->staid != 0)
				{
					rt2870_asic_del_ba_session(sc, wcid, tid);

					if (rx_ampdu->rxa_flags & IEEE80211_AGGR_XCHGPEND)
					{
						if (sc->rx_ampdu_sessions > 0)
							sc->rx_ampdu_sessions--;
						else
							printf("%s: number of A-MPDU Rx sessions cannot be negative\n",
								device_get_nameunit(sc->dev));
					}
				}
			break;
		}
	}

	sc->recv_action(ni, frm, efrm);
}

/*
 * rt2870_send_action
 */
static int rt2870_send_action(struct ieee80211_node *ni,
	int category, int action, uint16_t args[4])
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct ieee80211_rx_ampdu *rx_ampdu;
	struct rt2870_softc_node *rni;
	uint16_t status, baparamset;
	uint8_t wcid;
	int tid, bufsize;

	ic = ni->ni_ic;
	ifp = ic->ic_ifp;
	sc = ifp->if_softc;
	rni = (struct rt2870_softc_node *) ni;

	wcid = rni->staid;

	if (category == IEEE80211_ACTION_CAT_BA)
	{
		switch (action)
		{
			/* IEEE80211_ACTION_BA_ADDBA_RESPONSE */
			case IEEE80211_ACTION_BA_ADDBA_RESPONSE:
				status = args[1];
				baparamset = args[2];
				tid = RT2870_MS(baparamset, IEEE80211_BAPS_TID);
				bufsize = RT2870_MS(baparamset, IEEE80211_BAPS_BUFSIZ);
				rx_ampdu = &ni->ni_rx_ampdu[tid];

				RT2870_DPRINTF(sc, RT2870_DEBUG_BA,
					"%s: sending ADDBA response: associd=0x%04x, staid=0x%02x, status=%d, tid=%d, bufsize=%d\n",
					device_get_nameunit(sc->dev), ni->ni_associd, rni->staid, status, tid, bufsize);

				if (status == IEEE80211_STATUS_SUCCESS)
				{
					rt2870_asic_add_ba_session(sc, wcid, tid);

					sc->rx_ampdu_sessions++;
				}
			break;

			/* IEEE80211_ACTION_BA_DELBA */
			case IEEE80211_ACTION_BA_DELBA:
				baparamset = RT2870_SM(args[0], IEEE80211_DELBAPS_TID) | args[1];
				tid = RT2870_MS(baparamset, IEEE80211_DELBAPS_TID);
				rx_ampdu = &ni->ni_rx_ampdu[tid];

				RT2870_DPRINTF(sc, RT2870_DEBUG_BA,
					"%s: sending DELBA request: associd=0x%04x, staid=0x%02x, tid=%d\n",
					device_get_nameunit(sc->dev), ni->ni_associd, rni->staid, tid);

				if (RT2870_MS(baparamset, IEEE80211_DELBAPS_INIT) != IEEE80211_DELBAPS_INIT)
				{
					rt2870_asic_del_ba_session(sc, wcid, tid);

					if (rx_ampdu->rxa_flags & IEEE80211_AGGR_XCHGPEND)
					{
						if (sc->rx_ampdu_sessions > 0)
							sc->rx_ampdu_sessions--;
						else
							printf("%s: number of A-MPDU Rx sessions cannot be negative\n",
								device_get_nameunit(sc->dev));
					}
				}
			break;
		}
	}

	return sc->send_action(ni, category, action, args);
}

/*
 * rt2870_addba_response
 */
static int rt2870_addba_response(struct ieee80211_node *ni,
	struct ieee80211_tx_ampdu *tap,
	int status, int baparamset, int batimeout)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct rt2870_softc_node *rni;
	ieee80211_seq seqno;
	int ret, tid, old_bufsize, new_bufsize;

	ic = ni->ni_ic;
	ifp = ic->ic_ifp;
	sc = ifp->if_softc;
	rni = (struct rt2870_softc_node *) ni;

	tid = RT2870_MS(baparamset, IEEE80211_BAPS_TID);
	old_bufsize = RT2870_MS(baparamset, IEEE80211_BAPS_BUFSIZ);
	new_bufsize = old_bufsize;

	if (status == IEEE80211_STATUS_SUCCESS)
	{
		if (sc->mac_rev >= 0x28830300)
		{
			if (sc->mac_rev >= 0x30700200)
				new_bufsize = 13;
			else
				new_bufsize = 31;
		}
		else if (sc->mac_rev >= 0x28720200)
		{
			new_bufsize = 13;
		}
		else
		{
			new_bufsize = 7;
		}

		if (old_bufsize > new_bufsize)
		{
			baparamset &= ~IEEE80211_BAPS_BUFSIZ;
			baparamset = RT2870_SM(new_bufsize, IEEE80211_BAPS_BUFSIZ);
		}

		if (!(tap->txa_flags & IEEE80211_AGGR_RUNNING))
		{
			sc->tx_ampdu_sessions++;

			if (sc->tx_ampdu_sessions == 1)
				rt2870_asic_updateprot(sc);
		}
	}

	RT2870_DPRINTF(sc, RT2870_DEBUG_BA,
		"%s: received ADDBA response: associd=0x%04x, staid=0x%02x, status=%d, tid=%d, "
		"old bufsize=%d, new bufsize=%d\n",
		device_get_nameunit(sc->dev), ni->ni_associd, rni->staid, status, tid,
		old_bufsize, new_bufsize);

	ret = sc->addba_response(ni, tap, status, baparamset, batimeout);

	if (status == IEEE80211_STATUS_SUCCESS)
	{
		seqno = ni->ni_txseqs[tid];

		rt2870_send_bar(ni, tap, seqno);
	}

	return ret;
}

/*
 * rt2870_addba_stop
 */
static void rt2870_addba_stop(struct ieee80211_node *ni,
	struct ieee80211_tx_ampdu *tap)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct rt2870_softc_node *rni;
	int tid;

	ic = ni->ni_ic;
	ifp = ic->ic_ifp;
	sc = ifp->if_softc;
	rni = (struct rt2870_softc_node *) ni;

	tid = WME_AC_TO_TID(tap->txa_ac);

	RT2870_DPRINTF(sc, RT2870_DEBUG_BA,
		"%s: stopping A-MPDU Tx: associd=0x%04x, staid=0x%02x, tid=%d\n",
		device_get_nameunit(sc->dev), ni->ni_associd, rni->staid, tid);

	if (tap->txa_flags & IEEE80211_AGGR_RUNNING)
	{
		if (sc->tx_ampdu_sessions > 0)
		{
			sc->tx_ampdu_sessions--;

			if (sc->tx_ampdu_sessions == 0)
				rt2870_asic_updateprot(sc);
		}
		else
		{
			printf("%s: number of A-MPDU Tx sessions cannot be negative\n",
				device_get_nameunit(sc->dev));
		}
	}

	sc->addba_stop(ni, tap);
}

/*
 * rt2870_send_bar
 */
static int rt2870_send_bar(struct ieee80211_node *ni,
	struct ieee80211_tx_ampdu *tap, ieee80211_seq seqno)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct ieee80211_frame_bar *bar;
	struct rt2870_softc_node *rni;
	struct mbuf *m;
	uint16_t barctl, barseqctl;
	uint8_t *frm;
	int tid;

	ic = ni->ni_ic;
	ifp = ic->ic_ifp;
	sc = ifp->if_softc;
	rni = (struct rt2870_softc_node *) ni;

	if (!(tap->txa_flags & IEEE80211_AGGR_RUNNING))
		return EINVAL;

	m = ieee80211_getmgtframe(&frm, ic->ic_headroom, sizeof(struct ieee80211_frame_bar));
	if (m == NULL)
		return ENOMEM;

	bar = mtod(m, struct ieee80211_frame_bar *);

	bar->i_fc[0] = IEEE80211_FC0_VERSION_0 | IEEE80211_FC0_TYPE_CTL | IEEE80211_FC0_SUBTYPE_BAR;
	bar->i_fc[1] = 0;

	IEEE80211_ADDR_COPY(bar->i_ra, ni->ni_macaddr);
	IEEE80211_ADDR_COPY(bar->i_ta, ic->ic_myaddr);

	tid = WME_AC_TO_TID(tap->txa_ac);

	barctl = (tap->txa_flags & IEEE80211_AGGR_IMMEDIATE ?  0 : IEEE80211_BAR_NOACK) |
		IEEE80211_BAR_COMP |
		RT2870_SM(tid, IEEE80211_BAR_TID);
	barseqctl = RT2870_SM(seqno, IEEE80211_BASEQ_START);

	bar->i_ctl = htole16(barctl);
	bar->i_seq = htole16(barseqctl);

	m->m_pkthdr.len = m->m_len = sizeof(struct ieee80211_frame_bar);
	m->m_pkthdr.rcvif = (void *) ni;

	tap->txa_start = seqno;

	ieee80211_ref_node(ni);

	RT2870_DPRINTF(sc, RT2870_DEBUG_BA,
		"%s: sending BAR: associd=0x%04x, staid=0x%02x, tid=%d, seqno=%d\n",
		device_get_nameunit(sc->dev), ni->ni_associd, rni->staid, tid, seqno);

	IF_ENQUEUE(&ic->ic_mgtq, m);

	if_start(ifp);

	return 0;
}

/*
 * rt2870_amrr_update_iter_func
 */
static void rt2870_amrr_update_iter_func(void *arg, struct ieee80211_node *ni)
{
	struct rt2870_softc *sc;
	struct ieee80211com *ic;
	struct rt2870_softc_node *rni;
	uint8_t wcid;

	sc = arg;
	ic = &sc->ic;
	rni = (struct rt2870_softc_node *) ni;

	/* only associated stations */

	if (rni->staid != 0)
	{
		wcid = rni->staid;

		RT2870_DPRINTF(sc, RT2870_DEBUG_RATE,
			"%s: AMRR node: staid=0x%02x, txcnt=%d, success=%d, retrycnt=%d\n",
			device_get_nameunit(sc->dev),
			rni->staid, sc->amrr_node[wcid].txcnt, sc->amrr_node[wcid].success, sc->amrr_node[wcid].retrycnt);

		rt2870_amrr_choose(ni, &sc->amrr_node[wcid]);

		RT2870_DPRINTF(sc, RT2870_DEBUG_RATE,
			"%s:%s node Tx rate: associd=0x%04x, staid=0x%02x, rate=0x%02x, max rate=0x%02x\n",
			device_get_nameunit(sc->dev),
			(ni->ni_flags & IEEE80211_NODE_HT) ? " HT" : "",
			ni->ni_associd, rni->staid,
			(ni->ni_flags & IEEE80211_NODE_HT) ?
				(ni->ni_htrates.rs_rates[ni->ni_txrate] | IEEE80211_RATE_MCS) :
				(ni->ni_rates.rs_rates[ni->ni_txrate] & IEEE80211_RATE_VAL),
			(ni->ni_flags & IEEE80211_NODE_HT) ?
				(ni->ni_htrates.rs_rates[ni->ni_htrates.rs_nrates - 1] | IEEE80211_RATE_MCS) :
				(ni->ni_rates.rs_rates[ni->ni_rates.rs_nrates - 1] & IEEE80211_RATE_VAL));
	}
}

/*
 * rt2870_periodic
 */
static void rt2870_periodic(void *arg)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;

	sc = arg;
	ifp = sc->ifp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_PERIODIC,
		"%s: periodic\n",
		device_get_nameunit(sc->dev));

	if (!(ifp->if_drv_flags & IFF_DRV_RUNNING))
		return;

	taskqueue_enqueue(sc->taskqueue, &sc->periodic_task);
}

/*
 * rt2870_tx_watchdog
 */
static void rt2870_tx_watchdog(void *arg)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;

	sc = arg;
	ifp = sc->ifp;

	if (sc->tx_timer == 0)
		return;

	if (--sc->tx_timer == 0)
	{
		printf("%s: Tx watchdog timeout: resetting\n",
			device_get_nameunit(sc->dev));

		rt2870_stop_locked(sc);
		rt2870_init_locked(sc);

		ifp->if_oerrors++;

		sc->tx_watchdog_timeouts++;
	}

	callout_reset(&sc->tx_watchdog_ch, hz, rt2870_tx_watchdog, sc);
}

/*
 * rt2870_staid_alloc
 */
static int rt2870_staid_alloc(struct rt2870_softc *sc, int aid)
{
	int staid;

	if ((aid > 0 && aid < RT2870_SOFTC_STAID_COUNT) && isclr(sc->staid_mask, aid))
	{
		staid = aid;
	}
	else
	{
		for (staid = 1; staid < RT2870_SOFTC_STAID_COUNT; staid++)
		{
			if (isclr(sc->staid_mask, staid))
				break;
		}
	}

	setbit(sc->staid_mask, staid);

	return staid;
}

/*
 * rt2870_staid_delete
 */
static void rt2870_staid_delete(struct rt2870_softc *sc, int staid)
{
	clrbit(sc->staid_mask, staid);
}

/*
 * rt2870_do_async
 */
static int rt2870_do_async(struct rt2870_softc *sc,
	void (*cb)(struct rt2870_softc *sc, void *arg),
	void *arg, int len)
{
	struct ifnet *ifp;
	struct rt2870_softc_cmd_ring *ring;
	struct rt2870_softc_cmd *cmd;
	int run_cmd_task;

	ifp = sc->ifp;
	ring = &sc->cmd_ring;

	if (!(sc->flags & RT2870_SOFTC_FLAGS_VALID))
		return -1;

	RT2870_SOFTC_LOCK(sc);

	if (ring->queued >= RT2870_SOFTC_CMD_RING_CMD_COUNT)
	{
		RT2870_SOFTC_UNLOCK(sc);
		return -1;
	}

	cmd = STAILQ_FIRST(&ring->inactive);
	STAILQ_REMOVE_HEAD(&ring->inactive, next);

	cmd->cb = cb;

	if(arg != NULL)
		memcpy(cmd->data, arg, len);

	STAILQ_INSERT_TAIL(&ring->active, cmd, next);
	ring->queued++;

	run_cmd_task = (ring->queued == 1);

	RT2870_SOFTC_UNLOCK(sc);

	if (run_cmd_task)
		taskqueue_enqueue(sc->taskqueue, &sc->cmd_task);

	return 0;
}

/*
 * rt2870_newstate_cb
 */
static void rt2870_newstate_cb(struct rt2870_softc *sc, void *arg)
{
	struct ieee80211com *ic;
	enum ieee80211_state nstate;
	struct ieee80211_node *ni;
	struct rt2870_cmd_argv_newstate *cmd_argv;
	int error;

	ic = &sc->ic;
	cmd_argv = arg;
	nstate = cmd_argv->nstate;

	RT2870_DPRINTF(sc, RT2870_DEBUG_STATE,
		"%s: newstate: %s -> %s\n",
		device_get_nameunit(sc->dev),
		ieee80211_state_name[ic->ic_state], ieee80211_state_name[nstate]);

	error = sc->newstate(ic, nstate, cmd_argv->arg);
	if (error != 0)
		return;

	/* turn link LED off */

	if (nstate != IEEE80211_S_RUN)
		rt2870_led_cmd(sc, RT2870_LED_CMD_RADIO_OFF);

	switch (nstate)
	{
		case IEEE80211_S_INIT:
			rt2870_asic_disable_tsf_sync(sc);
		break;

		case IEEE80211_S_RUN:
			ni = ic->ic_bss;

			if (ic->ic_opmode != IEEE80211_M_MONITOR)
				rt2870_rf_set_chan(sc, ni->ni_chan);

			if (ic->ic_opmode != IEEE80211_M_MONITOR)
			{
				rt2870_asic_enable_mrr(sc);
				rt2870_asic_set_txpreamble(sc);
				rt2870_asic_set_basicrates(sc);
				rt2870_asic_set_bssid(sc, ni->ni_bssid);
			}

			if (ic->ic_opmode == IEEE80211_M_STA)
				rt2870_newassoc(ni, 1);

			if (ic->ic_opmode == IEEE80211_M_HOSTAP ||
		    	ic->ic_opmode == IEEE80211_M_IBSS)
			{
				error = rt2870_beacon_alloc(sc);
				if (error != 0)
					break;

				rt2870_asic_update_beacon(sc);
			}

			if (ic->ic_opmode != IEEE80211_M_MONITOR)
				rt2870_asic_enable_tsf_sync(sc);

			/* turn link LED on */

			if (ic->ic_opmode != IEEE80211_M_MONITOR)
			{
				rt2870_led_cmd(sc, RT2870_LED_CMD_RADIO_ON |
		    		(IEEE80211_IS_CHAN_2GHZ(ni->ni_chan) ?
		     			RT2870_LED_CMD_LINK_2GHZ : RT2870_LED_CMD_LINK_5GHZ));
			}
		break;

		default:
		break;
	}
}

/*
 * rt2870_node_cleanup_cb
 */
static void rt2870_node_cleanup_cb(struct rt2870_softc *sc, void *arg)
{
	struct ieee80211com *ic;
	struct rt2870_cmd_argv_nodecleanup *cmd_argv;
	uint8_t staid, vapid, wcid;
	uint32_t tmp;

	ic = &sc->ic;
	cmd_argv = arg;
	staid = cmd_argv->staid;

	vapid = 0;
	wcid = staid;

	tmp = ((vapid & RT2870_REG_VAP_MASK) << RT2870_REG_VAP_SHIFT) |
		(RT2870_REG_CIPHER_MODE_NONE << RT2870_REG_CIPHER_MODE_SHIFT) | RT2870_REG_PKEY_ENABLE;

	rt2870_io_mac_write(sc, RT2870_REG_WCID_ATTR(wcid), tmp);

	rt2870_io_mac_write(sc, RT2870_REG_WCID(wcid), 0x00000000);
	rt2870_io_mac_write(sc, RT2870_REG_WCID(wcid) + 4, 0x00000000);

	rt2870_staid_delete(sc, staid);
}

/*
 * rt2870_scan_start_cb
 */
static void rt2870_scan_start_cb(struct rt2870_softc *sc, void *arg)
{
	struct ieee80211com *ic;
	struct ifnet *ifp;

	ic = &sc->ic;
	ifp = ic->ic_ifp;

	rt2870_asic_disable_tsf_sync(sc);
}

/*
 * rt2870_scan_end_cb
 */
static void rt2870_scan_end_cb(struct rt2870_softc *sc, void *arg)
{
	struct ieee80211com *ic;
	struct ieee80211_node *ni;

	ic = &sc->ic;
	ni = ic->ic_bss;

	rt2870_asic_enable_tsf_sync(sc);
}

/*
 * rt2870_set_channel_cb
 */
static void rt2870_set_channel_cb(struct rt2870_softc *sc, void *arg)
{
	struct ieee80211com *ic;

	ic = &sc->ic;

	RT2870_DPRINTF(sc, RT2870_DEBUG_CHAN,
		"%s: set channel: channel=%u, HT%s%s\n",
		device_get_nameunit(sc->dev),
		ieee80211_chan2ieee(ic, ic->ic_curchan),
		!IEEE80211_IS_CHAN_HT(ic->ic_curchan) ? " disabled" :
			IEEE80211_IS_CHAN_HT20(ic->ic_curchan) ? "20":
				IEEE80211_IS_CHAN_HT40U(ic->ic_curchan) ? "40U" : "40D",
		(ic->ic_flags & IEEE80211_F_SCAN) ? ", scanning" : "");

	rt2870_rf_set_chan(sc, ic->ic_curchan);
}

/*
 * rt2870_newassoc_cb
 */
static void rt2870_newassoc_cb(struct rt2870_softc *sc, void *arg)
{
	struct ieee80211com *ic;
	struct ieee80211_node *ni;
	struct rt2870_softc_node *rni;
	struct rt2870_cmd_argv_newassoc *cmd_argv;
	int isnew;
	uint8_t *macaddr, wcid;
	uint16_t aid;
	uint32_t tmp;

	ic = &sc->ic;
	cmd_argv = arg;
	isnew = cmd_argv->isnew;
	macaddr = cmd_argv->macaddr;
	ni = ieee80211_find_node(&ic->ic_sta, macaddr);
	rni = (struct rt2870_softc_node *) ni;

	if (ni == NULL)
		return;

	if (isnew)
	{
		aid = IEEE80211_AID(ni->ni_associd);
		rni->staid = rt2870_staid_alloc(sc, aid);
		wcid = rni->staid;

		tmp = (ni->ni_macaddr[3] << 24) |
			(ni->ni_macaddr[2] << 16) |
			(ni->ni_macaddr[1] << 8) |
			ni->ni_macaddr[0];

		rt2870_io_mac_write(sc, RT2870_REG_WCID(wcid), tmp);

		tmp = (ni->ni_macaddr[5] << 8) |
			ni->ni_macaddr[4];

		rt2870_io_mac_write(sc, RT2870_REG_WCID(wcid) + 4, tmp);

		rt2870_amrr_node_init(&sc->amrr, &sc->amrr_node[wcid], ni);

		RT2870_DPRINTF(sc, RT2870_DEBUG_RATE,
			"%s: initial%s node Tx rate: associd=0x%04x, rate=0x%02x, max rate=0x%02x\n",
			device_get_nameunit(sc->dev),
			(ni->ni_flags & IEEE80211_NODE_HT) ? " HT" : "",
			ni->ni_associd, ni->ni_txrate,
			(ni->ni_flags & IEEE80211_NODE_HT) ?
				(ni->ni_htrates.rs_rates[ni->ni_htrates.rs_nrates - 1] | IEEE80211_RATE_MCS) :
				(ni->ni_rates.rs_rates[ni->ni_rates.rs_nrates - 1] & IEEE80211_RATE_VAL));

		rt2870_asic_updateprot(sc);
		rt2870_asic_updateslot(sc);
		rt2870_asic_set_txpreamble(sc);
	}

	RT2870_DPRINTF(sc, RT2870_DEBUG_NODE,
		"%s: new association: isnew=%d, macaddr=%s, associd=0x%04x, staid=0x%02x, QoS %s, ERP %s, HT %s\n",
		device_get_nameunit(sc->dev), isnew, ether_sprintf(ni->ni_macaddr),
		ni->ni_associd, rni->staid,
		(ni->ni_flags & IEEE80211_NODE_QOS) ? "enabled" : "disabled",
		(ni->ni_flags & IEEE80211_NODE_ERP) ? "enabled" : "disabled",
		(ni->ni_flags & IEEE80211_NODE_HT) ? "enabled" : "disabled");

	ieee80211_free_node(ni);
}

/*
 * rt2870_updateslot_cb
 */
static void rt2870_updateslot_cb(struct rt2870_softc *sc, void *arg)
{
	rt2870_asic_updateslot(sc);
}

/*
 * rt2870_wme_update_cb
 */
static void rt2870_wme_update_cb(struct rt2870_softc *sc, void *arg)
{
	rt2870_asic_wme_update(sc);
}

/*
 * rt2870_update_beacon_cb
 */
static void rt2870_update_beacon_cb(struct rt2870_softc *sc, void *arg)
{
	struct ieee80211com *ic;
	struct mbuf *m;
	struct ieee80211_beacon_offsets *bo;
	struct rt2870_cmd_argv_updatebeacon *cmd_argv;

	ic = &sc->ic;
	m = sc->beacon_mbuf;
	bo = &sc->beacon_offsets;
	cmd_argv = arg;

	RT2870_DPRINTF(sc, RT2870_DEBUG_BEACON,
		"%s: update beacon: what=%d\n",
		device_get_nameunit(sc->dev), cmd_argv->what);

	setbit(bo->bo_flags, cmd_argv->what);

	ieee80211_beacon_update(ic->ic_bss, bo, m, 0);

	rt2870_asic_update_beacon(sc);
}

/*
 * rt2870_key_update_begin_cb
 */
static void rt2870_key_update_begin_cb(struct rt2870_softc *sc, void *arg)
{
	struct ifnet *ifp;

	ifp = sc->ifp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_KEY,
		"%s: key update begin\n",
		device_get_nameunit(sc->dev));

	taskqueue_block(sc->taskqueue);

	IF_LOCK(&ifp->if_snd);
}

/*
 * rt2870_key_update_end_cb
 */
static void rt2870_key_update_end_cb(struct rt2870_softc *sc, void *arg)
{
	struct ifnet *ifp;

	ifp = sc->ifp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_KEY,
		"%s: key update end\n",
		device_get_nameunit(sc->dev));

	IF_UNLOCK(&ifp->if_snd);

	taskqueue_unblock(sc->taskqueue);
}

/*
 * rt2870_key_set_cb
 */
static void rt2870_key_set_cb(struct rt2870_softc *sc, void *arg)
{
	struct ieee80211com *ic;
	struct ieee80211_key *k;
	enum ieee80211_opmode opmode;
	struct rt2870_cmd_argv_keyset *cmd_argv;
	uint16_t key_base, keymode_base;
	uint8_t mode, vapid, wcid, iv[8];
	uint32_t tmp;

	ic = &sc->ic;
	cmd_argv = arg;
	k = &cmd_argv->key;
	opmode = cmd_argv->opmode;

	switch (k->wk_cipher->ic_cipher)
	{
		case IEEE80211_CIPHER_WEP:
			if(k->wk_keylen < 8)
				mode = RT2870_REG_CIPHER_MODE_WEP40;
			else
				mode = RT2870_REG_CIPHER_MODE_WEP104;
		break;

		case IEEE80211_CIPHER_TKIP:
			mode = RT2870_REG_CIPHER_MODE_TKIP;
		break;

		case IEEE80211_CIPHER_AES_CCM:
			mode = RT2870_REG_CIPHER_MODE_AES_CCMP;
		break;

		default:
			return;
	}

	RT2870_DPRINTF(sc, RT2870_DEBUG_KEY,
		"%s: key set: keyix=%d, keylen=%d, mode=%d, group=%d\n",
		device_get_nameunit(sc->dev), k->wk_keyix, k->wk_keylen,
		mode, (k->wk_flags & IEEE80211_KEY_GROUP) ? 1 : 0);

	if (!(k->wk_flags & IEEE80211_KEY_GROUP))
	{
		/* install pairwise key */

		vapid = 0;
		wcid = cmd_argv->staid;
		key_base = RT2870_REG_PKEY(wcid);

		if (k->wk_cipher->ic_cipher == IEEE80211_CIPHER_WEP)
		{
			memset(iv, 0, 8);

			iv[3] = (k->wk_keyix << 6);
		}
		else
		{
			if (k->wk_cipher->ic_cipher == IEEE80211_CIPHER_TKIP)
			{
				iv[0] = (k->wk_keytsc >> 8);
				iv[1] = ((iv[0] | 0x20) & 0x7f);
				iv[2] = k->wk_keytsc;
			}
			else
			{
				/* AES CCMP */

				iv[0] = k->wk_keytsc;
				iv[1] = k->wk_keytsc >> 8;
				iv[2] = 0;
			}

			iv[3] = ((k->wk_keyix << 6) | IEEE80211_WEP_EXTIV);
			iv[4] = (k->wk_keytsc >> 16);
			iv[5] = (k->wk_keytsc >> 24);
			iv[6] = (k->wk_keytsc >> 32);
			iv[7] = (k->wk_keytsc >> 40);

			RT2870_DPRINTF(sc, RT2870_DEBUG_KEY,
				"%s: VAP key set: iv=%02x %02x %02x %02x %02x %02x %02x %02x\n",
				device_get_nameunit(sc->dev),
				iv[0], iv[1], iv[2], iv[3], iv[4], iv[5], iv[6], iv[7]);
		}

		rt2870_io_mac_write_multi(sc, RT2870_REG_IVEIV(wcid), iv, 8);

		if (k->wk_cipher->ic_cipher == IEEE80211_CIPHER_TKIP)
		{
			rt2870_io_mac_write_multi(sc, key_base, k->wk_key, 16);

			if (opmode != IEEE80211_M_HOSTAP)
			{
				rt2870_io_mac_write_multi(sc, key_base + 16, &k->wk_key[16], 8);
				rt2870_io_mac_write_multi(sc, key_base + 24, &k->wk_key[24], 8);
			}
			else
			{
				rt2870_io_mac_write_multi(sc, key_base + 16, &k->wk_key[24], 8);
				rt2870_io_mac_write_multi(sc, key_base + 24, &k->wk_key[16], 8);
			}
		}
		else
		{
			rt2870_io_mac_write_multi(sc, key_base, k->wk_key, k->wk_keylen);
		}

		tmp = ((vapid & RT2870_REG_VAP_MASK) << RT2870_REG_VAP_SHIFT) |
			(mode << RT2870_REG_CIPHER_MODE_SHIFT) | RT2870_REG_PKEY_ENABLE;

		rt2870_io_mac_write(sc, RT2870_REG_WCID_ATTR(wcid), tmp);
	}

	if ((k->wk_flags & IEEE80211_KEY_GROUP) ||
		(k->wk_cipher->ic_cipher == IEEE80211_CIPHER_WEP))
	{
		/* install group key */

		vapid = 0;
		wcid = RT2870_WCID_MCAST;
		key_base = RT2870_REG_SKEY(vapid, k->wk_keyix);
		keymode_base = RT2870_REG_SKEY_MODE(vapid);

		if (k->wk_cipher->ic_cipher == IEEE80211_CIPHER_TKIP)
		{
			rt2870_io_mac_write_multi(sc, key_base, k->wk_key, 16);

			if (opmode != IEEE80211_M_HOSTAP)
			{
				rt2870_io_mac_write_multi(sc, key_base + 16, &k->wk_key[16], 8);
				rt2870_io_mac_write_multi(sc, key_base + 24, &k->wk_key[24], 8);
			}
			else
			{
				rt2870_io_mac_write_multi(sc, key_base + 16, &k->wk_key[24], 8);
				rt2870_io_mac_write_multi(sc, key_base + 24, &k->wk_key[16], 8);
			}
		}
		else
		{
			rt2870_io_mac_write_multi(sc, key_base, k->wk_key, k->wk_keylen);
		}

		tmp = rt2870_io_mac_read(sc, keymode_base);

		tmp &= ~(0xf << (k->wk_keyix * 4 + 16 * (vapid % 2)));
		tmp |= (mode << (k->wk_keyix * 4 + 16 * (vapid % 2)));

		rt2870_io_mac_write(sc, keymode_base, tmp);

		if (opmode == IEEE80211_M_HOSTAP)
		{
			if (k->wk_cipher->ic_cipher == IEEE80211_CIPHER_WEP)
			{
				memset(iv, 0, 8);

				iv[3] = (k->wk_keyix << 6);
			}
			else
			{
				if (k->wk_cipher->ic_cipher == IEEE80211_CIPHER_TKIP)
				{
					iv[0] = (k->wk_keytsc >> 8);
					iv[1] = ((iv[0] | 0x20) & 0x7f);
					iv[2] = k->wk_keytsc;
				}
				else
				{
					/* AES CCMP */

					iv[0] = k->wk_keytsc;
					iv[1] = k->wk_keytsc >> 8;
					iv[2] = 0;
				}

				iv[3] = ((k->wk_keyix << 6) | IEEE80211_WEP_EXTIV);
				iv[4] = (k->wk_keytsc >> 16);
				iv[5] = (k->wk_keytsc >> 24);
				iv[6] = (k->wk_keytsc >> 32);
				iv[7] = (k->wk_keytsc >> 40);

				RT2870_DPRINTF(sc, RT2870_DEBUG_KEY,
					"%s: VAP key set: iv=%02x %02x %02x %02x %02x %02x %02x %02x\n",
					device_get_nameunit(sc->dev),
					iv[0], iv[1], iv[2], iv[3], iv[4], iv[5], iv[6], iv[7]);
			}

			rt2870_io_mac_write_multi(sc, RT2870_REG_IVEIV(wcid), iv, 8);

			tmp = ((vapid & RT2870_REG_VAP_MASK) << RT2870_REG_VAP_SHIFT) |
				(mode << RT2870_REG_CIPHER_MODE_SHIFT);

			rt2870_io_mac_write(sc, RT2870_REG_WCID_ATTR(wcid), tmp);
		}
	}
}

/*
 * rt2870_key_delete_cb
 */
static void rt2870_key_delete_cb(struct rt2870_softc *sc, void *arg)
{
	struct ieee80211com *ic;
	struct ieee80211_key *k;
	enum ieee80211_opmode opmode;
	struct rt2870_cmd_argv_keydelete *cmd_argv;
	uint8_t vapid, wcid;
	uint32_t tmp;

	ic = &sc->ic;
	cmd_argv = arg;
	k = &cmd_argv->key;
	opmode = cmd_argv->opmode;

	RT2870_DPRINTF(sc, RT2870_DEBUG_KEY,
		"%s: key delete: keyix=%d, keylen=%d, group=%d\n",
		device_get_nameunit(sc->dev), k->wk_keyix, k->wk_keylen,
		(k->wk_flags & IEEE80211_KEY_GROUP) ? 1 : 0);

	if (k->wk_flags & IEEE80211_KEY_GROUP)
	{
		/* remove group key */

		vapid = 0;
		wcid = RT2870_WCID_MCAST;

		tmp = rt2870_io_mac_read(sc, RT2870_REG_SKEY_MODE(vapid));

		tmp &= ~(0xf << (k->wk_keyix * 4 + 16 * (vapid % 2)));
		tmp |= (RT2870_REG_CIPHER_MODE_NONE << (k->wk_keyix * 4 + 16 * (vapid % 2)));

		rt2870_io_mac_write(sc, RT2870_REG_SKEY_MODE(vapid), tmp);

		if (opmode == IEEE80211_M_HOSTAP)
		{
			tmp = ((vapid & RT2870_REG_VAP_MASK) << RT2870_REG_VAP_SHIFT) |
				(RT2870_REG_CIPHER_MODE_NONE << RT2870_REG_CIPHER_MODE_SHIFT);

			rt2870_io_mac_write(sc, RT2870_REG_WCID_ATTR(wcid), tmp);
		}
	}
}

/*
 * rt2870_asic_set_bssid
 */
static void rt2870_asic_set_bssid(struct rt2870_softc *sc,
	const uint8_t *bssid)
{
	uint32_t tmp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_STATE,
		"%s: set bssid: bssid=%s\n",
		device_get_nameunit(sc->dev), ether_sprintf(bssid));

	tmp = bssid[0] | (bssid[1]) << 8 | (bssid[2] << 16) | (bssid[3] << 24);

	rt2870_io_mac_write(sc, RT2870_REG_BSSID_DW0, tmp);

	tmp = bssid[4] | (bssid[5] << 8);

	rt2870_io_mac_write(sc, RT2870_REG_BSSID_DW1, tmp);
}

/*
 * rt2870_asic_set_macaddr
 */
static void rt2870_asic_set_macaddr(struct rt2870_softc *sc,
	const uint8_t *addr)
{
	uint32_t tmp;

	tmp = addr[0] | (addr[1] << 8) | (addr[2] << 16) | (addr[3] << 24);

	rt2870_io_mac_write(sc, RT2870_REG_ADDR_DW0, tmp);

	tmp = addr[4] | (addr[5] << 8) | (0xff << 16);

	rt2870_io_mac_write(sc, RT2870_REG_ADDR_DW1, tmp);
}

/*
 * rt2870_asic_enable_tsf_sync
 */
static void rt2870_asic_enable_tsf_sync(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	uint32_t tmp;

	ic = &sc->ic;

	RT2870_DPRINTF(sc, RT2870_DEBUG_BEACON,
		"%s: enabling TSF\n",
		device_get_nameunit(sc->dev));

	tmp = rt2870_io_mac_read(sc, RT2870_REG_BCN_TIME_CFG);

	tmp &= ~0x1fffff;
	tmp |= ic->ic_bss->ni_intval * 16;
	tmp |= (RT2870_REG_TSF_TIMER_ENABLE | RT2870_REG_TBTT_TIMER_ENABLE);

	if (ic->ic_opmode == IEEE80211_M_STA)
	{
		tmp |= (RT2870_REG_TSF_SYNC_MODE_STA << RT2870_REG_TSF_SYNC_MODE_SHIFT);
	}
	else if (ic->ic_opmode == IEEE80211_M_IBSS)
	{
		tmp |= RT2870_REG_BCN_TX_ENABLE;
		tmp |= (RT2870_REG_TSF_SYNC_MODE_IBSS << RT2870_REG_TSF_SYNC_MODE_SHIFT);
	}
	else if (ic->ic_opmode == IEEE80211_M_HOSTAP)
	{
		tmp |= RT2870_REG_BCN_TX_ENABLE;
		tmp |= (RT2870_REG_TSF_SYNC_MODE_HOSTAP << RT2870_REG_TSF_SYNC_MODE_SHIFT);
	}

	rt2870_io_mac_write(sc, RT2870_REG_BCN_TIME_CFG, tmp);
}

/*
 * rt2870_asic_disable_tsf_sync
 */
static void rt2870_asic_disable_tsf_sync(struct rt2870_softc *sc)
{
	uint32_t tmp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_BEACON,
		"%s: disabling TSF\n",
		device_get_nameunit(sc->dev));

	tmp = rt2870_io_mac_read(sc, RT2870_REG_BCN_TIME_CFG);

	tmp &= ~(RT2870_REG_BCN_TX_ENABLE |
		RT2870_REG_TSF_TIMER_ENABLE |
		RT2870_REG_TBTT_TIMER_ENABLE);

	tmp &= ~(RT2870_REG_TSF_SYNC_MODE_MASK << RT2870_REG_TSF_SYNC_MODE_SHIFT);
	tmp |= (RT2870_REG_TSF_SYNC_MODE_DISABLE << RT2870_REG_TSF_SYNC_MODE_SHIFT);

	rt2870_io_mac_write(sc, RT2870_REG_BCN_TIME_CFG, tmp);
}

/*
 * rt2870_asic_enable_mrr
 */
static void rt2870_asic_enable_mrr(struct rt2870_softc *sc)
{
#define CCK(mcs)	(mcs)
#define OFDM(mcs)	((1 << 3) | (mcs))
#define HT(mcs)		(mcs)

	rt2870_io_mac_write(sc, RT2870_REG_TX_LG_FBK_CFG0,
		(OFDM(6) << 28) |	/* 54 -> 48 */
		(OFDM(5) << 24) |	/* 48 -> 36 */
		(OFDM(4) << 20) |	/* 36 -> 24 */
		(OFDM(3) << 16) |	/* 24 -> 18 */
		(OFDM(2) << 12) |	/* 18 -> 12 */
		(OFDM(1) << 8)  |	/* 12 -> 9 */
		(OFDM(0) << 4)  |	/*  9 -> 6 */
		OFDM(0));			/*  6 -> 6 */

	rt2870_io_mac_write(sc, RT2870_REG_TX_LG_FBK_CFG1,
		(CCK(2) << 12) |	/* 11  -> 5.5 */
		(CCK(1) << 8)  |	/* 5.5 -> 2 */
		(CCK(0) << 4)  |	/*   2 -> 1 */
		CCK(0));			/*   1 -> 1 */

	rt2870_io_mac_write(sc, RT2870_REG_TX_HT_FBK_CFG0,
		(HT(6) << 28) |
		(HT(5) << 24) |
		(HT(4) << 20) |
		(HT(3) << 16) |
		(HT(2) << 12) |
		(HT(1) << 8)  |
		(HT(0) << 4)  |
		HT(0));

	rt2870_io_mac_write(sc, RT2870_REG_TX_HT_FBK_CFG1,
		(HT(14) << 28) |
		(HT(13) << 24) |
		(HT(12) << 20) |
		(HT(11) << 16) |
		(HT(10) << 12) |
		(HT(9) << 8)   |
		(HT(8) << 4)   |
		HT(7));

#undef HT
#undef OFDM
#undef CCK
}

/*
 * rt2870_asic_set_txpreamble
 */
static void rt2870_asic_set_txpreamble(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	uint32_t tmp;

	ic = &sc->ic;

	RT2870_DPRINTF(sc, RT2870_DEBUG_STATE,
		"%s: %s short Tx preamble\n",
		device_get_nameunit(sc->dev),
		(ic->ic_flags & IEEE80211_F_SHPREAMBLE) ? "enabling" : "disabling");

	tmp = rt2870_io_mac_read(sc, RT2870_REG_AUTO_RSP_CFG);

	tmp &= ~RT2870_REG_CCK_SHORT_ENABLE;

	if (ic->ic_flags & IEEE80211_F_SHPREAMBLE)
		tmp |= RT2870_REG_CCK_SHORT_ENABLE;

	rt2870_io_mac_write(sc, RT2870_REG_AUTO_RSP_CFG, tmp);
}

/*
 * rt2870_asic_set_basicrates
 */
static void rt2870_asic_set_basicrates(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;

	ic = &sc->ic;

	if (ic->ic_curmode == IEEE80211_MODE_11B)
		rt2870_io_mac_write(sc, RT2870_REG_LEGACY_BASIC_RATE, 0xf);
	else if (ic->ic_curmode == IEEE80211_MODE_11A)
		rt2870_io_mac_write(sc, RT2870_REG_LEGACY_BASIC_RATE, 0x150);
	else
		rt2870_io_mac_write(sc, RT2870_REG_LEGACY_BASIC_RATE, 0x15f);
}

/*
 * rt2870_asic_update_rtsthreshold
 */
static void rt2870_asic_update_rtsthreshold(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	uint32_t tmp;
	uint16_t threshold;

	ic = &sc->ic;

	RT2870_DPRINTF(sc, RT2870_DEBUG_PROT,
		"%s: updating RTS threshold: %d\n",
		device_get_nameunit(sc->dev), ic->ic_rtsthreshold);

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_RTS_CFG);

	tmp &= ~(RT2870_REG_TX_RTS_THRESHOLD_MASK << RT2870_REG_TX_RTS_THRESHOLD_SHIFT);

	if (ic->ic_flags_ext & IEEE80211_FEXT_AMSDU_TX)
		threshold = 0x1000;
	else
		threshold = ic->ic_rtsthreshold;

	tmp |= ((threshold & RT2870_REG_TX_RTS_THRESHOLD_MASK) <<
		RT2870_REG_TX_RTS_THRESHOLD_SHIFT);

	rt2870_io_mac_write(sc, RT2870_REG_TX_RTS_CFG, tmp);
}

/*
 * rt2870_asic_update_txpower
 */
static void rt2870_asic_update_txpower(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	uint32_t *txpow_rate;
	int8_t delta;
	uint8_t val;
	uint32_t tmp;
	int i;

	ic = &sc->ic;

	RT2870_DPRINTF(sc, RT2870_DEBUG_STATE,
		"%s: updating Tx power: %d\n",
		device_get_nameunit(sc->dev), ic->ic_txpowlimit);

	if (!IEEE80211_IS_CHAN_HT40(ic->ic_curchan))
	{
		txpow_rate = sc->txpow_rate_20mhz;
	}
	else
	{
		if (IEEE80211_IS_CHAN_2GHZ(ic->ic_curchan))
			txpow_rate = sc->txpow_rate_40mhz_2ghz;
		else
			txpow_rate = sc->txpow_rate_40mhz_5ghz;
	}

	delta = 0;

	val = rt2870_io_bbp_read(sc, 1);
	val &= 0xfc;

	if (ic->ic_txpowlimit > 90)
	{
		/* do nothing */
	}
	else if (ic->ic_txpowlimit > 60)
	{
		delta -= 1;
	}
	else if (ic->ic_txpowlimit > 30)
	{
		delta -= 3;
	}
	else if (ic->ic_txpowlimit > 15)
	{
		val |= 0x1;
	}
	else if (ic->ic_txpowlimit > 9)
	{
		val |= 0x1;
		delta -= 3;
	}
	else
	{
		val |= 0x2;
	}

	rt2870_io_bbp_write(sc, 1, val);

	for (i = 0; i < RT2870_SOFTC_TXPOW_RATE_COUNT; i++)
	{
		if (txpow_rate[i] == 0xffffffff)
			continue;

		tmp = rt2870_read_eeprom_txpow_rate_add_delta(txpow_rate[i], delta);

		rt2870_io_mac_write(sc, RT2870_REG_TX_PWR_CFG(i), tmp);
	}
}

/*
 * rt2870_asic_update_promisc
 */
static void rt2870_asic_update_promisc(struct rt2870_softc *sc)
{
	struct ifnet *ifp;
	uint32_t tmp;

	ifp = sc->ic.ic_ifp;

	printf("%s: %s promiscuous mode\n",
		device_get_nameunit(sc->dev),
		(ifp->if_flags & IFF_PROMISC) ? "entering" : "leaving");

	tmp = rt2870_io_mac_read(sc, RT2870_REG_RX_FILTER_CFG);

	tmp &= ~RT2870_REG_RX_FILTER_DROP_UC_NOME;

	if (!(ifp->if_flags & IFF_PROMISC))
		tmp |= RT2870_REG_RX_FILTER_DROP_UC_NOME;

	rt2870_io_mac_write(sc, RT2870_REG_RX_FILTER_CFG, tmp);
}

/*
 * rt2870_asic_updateprot
 */
static void rt2870_asic_updateprot(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	uint32_t cck_prot, ofdm_prot, mm20_prot, mm40_prot, gf20_prot, gf40_prot;
	uint8_t htopmode;
	enum ieee80211_protmode htprotmode;

	ic = &sc->ic;

	if (!(sc->flags & RT2870_SOFTC_FLAGS_VALID))
		return;

	/* CCK frame protection */

	cck_prot = RT2870_REG_RTSTH_ENABLE | RT2870_REG_PROT_NAV_SHORT |
		RT2870_REG_TXOP_ALLOW_ALL | RT2870_REG_PROT_CTRL_NONE;

	/* set up protection frame phy mode and rate (MCS code) */

	if (ic->ic_curmode == IEEE80211_MODE_11A)
		cck_prot |= (RT2870_REG_PROT_PHYMODE_OFDM << RT2870_REG_PROT_PHYMODE_SHIFT) |
			(0 << RT2870_REG_PROT_MCS_SHIFT);
	else
		cck_prot |= ((RT2870_REG_PROT_PHYMODE_CCK << RT2870_REG_PROT_PHYMODE_SHIFT) |
			(3 << RT2870_REG_PROT_MCS_SHIFT));

	rt2870_io_mac_write(sc, RT2870_REG_TX_CCK_PROT_CFG, cck_prot);

	/* OFDM frame protection */

	ofdm_prot = RT2870_REG_RTSTH_ENABLE | RT2870_REG_PROT_NAV_SHORT |
		RT2870_REG_TXOP_ALLOW_ALL;

	if (ic->ic_flags & IEEE80211_F_USEPROT)
	{
		RT2870_DPRINTF(sc, RT2870_DEBUG_PROT,
			"%s: updating protection mode: b/g protection mode=%s\n",
			device_get_nameunit(sc->dev),
			(ic->ic_protmode == IEEE80211_PROT_RTSCTS) ? "RTS/CTS" :
				((ic->ic_protmode == IEEE80211_PROT_CTSONLY) ? "CTS-to-self" : "none"));

		if (ic->ic_protmode == IEEE80211_PROT_RTSCTS)
			ofdm_prot |= RT2870_REG_PROT_CTRL_RTS_CTS;
		else if (ic->ic_protmode == IEEE80211_PROT_CTSONLY)
			ofdm_prot |= RT2870_REG_PROT_CTRL_CTS;
		else
			ofdm_prot |= RT2870_REG_PROT_CTRL_NONE;
	}
	else
	{
		RT2870_DPRINTF(sc, RT2870_DEBUG_PROT,
			"%s: updating protection mode: b/g protection mode=%s\n",
			device_get_nameunit(sc->dev), "none");

		ofdm_prot |= RT2870_REG_PROT_CTRL_NONE;
	}

	rt2870_io_mac_write(sc, RT2870_REG_TX_OFDM_PROT_CFG, ofdm_prot);

	/* HT frame protection */

	if ((ic->ic_opmode == IEEE80211_M_STA) && (ic->ic_state == IEEE80211_S_RUN))
		htopmode = ic->ic_bss->ni_htopmode;
	else
		htopmode = ic->ic_curhtprotmode;

	htprotmode = ic->ic_htprotmode;

	/* force HT mixed mode and RTS/CTS protection if A-MPDU Tx aggregation is enabled */

	if (sc->tx_ampdu_sessions > 0)
	{
		RT2870_DPRINTF(sc, RT2870_DEBUG_PROT,
			"%s: updating protection mode: forcing HT mixed mode and RTS/CTS protection\n",
			device_get_nameunit(sc->dev));

		htopmode = IEEE80211_HTINFO_OPMODE_MIXED;
		htprotmode = IEEE80211_PROT_RTSCTS;
	}

	RT2870_DPRINTF(sc, RT2870_DEBUG_PROT,
		"%s: updating protection mode: HT operation mode=0x%02x, protection mode=%s\n",
		device_get_nameunit(sc->dev),
		htopmode & IEEE80211_HTINFO_OPMODE,
		(htprotmode == IEEE80211_PROT_RTSCTS) ? "RTS/CTS" :
			((htprotmode == IEEE80211_PROT_CTSONLY) ? "CTS-to-self" : "none"));

	switch (htopmode & IEEE80211_HTINFO_OPMODE)
	{
		/* IEEE80211_HTINFO_OPMODE_HT20PR */
		case IEEE80211_HTINFO_OPMODE_HT20PR:
			mm20_prot = RT2870_REG_PROT_NAV_SHORT | RT2870_REG_PROT_CTRL_NONE |
				RT2870_REG_TXOP_ALLOW_CCK | RT2870_REG_TXOP_ALLOW_OFDM |
				RT2870_REG_TXOP_ALLOW_MM20 | RT2870_REG_TXOP_ALLOW_GF20 |
				(RT2870_REG_PROT_PHYMODE_OFDM << RT2870_REG_PROT_PHYMODE_SHIFT) |
				(4 << RT2870_REG_PROT_MCS_SHIFT);

			gf20_prot = mm20_prot;

			mm40_prot = RT2870_REG_PROT_NAV_SHORT | RT2870_REG_PROT_CTRL_NONE |
				RT2870_REG_TXOP_ALLOW_ALL |
				(RT2870_REG_PROT_PHYMODE_OFDM << RT2870_REG_PROT_PHYMODE_SHIFT) |
				(0x84 << RT2870_REG_PROT_MCS_SHIFT);

			if (htprotmode == IEEE80211_PROT_RTSCTS)
				mm40_prot |= RT2870_REG_PROT_CTRL_RTS_CTS;
			else if (htprotmode == IEEE80211_PROT_CTSONLY)
				mm40_prot |= RT2870_REG_PROT_CTRL_CTS;
			else
				mm40_prot |= RT2870_REG_PROT_CTRL_NONE;

			gf40_prot = mm40_prot;
		break;

		/* IEEE80211_HTINFO_OPMODE_MIXED */
		case IEEE80211_HTINFO_OPMODE_MIXED:
			mm20_prot = RT2870_REG_PROT_NAV_SHORT |
				RT2870_REG_TXOP_ALLOW_CCK | RT2870_REG_TXOP_ALLOW_OFDM |
				RT2870_REG_TXOP_ALLOW_MM20 | RT2870_REG_TXOP_ALLOW_GF20;

			if (ic->ic_flags & IEEE80211_F_USEPROT)
				mm20_prot |= (RT2870_REG_PROT_PHYMODE_CCK << RT2870_REG_PROT_PHYMODE_SHIFT) |
					(3 << RT2870_REG_PROT_MCS_SHIFT);
			else
				mm20_prot |= (RT2870_REG_PROT_PHYMODE_OFDM << RT2870_REG_PROT_PHYMODE_SHIFT) |
					(4 << RT2870_REG_PROT_MCS_SHIFT);

			if (htprotmode == IEEE80211_PROT_RTSCTS)
				mm20_prot |= RT2870_REG_PROT_CTRL_RTS_CTS;
			else if (htprotmode == IEEE80211_PROT_CTSONLY)
				mm20_prot |= RT2870_REG_PROT_CTRL_CTS;
			else
				mm20_prot |= RT2870_REG_PROT_CTRL_NONE;

			gf20_prot = mm20_prot;

			mm40_prot = RT2870_REG_PROT_NAV_SHORT | RT2870_REG_TXOP_ALLOW_ALL;

			if (ic->ic_flags & IEEE80211_F_USEPROT)
				mm40_prot |= (RT2870_REG_PROT_PHYMODE_CCK << RT2870_REG_PROT_PHYMODE_SHIFT) |
					(3 << RT2870_REG_PROT_MCS_SHIFT);
			else
				mm40_prot |= (RT2870_REG_PROT_PHYMODE_OFDM << RT2870_REG_PROT_PHYMODE_SHIFT) |
					(0x84 << RT2870_REG_PROT_MCS_SHIFT);

			if (htprotmode == IEEE80211_PROT_RTSCTS)
				mm40_prot |= RT2870_REG_PROT_CTRL_RTS_CTS;
			else if (htprotmode == IEEE80211_PROT_CTSONLY)
				mm40_prot |= RT2870_REG_PROT_CTRL_CTS;
			else
				mm40_prot |= RT2870_REG_PROT_CTRL_NONE;

			gf40_prot = mm40_prot;
		break;

		/*
		 * IEEE80211_HTINFO_OPMODE_PURE
		 * IEEE80211_HTINFO_OPMODE_PROTOPT
		 */
		case IEEE80211_HTINFO_OPMODE_PURE:
		case IEEE80211_HTINFO_OPMODE_PROTOPT:
		default:
			mm20_prot = RT2870_REG_PROT_NAV_SHORT | RT2870_REG_PROT_CTRL_NONE |
				RT2870_REG_TXOP_ALLOW_CCK | RT2870_REG_TXOP_ALLOW_OFDM |
				RT2870_REG_TXOP_ALLOW_MM20 | RT2870_REG_TXOP_ALLOW_GF20 |
				(RT2870_REG_PROT_PHYMODE_OFDM << RT2870_REG_PROT_PHYMODE_SHIFT) |
				(4 << RT2870_REG_PROT_MCS_SHIFT);

			gf20_prot = mm20_prot;

			mm40_prot = RT2870_REG_PROT_NAV_SHORT | RT2870_REG_PROT_CTRL_NONE |
				RT2870_REG_TXOP_ALLOW_ALL |
				(RT2870_REG_PROT_PHYMODE_OFDM << RT2870_REG_PROT_PHYMODE_SHIFT) |
				(0x84 << RT2870_REG_PROT_MCS_SHIFT);

			gf40_prot = mm40_prot;
		break;
	}

	rt2870_io_mac_write(sc, RT2870_REG_TX_MM20_PROT_CFG, mm20_prot);
	rt2870_io_mac_write(sc, RT2870_REG_TX_MM40_PROT_CFG, mm40_prot);
	rt2870_io_mac_write(sc, RT2870_REG_TX_GF20_PROT_CFG, gf20_prot);
	rt2870_io_mac_write(sc, RT2870_REG_TX_GF40_PROT_CFG, gf40_prot);
}

/*
 * rt2870_asic_updateslot
 */
static void rt2870_asic_updateslot(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	uint32_t tmp;

	ic = &sc->ic;

	RT2870_DPRINTF(sc, RT2870_DEBUG_STATE,
		"%s: %s short slot time\n",
		device_get_nameunit(sc->dev),
		(ic->ic_flags & (IEEE80211_F_SHSLOT | IEEE80211_F_BURST)) ? "enabling" : "disabling");

	tmp = rt2870_io_mac_read(sc, RT2870_REG_BKOFF_SLOT_CFG);

	tmp &= ~0xff;
	tmp |= (ic->ic_flags & (IEEE80211_F_SHSLOT | IEEE80211_F_BURST)) ? 9 : 20;

	rt2870_io_mac_write(sc, RT2870_REG_BKOFF_SLOT_CFG, tmp);
}

/*
 * rt2870_asic_wme_update
 */
static void rt2870_asic_wme_update(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	struct ieee80211_wme_state *wme;
	const struct wmeParams *wmep;
	int i;

	ic = &sc->ic;
	wme = &ic->ic_wme;
	wmep = wme->wme_chanParams.cap_wmeParams;

	RT2870_DPRINTF(sc, RT2870_DEBUG_WME,
		"%s: wme update: WME_AC_VO=%d/%d/%d/%d, WME_AC_VI=%d/%d/%d/%d, "
		"WME_AC_BK=%d/%d/%d/%d, WME_AC_BE=%d/%d/%d/%d\n",
		device_get_nameunit(sc->dev),
		wmep[WME_AC_VO].wmep_aifsn,
		wmep[WME_AC_VO].wmep_logcwmin, wmep[WME_AC_VO].wmep_logcwmax,
		wmep[WME_AC_VO].wmep_txopLimit,
		wmep[WME_AC_VI].wmep_aifsn,
		wmep[WME_AC_VI].wmep_logcwmin, wmep[WME_AC_VI].wmep_logcwmax,
		wmep[WME_AC_VI].wmep_txopLimit,
		wmep[WME_AC_BK].wmep_aifsn,
		wmep[WME_AC_BK].wmep_logcwmin, wmep[WME_AC_BK].wmep_logcwmax,
		wmep[WME_AC_BK].wmep_txopLimit,
		wmep[WME_AC_BE].wmep_aifsn,
		wmep[WME_AC_BE].wmep_logcwmin, wmep[WME_AC_BE].wmep_logcwmax,
		wmep[WME_AC_BE].wmep_txopLimit);

	for (i = 0; i < WME_NUM_AC; i++)
		rt2870_io_mac_write(sc, RT2870_REG_TX_EDCA_AC_CFG(i),
			(wmep[i].wmep_logcwmax << 16) | (wmep[i].wmep_logcwmin << 12) |
			(wmep[i].wmep_aifsn << 8) | wmep[i].wmep_txopLimit);

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_WMM_AIFSN_CFG,
		(wmep[WME_AC_VO].wmep_aifsn << 12) | (wmep[WME_AC_VI].wmep_aifsn << 8) |
		(wmep[WME_AC_BK].wmep_aifsn << 4) | wmep[WME_AC_BE].wmep_aifsn);

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_WMM_CWMIN_CFG,
		(wmep[WME_AC_VO].wmep_logcwmin << 12) | (wmep[WME_AC_VI].wmep_logcwmin << 8) |
		(wmep[WME_AC_BK].wmep_logcwmin << 4) | wmep[WME_AC_BE].wmep_logcwmin);

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_WMM_CWMAX_CFG,
		(wmep[WME_AC_VO].wmep_logcwmax << 12) | (wmep[WME_AC_VI].wmep_logcwmax << 8) |
		(wmep[WME_AC_BK].wmep_logcwmax << 4) | wmep[WME_AC_BE].wmep_logcwmax);

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_WMM_TXOP0_CFG,
		(wmep[WME_AC_BK].wmep_txopLimit << 16) | wmep[WME_AC_BE].wmep_txopLimit);

	rt2870_io_mac_write(sc, RT2870_REG_SCHDMA_WMM_TXOP1_CFG,
		(wmep[WME_AC_VO].wmep_txopLimit << 16) | wmep[WME_AC_VI].wmep_txopLimit);
}

/*
 * rt2870_asic_update_beacon
 */
static void rt2870_asic_update_beacon(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	struct mbuf *m;
	struct rt2870_txwi *txwi;
	uint32_t tmp;

	ic = &sc->ic;

	m = sc->beacon_mbuf;
	txwi = &sc->beacon_txwi;

	/* disable temporarily TSF sync */

	tmp = rt2870_io_mac_read(sc, RT2870_REG_BCN_TIME_CFG);

	tmp &= ~(RT2870_REG_BCN_TX_ENABLE |
		RT2870_REG_TSF_TIMER_ENABLE |
		RT2870_REG_TBTT_TIMER_ENABLE);

	rt2870_io_mac_write(sc, RT2870_REG_BCN_TIME_CFG, tmp);

	/* write Tx wireless info and beacon frame to on-chip memory */

	rt2870_io_mac_write_multi(sc, RT2870_REG_BEACON_BASE(0),
		txwi, sizeof(struct rt2870_txwi));

	rt2870_io_mac_write_multi(sc, RT2870_REG_BEACON_BASE(0) + sizeof(struct rt2870_txwi),
		mtod(m, uint8_t *), m->m_pkthdr.len);

	/* enable again TSF sync */

	tmp = rt2870_io_mac_read(sc, RT2870_REG_BCN_TIME_CFG);

	tmp |= (RT2870_REG_BCN_TX_ENABLE |
		RT2870_REG_TSF_TIMER_ENABLE |
		RT2870_REG_TBTT_TIMER_ENABLE);

	rt2870_io_mac_write(sc, RT2870_REG_BCN_TIME_CFG, tmp);
}

/*
 * rt2870_asic_clear_keytables
 */
static void rt2870_asic_clear_keytables(struct rt2870_softc *sc)
{
	int i;

	/* clear Rx WCID search table (entries = 256, entry size = 8) */

	for (i = 0; i < 256; i++)
	{
		rt2870_io_mac_write(sc, RT2870_REG_WCID(i), 0xffffffff);
		rt2870_io_mac_write(sc, RT2870_REG_WCID(i) + 4, 0x0000ffff);
	}

	/* clear WCID attribute table (entries = 256, entry size = 4) */

	rt2870_io_mac_set_region_4(sc, RT2870_REG_WCID_ATTR(0), 0, 256);

	/* clear IV/EIV table (entries = 256, entry size = 8) */

	rt2870_io_mac_set_region_4(sc, RT2870_REG_IVEIV(0), 0, 2 * 256);

	/* clear pairwise key table (entries = 64, entry size = 32) */

	rt2870_io_mac_set_region_4(sc, RT2870_REG_PKEY(0), 0, 8 * 64);

	/* clear shared key table (entries = 32, entry size = 32) */

	rt2870_io_mac_set_region_4(sc, RT2870_REG_SKEY(0, 0), 0, 8 * 32);

	/* clear shared key mode (entries = 32, entry size = 2) */

	rt2870_io_mac_set_region_4(sc, RT2870_REG_SKEY_MODE(0), 0, 16);
}

/*
 * rt2870_asic_add_ba_session
 */
static void rt2870_asic_add_ba_session(struct rt2870_softc *sc,
	uint8_t wcid, int tid)
{
	uint32_t tmp;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_WCID(wcid) + 4);

	tmp |= (0x10000 << tid);

	rt2870_io_mac_write(sc, RT2870_REG_WCID(wcid) + 4, tmp);
}

/*
 * rt2860_asic_del_ba_session
 */
static void rt2870_asic_del_ba_session(struct rt2870_softc *sc,
	uint8_t wcid, int tid)
{
	uint32_t tmp;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_WCID(wcid) + 4);

	tmp &= ~(0x10000 << tid);

	rt2870_io_mac_write(sc, RT2870_REG_WCID(wcid) + 4, tmp);
}

/*
 * rt2870_beacon_alloc
 */
static int rt2870_beacon_alloc(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	struct mbuf *m;
	struct rt2870_txwi txwi;
	uint8_t rate, mcs;

	ic = &sc->ic;

	m = ieee80211_beacon_alloc(ic->ic_bss, &sc->beacon_offsets);
	if (m == NULL)
		return ENOMEM;

	rate = IEEE80211_IS_CHAN_5GHZ(ic->ic_curchan) ? 12 : 2;
	mcs = rt2870_rate2mcs(rate);

	memset(&txwi, 0, sizeof(struct rt2870_txwi));

	txwi.wcid = 0xff;
	txwi.pid_mpdu_len = ((htole16(m->m_pkthdr.len) & RT2870_TXWI_MPDU_LEN_MASK) <<
		 RT2870_TXWI_MPDU_LEN_SHIFT);
	txwi.txop = (RT2870_TXWI_TXOP_HT << RT2870_TXWI_TXOP_SHIFT);
	txwi.mpdu_density_flags |=
		(RT2870_TXWI_FLAGS_TS << RT2870_TXWI_FLAGS_SHIFT);
	txwi.bawin_size_xflags |=
		(RT2870_TXWI_XFLAGS_NSEQ << RT2870_TXWI_XFLAGS_SHIFT);

	if (rate == 2)
	{
		txwi.phymode_ifs_stbc_shortgi =
			(RT2870_TXWI_PHYMODE_CCK << RT2870_TXWI_PHYMODE_SHIFT);

		if (rate != 2 && (ic->ic_flags & IEEE80211_F_SHPREAMBLE))
			mcs |= RT2870_TXWI_MCS_SHOTPRE;
	}
	else
	{
		txwi.phymode_ifs_stbc_shortgi =
			(RT2870_TXWI_PHYMODE_OFDM << RT2870_TXWI_PHYMODE_SHIFT);
	}

	txwi.bw_mcs = (RT2870_TXWI_BW_20 << RT2870_TXWI_BW_SHIFT) |
		((mcs & RT2870_TXWI_MCS_MASK) << RT2870_TXWI_MCS_SHIFT);

	if (sc->beacon_mbuf != NULL)
	{
		m_free(sc->beacon_mbuf);
		sc->beacon_mbuf = NULL;
	}

	sc->beacon_mbuf = m;
	sc->beacon_txwi = txwi;

	return 0;
}

/*
 * rt2870_rxrate
 */
static uint8_t rt2870_rxrate(struct rt2870_rxwi *rxwi)
{
	uint8_t mcs, phymode;
	uint8_t rate;

	mcs = (rxwi->bw_mcs >> RT2870_RXWI_MCS_SHIFT) & RT2870_RXWI_MCS_MASK;
	phymode = (rxwi->phymode_stbc_shortgi >> RT2870_RXWI_PHYMODE_SHIFT) &
		RT2870_RXWI_PHYMODE_MASK;

	rate = 2;

	switch (phymode)
	{
		case RT2870_RXWI_PHYMODE_CCK:
			switch (mcs & ~RT2870_RXWI_MCS_SHOTPRE)
			{
				case 0: rate = 2; break;	/* 1 Mbps */
				case 1: rate = 4; break;	/* 2 MBps */
				case 2: rate = 11; break;	/* 5.5 Mbps */
				case 3: rate = 22; break;	/* 11 Mbps */
			}
		break;

		case RT2870_RXWI_PHYMODE_OFDM:
			switch (mcs)
			{
				case 0: rate = 12; break;	/* 6 Mbps */
				case 1: rate = 18; break;	/* 9 Mbps */
				case 2: rate = 24; break;	/* 12 Mbps */
				case 3: rate = 36; break;	/* 18 Mbps */
				case 4: rate = 48; break;	/* 24 Mbps */
				case 5: rate = 72; break;	/* 36 Mbps */
				case 6: rate = 96; break;	/* 48 Mbps */
				case 7: rate = 108; break;	/* 54 Mbps */
			}
		break;

		case RT2870_RXWI_PHYMODE_HT_MIXED:
		case RT2870_RXWI_PHYMODE_HT_GF:
		break;
	}

	return rate;
}

/*
 * rt2870_maxrssi_rxpath
 */
static uint8_t rt2870_maxrssi_rxpath(struct rt2870_softc *sc,
	const struct rt2870_rxwi *rxwi)
{
	uint8_t rxpath;

	rxpath = 0;

	if (sc->nrxpath > 1)
		if (rxwi->rssi[1] > rxwi->rssi[rxpath])
			rxpath = 1;

	if (sc->nrxpath > 2)
		if (rxwi->rssi[2] > rxwi->rssi[rxpath])
			rxpath = 2;

	return rxpath;
}

/*
 * rt2870_rssi2dbm
 */
static int8_t rt2870_rssi2dbm(struct rt2870_softc *sc,
	uint8_t rssi, uint8_t rxpath)
{
	struct ieee80211com *ic;
	struct ieee80211_channel *c;
	int chan;
	int8_t rssi_off, lna_gain;

	if (rssi == 0)
		return -99;

	ic = &sc->ic;
	c = ic->ic_curchan;
	chan = ieee80211_chan2ieee(ic, c);

	if (IEEE80211_IS_CHAN_5GHZ(c))
	{
		rssi_off = sc->rssi_off_5ghz[rxpath];

		if (chan <= 64)
			lna_gain = sc->lna_gain[1];
		else if (chan <= 128)
			lna_gain = sc->lna_gain[2];
		else
			lna_gain = sc->lna_gain[3];
	}
	else
	{
		rssi_off = sc->rssi_off_2ghz[rxpath];
		lna_gain = sc->lna_gain[0];
	}

	return (-12 - rssi_off - lna_gain - rssi);
}

/*
 * rt2870_rate2mcs
 */
static uint8_t rt2870_rate2mcs(uint8_t rate)
{
	switch (rate)
	{
		/* CCK rates */
		case 2:	return 0;
		case 4:	return 1;
		case 11: return 2;
		case 22: return 3;

		/* OFDM rates */
		case 12: return 0;
		case 18: return 1;
		case 24: return 2;
		case 36: return 3;
		case 48: return 4;
		case 72: return 5;
		case 96: return 6;
		case 108: return 7;
	}

	return 0;
}

/*
 * rt2870_ackrate
 */
static int rt2870_ackrate(struct ieee80211com *ic, int rate)
{
	switch (rate)
	{
		/* CCK rates */

		case 2:
			return 2;

		case 4:
		case 11:
		case 22:
			return (ic->ic_curmode == IEEE80211_MODE_11B) ? 4 : rate;

		/* OFDM rates */

		case 12:
		case 18:
			return 12;

		case 24:
		case 36:
			return 24;

		case 48:
		case 72:
		case 96:
		case 108:
			return 48;
	}

	/* default to 1Mbps */
	return 2;
}

/*
 * rt2870_txtime
 */
static uint16_t rt2870_txtime(int len, int rate, uint32_t flags)
{
	uint16_t txtime;

	if (RT2870_RATE_IS_OFDM(rate))
	{
		txtime = (8 + 4 * len + 3 + rate - 1) / rate;
		txtime = 16 + 4 + 4 * txtime + 6;
	}
	else
	{
		txtime = (16 * len + rate - 1) / rate;

		if (rate != 2 && (flags & IEEE80211_F_SHPREAMBLE))
			txtime += 72 + 24;
		else
			txtime += 144 + 48;
	}

	return txtime;
}

/*
 * rt2870_rx_frame
 */
static void rt2870_rx_frame(struct rt2870_softc *sc,
	uint8_t *buf, uint32_t dmalen)
{
	struct ieee80211com *ic;
	struct ifnet *ifp;
	struct ieee80211_frame *wh;
	struct ieee80211_node *ni;
	struct rt2870_softc_node *rni;
	struct rt2870_softc_rx_radiotap_header *tap;
	struct rt2870_rxwi *rxwi;
	struct rt2870_rxinfo *rxinfo;
	struct mbuf *m;
	uint32_t rxinfo_flags;
	uint8_t cipher_err, rssi, ant, phymode, bw, shortgi, stbc, mcs, keyidx, tid, frag;
	uint16_t seq;
	int8_t rssi_dbm;
	int len, ampdu, amsdu, rssi_dbm_rel, i;

	ic = &sc->ic;
	ifp = ic->ic_ifp;

	/* get Rx wireless info */

	rxwi = (struct rt2870_rxwi *) buf;
	len = (le16toh(rxwi->tid_size) >> RT2870_RXWI_SIZE_SHIFT) &
		RT2870_RXWI_SIZE_MASK;

	if (len > dmalen)
	{
		RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
			"%s: bad rxwi len: DMA len=%d, rxwi len=%d\n",
			device_get_nameunit(sc->dev), dmalen, len);
		return;
	}

	/* get Rx info */

	rxinfo = (struct rt2870_rxinfo *) ((caddr_t) rxwi + dmalen);
	rxinfo_flags = le32toh(rxinfo->flags);

	RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
		"%s: Rx frame: DMA len=%d, len=%d, rxinfo flags=0x%08x\n",
		device_get_nameunit(sc->dev), dmalen, len, rxinfo_flags);
	
	/* check for crc errors */

	if (rxinfo_flags & RT2870_RXINFO_FLAGS_CRC_ERR)
	{
		RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
			"%s: rxinfo: crc error\n",
			device_get_nameunit(sc->dev));

		ifp->if_ierrors++;

		if (!(ifp->if_flags & IFF_PROMISC))
			return;
	}

	wh = (struct ieee80211_frame *) (rxwi + 1);

	/* check for L2 padding between IEEE 802.11 frame header and body */

	if (rxinfo_flags & RT2870_RXINFO_FLAGS_L2PAD)
	{
		RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
			"%s: L2 padding: DMA len=%d, len=%d\n",
			device_get_nameunit(sc->dev), dmalen, len);

		len += 2;
	}

	m = m_getjcl(M_DONTWAIT, MT_DATA, M_PKTHDR, MJUMPAGESIZE);
	if (m == NULL)
	{
		sc->rx_mbuf_alloc_errors++;
		ifp->if_ierrors++;
		return;
	}

	m->m_pkthdr.rcvif = ifp;
	m->m_pkthdr.len = m->m_len = len;

	m_copyback(m, 0, len, (caddr_t) wh);

	wh = mtod(m, struct ieee80211_frame *);

	/* check for cipher errors */

	if (rxinfo_flags & RT2870_RXINFO_FLAGS_DECRYPTED)
	{
		cipher_err = ((rxinfo_flags >> RT2870_RXINFO_FLAGS_CIPHER_ERR_SHIFT) &
			RT2870_RXINFO_FLAGS_CIPHER_ERR_MASK);
		if (cipher_err == RT2870_RXINFO_FLAGS_CIPHER_ERR_NONE)
		{
			if (wh->i_fc[1] & IEEE80211_FC1_WEP)
				wh->i_fc[1] &= ~IEEE80211_FC1_WEP;

			m->m_flags |= M_WEP;

			sc->rx_cipher_no_errors++;
		}
		else
		{
			RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
				"%s: rxinfo: cipher error=0x%02x\n",
				device_get_nameunit(sc->dev), cipher_err);

			if (cipher_err == RT2870_RXINFO_FLAGS_CIPHER_ERR_ICV)
				sc->rx_cipher_icv_errors++;
			else if (cipher_err == RT2870_RXINFO_FLAGS_CIPHER_ERR_MIC)
				sc->rx_cipher_mic_errors++;
			else if (cipher_err == RT2870_RXINFO_FLAGS_CIPHER_ERR_INVALID_KEY)
				sc->rx_cipher_invalid_key_errors++;

			if ((cipher_err == RT2870_RXINFO_FLAGS_CIPHER_ERR_MIC) &&
				(rxinfo_flags & RT2870_RXINFO_FLAGS_MYBSS))
			{
				keyidx = (rxwi->udf_bssidx_keyidx >> RT2870_RXWI_KEYIDX_SHIFT) &
					RT2870_RXWI_KEYIDX_MASK;

				ieee80211_notify_michael_failure(ic, wh, keyidx);
			}

			ifp->if_ierrors++;

			if (!(ifp->if_flags & IFF_PROMISC))
			{
				m_free(m);
				return;
			}
		}
	}
	else
	{
		if (wh->i_fc[1] & IEEE80211_FC1_WEP)
		{
			RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
				"%s: rxinfo: not decrypted but protected flag set\n",
				device_get_nameunit(sc->dev));

			ifp->if_ierrors++;

			if (!(ifp->if_flags & IFF_PROMISC))
			{
				m_free(m);
				return;
			}
		}
	}

	/* check for A-MPDU */

	if (rxinfo_flags & RT2870_RXINFO_FLAGS_BA)
	{
		m->m_flags |= M_AMPDU;

		sc->rx_ampdu++;

		if (wh->i_fc[1] & IEEE80211_FC1_RETRY)
			sc->rx_ampdu_retries++;

		ampdu = 1;
	}
	else
	{
		ampdu = 0;
	}

	/* check for A-MSDU */

	if (rxinfo_flags & RT2870_RXINFO_FLAGS_AMSDU)
	{
		sc->rx_amsdu++;

		amsdu = 1;
	}
	else
	{
		amsdu = 0;
	}

	ant = rt2870_maxrssi_rxpath(sc, rxwi);
	rssi = rxwi->rssi[ant];
	rssi_dbm = rt2870_rssi2dbm(sc, rssi, ant);
	phymode = ((rxwi->phymode_stbc_shortgi >> RT2870_RXWI_PHYMODE_SHIFT) &
		RT2870_RXWI_PHYMODE_MASK);
	bw = ((rxwi->bw_mcs >> RT2870_RXWI_BW_SHIFT) & RT2870_RXWI_BW_MASK);
	shortgi = ((rxwi->phymode_stbc_shortgi >> RT2870_RXWI_SHORTGI_SHIFT) &
		RT2870_RXWI_SHORTGI_MASK);
	stbc = ((rxwi->phymode_stbc_shortgi >> RT2870_RXWI_STBC_SHIFT) &
		RT2870_RXWI_STBC_MASK);
	mcs = ((rxwi->bw_mcs >> RT2870_RXWI_MCS_SHIFT) & RT2870_RXWI_MCS_MASK);
	tid = ((rxwi->tid_size >> RT2870_RXWI_TID_SHIFT) & RT2870_RXWI_TID_MASK);
	seq = ((rxwi->seq_frag >> RT2870_RXWI_SEQ_SHIFT) & RT2870_RXWI_SEQ_MASK);
	frag = ((rxwi->seq_frag >> RT2870_RXWI_FRAG_SHIFT) & RT2870_RXWI_FRAG_MASK);

	if (bpf_peers_present(sc->drvbpf))
	{
		tap = &sc->rxtap;

		tap->flags = IEEE80211_RADIOTAP_F_DATAPAD;
		tap->dbm_antsignal = rssi_dbm;
		tap->dbm_antnoise = RT2870_NOISE_FLOOR;
		tap->antenna = ant;
		tap->antsignal = rssi;
		tap->chan_flags = htole32(ic->ic_curchan->ic_flags);
		tap->chan_freq = htole16(ic->ic_curchan->ic_freq);
		tap->chan_ieee = ic->ic_curchan->ic_ieee;
		tap->chan_maxpow = 0;

		if (phymode == RT2870_RXWI_PHYMODE_CCK || phymode == RT2870_RXWI_PHYMODE_OFDM)
			tap->rate = rt2870_rxrate(rxwi);
		else
			tap->rate = mcs | IEEE80211_RATE_MCS;

		if (rxinfo_flags & RT2870_RXINFO_FLAGS_CRC_ERR)
			tap->flags |= IEEE80211_RADIOTAP_F_BADFCS;

		if (rxinfo_flags & RT2870_RXINFO_FLAGS_FRAG)
			tap->flags |= IEEE80211_RADIOTAP_F_FRAG;

		if (rxwi->bw_mcs & RT2870_RXWI_MCS_SHOTPRE)
			tap->flags |= IEEE80211_RADIOTAP_F_SHORTPRE;

		if (shortgi)
			tap->flags |= IEEE80211_RADIOTAP_F_SHORTGI;

		/* XXX use temporarily radiotap CFP flag as A-MPDU flag */

		if (ampdu)
			tap->flags |= IEEE80211_RADIOTAP_F_CFP;

		bpf_mtap2(sc->drvbpf, tap, sc->rxtap_len, m);
	}

	/*
	 * net80211 assumes that RSSI data are in the range [-127..127] and
	 * in .5 dBm units relative to the current noise floor
	 */

	rssi_dbm_rel = (rssi_dbm - RT2870_NOISE_FLOOR) * 2;
	if (rssi_dbm_rel > 127)
		rssi_dbm_rel = 127;

	RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
		"%s: received frame: len=%d, phymode=%d, bw=%d, shortgi=%d, stbc=0x%02x, mcs=%d, "
		"ant=%d, rssi=%d/%d/%d, snr=%d/%d, wcid=0x%02x, ampdu=%d, amsdu=%d, "
		"tid=%d, seq=%d, frag=%d, retry=%d, rssi_dbm=%d, rssi_dbm_rel=%d\n",
		device_get_nameunit(sc->dev),
		len, phymode, bw, shortgi, stbc, mcs,
		ant, rxwi->rssi[0], rxwi->rssi[1], rxwi->rssi[2],
		rxwi->snr[0], rxwi->snr[1],
		rxwi->wcid, ampdu, amsdu, tid, seq, frag, (wh->i_fc[1] & IEEE80211_FC1_RETRY) ? 1 : 0,
		rssi_dbm, rssi_dbm_rel);

	ni = ieee80211_find_rxnode(ic, (struct ieee80211_frame_min *) wh);

	if (ni != NULL)
	{
		rni = (struct rt2870_softc_node *) ni;

		for (i = 0; i < RT2870_SOFTC_RSSI_DBM_COUNT; i++)
			rni->last_rssi_dbm[i] = rt2870_rssi2dbm(sc, rxwi->rssi[i], i);
	}

	ieee80211_input(ic, m, ni, rssi_dbm, RT2870_NOISE_FLOOR, 0);

	ieee80211_free_node(ni);
}

/*
 * rt2870_tx_frame
 */
static int rt2870_tx_frame(struct rt2870_softc *sc,
	struct mbuf *m, struct ieee80211_node *ni, int qid)
{
	struct ieee80211com *ic;
	struct rt2870_softc_node *rni;
	struct rt2870_softc_tx_ring *ring;
	struct rt2870_softc_tx_data *data;
	struct rt2870_txinfo *txinfo;
	struct rt2870_txwi *txwi;
	struct ieee80211_frame *wh;
	struct ieee80211_tx_ampdu *tx_ampdu;
	struct rt2870_softc_tx_radiotap_header *tap;
	u_int hdrsize, hdrspace;
	uint8_t type, rate, bw, stbc, shortgi, mcs, pid, wcid, mpdu_density, bawin_size;
	uint16_t qos, len, dmalen, mpdu_len, dur;
	int hasqos, ac, ampdu, mimops, ackrate;

	ic = &sc->ic;
	rni = (struct rt2870_softc_node *) ni;

	ring = &sc->tx_ring[qid];
	data = STAILQ_FIRST(&ring->inactive);
	STAILQ_REMOVE_HEAD(&ring->inactive, next);
	txinfo = (struct rt2870_txinfo *) data->buf;
	txwi = (struct rt2870_txwi *) (txinfo + 1);

	wh = mtod(m, struct ieee80211_frame *);

	type = wh->i_fc[0] & IEEE80211_FC0_TYPE_MASK;

	hasqos = IEEE80211_QOS_HAS_SEQ(wh);
	if (hasqos)
	{
		if (IEEE80211_HAS_ADDR4(wh))
			qos = le16toh(*(const uint16_t *)
				(((struct ieee80211_qosframe_addr4 *) wh)->i_qos));
		else
			qos = le16toh(*(const uint16_t *)
				(((struct ieee80211_qosframe *) wh)->i_qos));
	}
	else
	{
		qos = 0;
	}

	if (ni->ni_flags & IEEE80211_NODE_HT)
	{
		if (IEEE80211_IS_MULTICAST(wh->i_addr1) || type != IEEE80211_FC0_TYPE_DATA)
			rate = 0;
		else if (ic->ic_fixed_rate != IEEE80211_FIXED_RATE_NONE)
			rate = ic->ic_fixed_rate;
		else
			rate = ni->ni_htrates.rs_rates[ni->ni_txrate];
	}
	else
	{
		if (IEEE80211_IS_MULTICAST(wh->i_addr1) || type != IEEE80211_FC0_TYPE_DATA)
			rate = IEEE80211_IS_CHAN_5GHZ(ic->ic_curchan) ? 12 : 2;
		else if (ic->ic_fixed_rate != IEEE80211_FIXED_RATE_NONE)
			rate = ic->ic_fixed_rate;
		else
			rate = ni->ni_rates.rs_rates[ni->ni_txrate];
	}

	rate &= IEEE80211_RATE_VAL;

	len = sizeof(struct rt2870_txinfo) + sizeof(struct rt2870_txwi) + m->m_pkthdr.len;

	/* align end on a 4-bytes boundary */

	dmalen = (len + 3) & ~3;

	/* fill Tx info */

	memset(txinfo, 0, sizeof(struct rt2870_txinfo));

	txinfo->len = htole16(dmalen);

	txinfo->qsel_flags = (RT2870_TXINFO_QSEL_EDCA << RT2870_TXINFO_QSEL_SHIFT);

	/* fill Tx wireless info */

	if (ni->ni_flags & IEEE80211_NODE_HT)
		mcs = rate;
	else
		mcs = rt2870_rate2mcs(rate);

	if (type == IEEE80211_FC0_TYPE_DATA)
		wcid = !IEEE80211_IS_MULTICAST(wh->i_addr1) ? rni->staid : RT2870_WCID_MCAST;
	else
		wcid = RT2870_WCID_RESERVED;

	/* calculate MPDU length without padding */

	hdrsize = ieee80211_anyhdrsize(wh);
	hdrspace = ieee80211_anyhdrspace(ic, wh);
	mpdu_len = m->m_pkthdr.len - hdrspace + hdrsize;

	memset(txwi, 0, sizeof(struct rt2870_txwi));

	txwi->wcid = wcid;

	/* MIMO power save */

	if ((ni->ni_flags & IEEE80211_NODE_HT) &&
		((ni->ni_htcap & IEEE80211_HTCAP_SMPS) != IEEE80211_HTCAP_SMPS_OFF))
	{
		if (mcs > 7)
		{
			if ((ni->ni_htcap & IEEE80211_HTCAP_SMPS) == IEEE80211_HTCAP_SMPS_DYNAMIC)
			{
				/* dynamic MIMO power save */

				txwi->mpdu_density_flags |=
					(RT2870_TXWI_FLAGS_MIMOPS << RT2870_TXWI_FLAGS_SHIFT);
			}
			else
			{
				/* static MIMO power save */

				mcs = 7;
			}
		}

		mimops = 1;
	}
	else
	{
		mimops = 0;
	}

	pid = (mcs < 0xf) ? (mcs + 1) : mcs;

	txwi->pid_mpdu_len = ((htole16(pid) & RT2870_TXWI_PID_MASK) <<
		RT2870_TXWI_PID_SHIFT) | ((htole16(mpdu_len) & RT2870_TXWI_MPDU_LEN_MASK) <<
			RT2870_TXWI_MPDU_LEN_SHIFT);

	stbc = ((wh->i_fc[0] & IEEE80211_FC0_TYPE_MASK) == IEEE80211_FC0_TYPE_DATA) &&
		sc->tx_stbc && (mcs <= 7) && (ic->ic_htcaps & IEEE80211_HTCAP_TXSTBC) &&
		(ni->ni_flags & IEEE80211_NODE_HT) && (ni->ni_htcap & IEEE80211_HTCAP_RXSTBC);

	shortgi = ((wh->i_fc[0] & IEEE80211_FC0_TYPE_MASK) == IEEE80211_FC0_TYPE_DATA) &&
		(((ic->ic_flags_ext & IEEE80211_FEXT_SHORTGI20) &&
			(ni->ni_flags & IEEE80211_NODE_HT) && (ni->ni_htcap & IEEE80211_HTCAP_SHORTGI20) &&
			(ni->ni_chw == 20)) ||
		 ((ic->ic_flags_ext & IEEE80211_FEXT_SHORTGI40) &&
			(ni->ni_flags & IEEE80211_NODE_HT) && (ni->ni_htcap & IEEE80211_HTCAP_SHORTGI40) &&
			(ni->ni_chw == 40)));

	txwi->phymode_ifs_stbc_shortgi |=
		((stbc & RT2870_TXWI_STBC_MASK) << RT2870_TXWI_STBC_SHIFT) |
		((shortgi & RT2870_TXWI_SHORTGI_MASK) << RT2870_TXWI_SHORTGI_SHIFT);

	if (ni->ni_flags & IEEE80211_NODE_HT)
	{
		txwi->phymode_ifs_stbc_shortgi |=
			(RT2870_TXWI_PHYMODE_HT_MIXED << RT2870_TXWI_PHYMODE_SHIFT);
	}
	else
	{
		if (!RT2870_RATE_IS_OFDM(rate))
		{
			txwi->phymode_ifs_stbc_shortgi |=
				(RT2870_TXWI_PHYMODE_CCK << RT2870_TXWI_PHYMODE_SHIFT);

			if (rate != 2 && (ic->ic_flags & IEEE80211_F_SHPREAMBLE))
				mcs |= RT2870_TXWI_MCS_SHOTPRE;
		}
		else
		{
			txwi->phymode_ifs_stbc_shortgi |=
				(RT2870_TXWI_PHYMODE_OFDM << RT2870_TXWI_PHYMODE_SHIFT);
		}
	}

	if ((ni->ni_flags & IEEE80211_NODE_HT) && (ni->ni_chw == 40))
		bw = RT2870_TXWI_BW_40;
	else
		bw = RT2870_TXWI_BW_20;

	txwi->bw_mcs = ((bw & RT2870_TXWI_BW_MASK) << RT2870_TXWI_BW_SHIFT) |
		((mcs & RT2870_TXWI_MCS_MASK) << RT2870_TXWI_MCS_SHIFT);

	if (type != IEEE80211_FC0_TYPE_DATA)
		txwi->txop = (RT2870_TXWI_TXOP_BACKOFF << RT2870_TXWI_TXOP_SHIFT);
	else
		txwi->txop = (RT2870_TXWI_TXOP_HT << RT2870_TXWI_TXOP_SHIFT);

	/* skip ACKs for multicast frames */

	if (!IEEE80211_IS_MULTICAST(wh->i_addr1) &&
		(!hasqos || (qos & IEEE80211_QOS_ACKPOLICY) != IEEE80211_QOS_ACKPOLICY_NOACK))
	{
		txwi->bawin_size_xflags |=
			(RT2870_TXWI_XFLAGS_ACK << RT2870_TXWI_XFLAGS_SHIFT);

		if (ni->ni_flags & IEEE80211_NODE_HT)
		{
			/* preamble + plcp + signal extension + SIFS */

			dur = 16 + 4 + 6 + 10;
		}
		else
		{
			ackrate = rt2870_ackrate(ic, rate);

			dur = rt2870_txtime(RT2870_ACK_SIZE, ackrate, ic->ic_flags) +
				sc->sifs;
		}

		*(uint16_t *) wh->i_dur = htole16(dur);
	}

	/* check fo A-MPDU */

	if ((qos & IEEE80211_QOS_ACKPOLICY) == IEEE80211_QOS_ACKPOLICY_BA)
	{
		ac = M_WME_GETAC(m);
		tx_ampdu = &ni->ni_tx_ampdu[ac];

		mpdu_density = RT2870_MS(ni->ni_htparam, IEEE80211_HTCAP_MPDUDENSITY);
		bawin_size = tx_ampdu->txa_wnd;

		txwi->mpdu_density_flags |=
			((mpdu_density & RT2870_TXWI_MPDU_DENSITY_MASK) << RT2870_TXWI_MPDU_DENSITY_SHIFT) |
			(RT2870_TXWI_FLAGS_AMPDU << RT2870_TXWI_FLAGS_SHIFT);

		txwi->bawin_size_xflags |=
			((bawin_size & RT2870_TXWI_BAWIN_SIZE_MASK) << RT2870_TXWI_BAWIN_SIZE_SHIFT);

		if (IEEE80211_HAS_ADDR4(wh))
			((struct ieee80211_qosframe_addr4 *) wh)->i_qos[0] &= ~IEEE80211_QOS_ACKPOLICY;
		else
			((struct ieee80211_qosframe *) wh)->i_qos[0] &= ~IEEE80211_QOS_ACKPOLICY;

		ampdu = 1;
	}
	else
	{
		mpdu_density = 0;
		bawin_size = 0;
		ampdu = 0;
	}

	/* ask MAC to insert timestamp into probe responses */

	if ((wh->i_fc[0] & (IEEE80211_FC0_TYPE_MASK | IEEE80211_FC0_SUBTYPE_MASK)) ==
		(IEEE80211_FC0_TYPE_MGT | IEEE80211_FC0_SUBTYPE_PROBE_RESP))
		txwi->mpdu_density_flags |=
			(RT2870_TXWI_FLAGS_TS << RT2870_TXWI_FLAGS_SHIFT);

	if (bpf_peers_present(sc->drvbpf))
	{
		tap = &sc->txtap;

		tap->flags = IEEE80211_RADIOTAP_F_DATAPAD;
		tap->chan_flags = htole32(ic->ic_curchan->ic_flags);
		tap->chan_freq = htole16(ic->ic_curchan->ic_freq);
		tap->chan_ieee = ic->ic_curchan->ic_ieee;
		tap->chan_maxpow = 0;

		if (ni->ni_flags & IEEE80211_NODE_HT)
			tap->rate = mcs | IEEE80211_RATE_MCS;
		else
			tap->rate = rate;

		if (mcs & RT2870_TXWI_MCS_SHOTPRE)
			tap->flags |= IEEE80211_RADIOTAP_F_SHORTPRE;

		if (shortgi)
			tap->flags |= IEEE80211_RADIOTAP_F_SHORTGI;

		if (wh->i_fc[1] & IEEE80211_FC1_WEP)
			tap->flags |= IEEE80211_RADIOTAP_F_WEP;

		/* XXX use temporarily radiotap CFP flag as A-MPDU flag */

		if (ampdu)
			tap->flags |= IEEE80211_RADIOTAP_F_CFP;

		if (wh->i_fc[1] & IEEE80211_FC1_WEP)
		{
			wh->i_fc[1] &= ~IEEE80211_FC1_WEP;

			bpf_mtap2(sc->drvbpf, tap, sc->txtap_len, m);

			wh->i_fc[1] |= IEEE80211_FC1_WEP;
		}
		else
		{
			bpf_mtap2(sc->drvbpf, tap, sc->txtap_len, m);
		}
	}

	m_copydata(m, 0, m->m_pkthdr.len, (caddr_t) (txwi + 1));

	RT2870_DPRINTF(sc, RT2870_DEBUG_TX,
		"%s: sending frame: qid=%d, hdrsize=%d, hdrspace=%d, len=%d, "
		"bw=%d, stbc=%d, shortgi=%d, mcs=%d, wcid=0x%02x, ampdu=%d (density=%d, winsize=%d), "
		"mimops=%d, DMA len=%d\n",
		device_get_nameunit(sc->dev),
		qid, hdrsize, hdrspace, m->m_pkthdr.len, bw, stbc, shortgi,
		mcs, wcid, ampdu, mpdu_density, bawin_size, mimops, dmalen);

	data->m = m;
	data->ni = ni;

	STAILQ_INSERT_TAIL(&ring->active, data, next);
	ring->queued++;

	usbd_setup_xfer(data->xfer, ring->usb_pipe, ring, data->buf, len,
	    USBD_FORCE_SHORT_XFER | USBD_NO_COPY, RT2870_USB_XFER_TIMEOUT, rt2870_tx_intr);

	usbd_transfer(data->xfer);

	return 0;
}

/*
 * rt2870_tx_raw
 */
static int rt2870_tx_raw(struct rt2870_softc *sc,
	struct mbuf *m, struct ieee80211_node *ni,
	const struct ieee80211_bpf_params *params)
{
	RT2870_DPRINTF(sc, RT2870_DEBUG_TX,
		"%s: Tx raw\n",
		device_get_nameunit(sc->dev));

	return 0;
}

/*
 * rt2870_rx_intr
 */
static void rt2870_rx_intr(usbd_xfer_handle xfer,
	usbd_private_handle priv, usbd_status status)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;
	struct rt2870_softc_rx_ring *ring;
	struct rt2870_softc_rx_data *data;
	int len;

	sc = priv;
	ifp = sc->ifp;
	ring = &sc->rx_ring;

	if (!(ifp->if_drv_flags & IFF_DRV_RUNNING))
		return;

	RT2870_DPRINTF(sc, RT2870_DEBUG_INTR,
		"%s: Rx interrupt: %s\n",
		device_get_nameunit(sc->dev), usbd_errstr(status));

	sc->interrupts++;
	sc->rx_interrupts++;

	switch (status)
	{
		case USBD_NORMAL_COMPLETION:
			data = STAILQ_FIRST(&ring->active);
			if (data == NULL)
				break;

			STAILQ_REMOVE_HEAD(&ring->active, next);

			KASSERT(data->xfer == xfer,
				("%s: Rx interrupt: invalid USB xfer\n",
				 device_get_nameunit(sc->dev)));

			usbd_get_xfer_status(xfer, NULL, NULL, &len, NULL);

			data->len = len;

			STAILQ_INSERT_TAIL(&ring->done, data, next);

			taskqueue_enqueue(sc->taskqueue, &sc->rx_done_task);
		break;

		case USBD_CANCELLED:
		case USBD_NOT_STARTED:
		break;

		default:
			if (status == USBD_STALLED)
				usbd_clear_endpoint_stall_async(ring->usb_pipe);

			data = STAILQ_FIRST(&ring->active);
			if (data != NULL)
			{
				STAILQ_REMOVE_HEAD(&ring->active, next);

				STAILQ_INSERT_TAIL(&ring->active, data, next);

				usbd_setup_xfer(xfer, ring->usb_pipe, sc, data->buf,
					RT2870_USB_RX_BULK_BUFLEN, USBD_SHORT_XFER_OK | USBD_NO_COPY,
					USBD_NO_TIMEOUT, rt2870_rx_intr);

				usbd_transfer(xfer);
			}
		break;
	}
}

/*
 * rt2870_tx_intr
 */
static void rt2870_tx_intr(usbd_xfer_handle xfer,
	usbd_private_handle priv, usbd_status status)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;
	struct rt2870_softc_tx_ring *ring;
	struct rt2870_softc_tx_data *data;

	ring = priv;
	sc = ring->sc;
	ifp = sc->ifp;

	if (!(ifp->if_drv_flags & IFF_DRV_RUNNING))
		return;

	RT2870_DPRINTF(sc, RT2870_DEBUG_INTR,
		"%s: Tx interrupt: %s\n",
		device_get_nameunit(sc->dev), usbd_errstr(status));

	sc->interrupts++;
	sc->tx_interrupts[ring->qid]++;

	switch (status)
	{
		case USBD_NORMAL_COMPLETION:
			data = STAILQ_FIRST(&ring->active);
			if (data == NULL)
				break;

			STAILQ_REMOVE_HEAD(&ring->active, next);

			STAILQ_INSERT_TAIL(&ring->done, data, next);

			sc->tx_qid_pending_mask |= (1 << ring->qid);

			taskqueue_enqueue(sc->taskqueue, &sc->tx_done_task);
		break;

		case USBD_CANCELLED:
		case USBD_NOT_STARTED:
		break;

		default:
			data = STAILQ_FIRST(&ring->active);
			if (data != NULL)
			{
				STAILQ_REMOVE_HEAD(&ring->active, next);

				if (data->m != NULL)
				{
					m_freem(data->m);
					data->m = NULL;
				}

				if (data->ni != NULL)
				{
					ieee80211_free_node(data->ni);
					data->ni = NULL;
				}

				STAILQ_INSERT_TAIL(&ring->inactive, data, next);

				ring->queued--;
			}

			printf("%s: could not transmit buffer: qid=%d, status=%s\n",
		    	device_get_nameunit(sc->dev), ring->qid, usbd_errstr(status));

			ifp->if_oerrors++;
			ifp->if_drv_flags &= ~IFF_DRV_OACTIVE;

			if (status == USBD_STALLED)
				usbd_clear_endpoint_stall_async(ring->usb_pipe);
		break;
	}
}

/*
 * rt2870_rx_done_task
 */
static void rt2870_rx_done_task(void *context, int pending)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;

	sc = context;
	ifp = sc->ifp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
		"%s: Rx done task\n",
		device_get_nameunit(sc->dev));

	if (!(ifp->if_drv_flags & IFF_DRV_RUNNING))
		return;

	if (rt2870_rx_eof(sc, sc->rx_process_limit) != 0)
	{
		RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
			"%s: Rx done task: scheduling again\n",
			device_get_nameunit(sc->dev));

		taskqueue_enqueue(sc->taskqueue, &sc->rx_done_task);
	}
}

/*
 * rt2870_tx_done_task
 */
static void rt2870_tx_done_task(void *context, int pending)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;
	int i;

	sc = context;
	ifp = sc->ifp;

	RT2870_DPRINTF(sc, RT2870_DEBUG_TX,
		"%s: Tx done task\n",
		device_get_nameunit(sc->dev));

	if (!(ifp->if_drv_flags & IFF_DRV_RUNNING))
		return;

	for (i = sc->usb_endpoints - 2; i >= 0; i--)
	{
		if (sc->tx_qid_pending_mask & (1 << i))
		{
			sc->tx_qid_pending_mask &= ~(1 << i);

			rt2870_tx_eof(sc, &sc->tx_ring[i]);
		}
	}

	if (sc->tx_qid_pending_mask != 0)
	{
		RT2870_DPRINTF(sc, RT2870_DEBUG_TX,
			"%s: Tx done task: scheduling again\n",
			device_get_nameunit(sc->dev));

		taskqueue_enqueue(sc->taskqueue, &sc->tx_done_task);
	}

	sc->tx_timer = 0;

	ifp->if_drv_flags &= ~IFF_DRV_OACTIVE;
}

/*
 * rt2870_periodic_task
 */
static void rt2870_periodic_task(void *context, int pending)
{
	struct rt2870_softc *sc;
	struct ifnet *ifp;
	struct ieee80211com *ic;

	sc = context;
	ifp = sc->ifp;
	ic = &sc->ic;

	RT2870_DPRINTF(sc, RT2870_DEBUG_PERIODIC,
		"%s: periodic task: round=%lu\n",
		device_get_nameunit(sc->dev), sc->periodic_round);

	if (!(ifp->if_drv_flags & IFF_DRV_RUNNING))
		return;

	sc->periodic_round++;

	rt2870_update_stats(sc);

	if ((sc->periodic_round % 10) == 0)
	{
		rt2870_bbp_tuning(sc);

		rt2870_update_raw_counters(sc);

		rt2870_watchdog(sc);

		if (ic->ic_opmode == IEEE80211_M_STA)
			rt2870_amrr_update_iter_func(sc, ic->ic_bss);
		else
			ieee80211_iterate_nodes(&ic->ic_sta, rt2870_amrr_update_iter_func, sc);
	}

	callout_reset(&sc->periodic_ch, hz / 10, rt2870_periodic, sc);
}

/*
 * rt2870_cmd_task
 */
static void rt2870_cmd_task(void *context, int pending)
{
	struct rt2870_softc *sc;
	struct rt2870_softc_cmd_ring *ring;
	struct rt2870_softc_cmd *cmd;

	sc = context;
	ring = &sc->cmd_ring;

	while (1)
	{
		if (!(sc->flags & RT2870_SOFTC_FLAGS_VALID))
			break;

		RT2870_SOFTC_LOCK(sc);

		cmd = STAILQ_FIRST(&ring->active);
		if (cmd == NULL)
		{
			RT2870_SOFTC_UNLOCK(sc);
			break;
		}
		
		STAILQ_REMOVE_HEAD(&ring->active, next);

		RT2870_SOFTC_UNLOCK(sc);

		cmd->cb(sc, cmd->data);

		RT2870_SOFTC_LOCK(sc);

		STAILQ_INSERT_TAIL(&ring->inactive, cmd, next);
		ring->queued--;

		RT2870_SOFTC_UNLOCK(sc);
	}
}

/*
 * rt2870_rx_eof
 */
static int rt2870_rx_eof(struct rt2870_softc *sc, int limit)
{
	struct ifnet *ifp;
	struct rt2870_softc_rx_ring *ring;
	struct rt2870_softc_rx_data *data;
	uint32_t dmalen;
	uint8_t *buf;
	int nframes, len;

	ifp = sc->ifp;
	ring = &sc->rx_ring;

	nframes = 0;

	while (limit != 0)
	{
		RT2870_SOFTC_LOCK(sc);

		data = STAILQ_FIRST(&ring->done);
		if (data == NULL)
		{
			RT2870_SOFTC_UNLOCK(sc);
			break;
		}

		STAILQ_REMOVE_HEAD(&ring->done, next);

		RT2870_SOFTC_UNLOCK(sc);

		buf = data->buf;
		len = data->len;

		if (len < RT2870_RX_DESC_SIZE)
		{
			ifp->if_ierrors++;
			goto skip;
		}

		while (len > (sizeof(uint32_t) + sizeof(struct rt2870_rxinfo)))
		{
			dmalen = (le32toh(*(uint32_t *) buf) & 0xffff);

			if (dmalen == 0 || (dmalen & 3) != 0)
			{
				RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
					"%s: bad DMA len=%u\n",
					device_get_nameunit(sc->dev), dmalen);
				goto skip;
			}

			if ((dmalen + sizeof(uint32_t) + sizeof(struct rt2870_rxinfo)) > len)
			{
				RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
					"%s: bad DMA len: DMA len=%u, USB len=%d\n",
			    	device_get_nameunit(sc->dev),
					(unsigned int) (dmalen + sizeof(uint32_t) + sizeof(struct rt2870_rxinfo)), len);
				goto skip;
			}

			nframes++;

			rt2870_rx_frame(sc, buf + sizeof(uint32_t), dmalen);

			buf += (dmalen + sizeof(uint32_t) + sizeof(struct rt2870_rxinfo));
			len -= (dmalen + sizeof(uint32_t) + sizeof(struct rt2870_rxinfo));
		}

skip:

		RT2870_SOFTC_LOCK(sc);

		STAILQ_INSERT_TAIL(&ring->active, data, next);

		RT2870_SOFTC_UNLOCK(sc);

		usbd_setup_xfer(data->xfer, ring->usb_pipe, sc, data->buf,
			RT2870_USB_RX_BULK_BUFLEN, USBD_SHORT_XFER_OK | USBD_NO_COPY,
			USBD_NO_TIMEOUT, rt2870_rx_intr);

		usbd_transfer(data->xfer);

		limit--;
	}

	RT2870_DPRINTF(sc, RT2870_DEBUG_RX,
		"%s: Rx eof: nframes=%d\n",
		device_get_nameunit(sc->dev), nframes);

	sc->rx_packets += nframes;

	return (limit == 0);
}

/*
 * rt2870_tx_eof
 */
static void rt2870_tx_eof(struct rt2870_softc *sc,
	struct rt2870_softc_tx_ring *ring)
{
	struct ifnet *ifp;
	struct rt2870_softc_tx_data *data;
	int nframes;

	ifp = sc->ifp;

	nframes = 0;

	while (1)
	{
		RT2870_SOFTC_LOCK(sc);

		data = STAILQ_FIRST(&ring->done);
		if (data == NULL)
		{
			RT2870_SOFTC_UNLOCK(sc);
			break;
		}

		STAILQ_REMOVE_HEAD(&ring->done, next);

		RT2870_SOFTC_UNLOCK(sc);

		rt2870_drain_fifo_stats(sc);

		nframes++;

		if (data->m->m_flags & M_TXCB)
			ieee80211_process_callback(data->ni, data->m, 0);

		m_freem(data->m);

		ieee80211_free_node(data->ni);

		data->m = NULL;
		data->ni = NULL;

		RT2870_SOFTC_LOCK(sc);

		STAILQ_INSERT_TAIL(&ring->inactive, data, next);

		ring->queued--;

		RT2870_SOFTC_UNLOCK(sc);

		ifp->if_opackets++;
	}

	RT2870_DPRINTF(sc, RT2870_DEBUG_TX,
		"%s: Tx eof: qid=%d, nframes=%d\n",
		device_get_nameunit(sc->dev), ring->qid, nframes);
}

/*
 * rt2870_update_stats
 */
static void rt2870_update_stats(struct rt2870_softc *sc)
{
	struct ifnet *ifp;
	struct ieee80211com *ic;
	uint32_t stacnt[3];
	int beacons, noretryok, retryok, failed, underflows, zerolen;

	ifp = sc->ifp;
	ic = &sc->ic;

	RT2870_DPRINTF(sc, RT2870_DEBUG_STATS,
		"%s: update statistic\n",
		device_get_nameunit(sc->dev));

	rt2870_drain_fifo_stats(sc);

	/* read and clear Tx statistic registers */

	rt2870_io_mac_read_multi(sc, RT2870_REG_TX_STA_CNT0,
		stacnt, sizeof(stacnt));

	stacnt[0] = le32toh(stacnt[0]);
	stacnt[1] = le32toh(stacnt[1]);
	stacnt[2] = le32toh(stacnt[2]);

	beacons = stacnt[0] >> 16;
	noretryok = stacnt[1] & 0xffff;
	retryok = stacnt[1] >> 16;
	failed = stacnt[0] & 0xffff;
	underflows = stacnt[2] >> 16;
	zerolen = stacnt[2] & 0xffff;

	RT2870_DPRINTF(sc, RT2870_DEBUG_STATS,
		"%s: update statistic: beacons=%d, noretryok=%d, retryok=%d, failed=%d, underflows=%d, zerolen=%d\n",
		device_get_nameunit(sc->dev),
		beacons, noretryok, retryok, failed, underflows, zerolen);

	ifp->if_oerrors += failed;

	sc->tx_beacons += beacons;
	sc->tx_noretryok += noretryok;
	sc->tx_retryok += retryok;
	sc->tx_failed += failed;
	sc->tx_underflows += underflows;
	sc->tx_zerolen += zerolen;
}

/*
 * rt2870_bbp_tuning
 */
static void rt2870_bbp_tuning(struct rt2870_softc *sc)
{
	struct ieee80211com *ic;
	struct ieee80211_node *ni;
	int chan, group;
	int8_t rssi, old, new;

	/* RT2860C does not support BBP tuning */

	if (sc->mac_rev == 0x28600100)
		return;

	ic = &sc->ic;

	if ((ic->ic_flags & IEEE80211_F_SCAN) ||
		ic->ic_opmode != IEEE80211_M_STA || ic->ic_state != IEEE80211_S_RUN)
		return;

	ni = ic->ic_bss;

	chan = ieee80211_chan2ieee(ic, ni->ni_chan);

	if (chan <= 14)
		group = 0;
	else if (chan <= 64)
		group = 1;
	else if (chan <= 128)
		group = 2;
	else
		group = 3;

	rssi = ieee80211_getrssi(ic);

	if (IEEE80211_IS_CHAN_2GHZ(ni->ni_chan))
	{
		new = 0x2e + sc->lna_gain[group];
	}
	else
	{
		if (!IEEE80211_IS_CHAN_HT40(ni->ni_chan))
			new = 0x32 + sc->lna_gain[group] * 5 / 3;
		else
			new = 0x3a + sc->lna_gain[group] * 5 / 3;
	}

	/* Tune if absolute average RSSI is greater than -80 */

	if (rssi > -80)
		new += 0x10;

	old = rt2870_io_bbp_read(sc, 66);

	if (old != new)
		rt2870_io_bbp_write(sc, 66, new);
}

/*
 * rt2870_watchdog
 */
static void rt2870_watchdog(struct rt2870_softc *sc)
{
	uint32_t tmp;
	int ntries;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_PBF_TXRXQ_PCNT);

	RT2870_DPRINTF(sc, RT2870_DEBUG_WATCHDOG,
		"%s: watchdog: TXRXQ_PCNT=0x%08x\n",
		device_get_nameunit(sc->dev), tmp);

	if (((tmp >> RT2870_REG_TX0Q_PCNT_SHIFT) & RT2870_REG_TX0Q_PCNT_MASK) != 0)
	{
		sc->tx_queue_not_empty[0]++;

		rt2870_io_mac_write(sc, RT2870_REG_PBF_CFG, 0xf40012);

		for (ntries = 0; ntries < 10; ntries++)
		{
			tmp = rt2870_io_mac_read(sc, RT2870_REG_PBF_TXRXQ_PCNT);
			if (((tmp >> RT2870_REG_TX0Q_PCNT_SHIFT) & RT2870_REG_TX0Q_PCNT_MASK) == 0)
				break;

			DELAY(1);
		}

		rt2870_io_mac_write(sc, RT2870_REG_PBF_CFG, 0xf40006);
	}

	if (((tmp >> RT2870_REG_TX1Q_PCNT_SHIFT) & RT2870_REG_TX1Q_PCNT_MASK) != 0)
	{
		sc->tx_queue_not_empty[1]++;

		rt2870_io_mac_write(sc, RT2870_REG_PBF_CFG, 0xf4000a);

		for (ntries = 0; ntries < 10; ntries++)
		{
			tmp = rt2870_io_mac_read(sc, RT2870_REG_PBF_TXRXQ_PCNT);
			if (((tmp >> RT2870_REG_TX1Q_PCNT_SHIFT) & RT2870_REG_TX1Q_PCNT_MASK) == 0)
				break;

			DELAY(1);
		}

		rt2870_io_mac_write(sc, RT2870_REG_PBF_CFG, 0xf40006);
	}
}

/*
 * rt2870_drain_fifo_stats
 */
static void rt2870_drain_fifo_stats(struct rt2870_softc *sc)
{
	struct ifnet *ifp;
	uint32_t stats;
	uint8_t wcid, mcs, pid;
	int ok, agg, retrycnt;

	ifp = sc->ic.ic_ifp;

	/* drain Tx status FIFO (maxsize = 16) */

	while ((sc->flags & RT2870_SOFTC_FLAGS_VALID) &&
		(stats = rt2870_io_mac_read(sc, RT2870_REG_TX_STA_FIFO)) &
			RT2870_REG_TX_STA_FIFO_VALID)
	{
		wcid = (stats >> RT2870_REG_TX_STA_FIFO_WCID_SHIFT) &
			RT2870_REG_TX_STA_FIFO_WCID_MASK;

		/* if no ACK was requested, no feedback is available */

		if (!(stats & RT2870_REG_TX_STA_FIFO_ACK_REQ) || wcid == 0xff)
			continue;

		/* update AMRR statistic */

		ok = (stats & RT2870_REG_TX_STA_FIFO_TX_OK) ? 1 : 0;
		agg = (stats & RT2870_REG_TX_STA_FIFO_AGG) ? 1 : 0;
		mcs = (stats >> RT2870_REG_TX_STA_FIFO_MCS_SHIFT) &
			RT2870_REG_TX_STA_FIFO_MCS_MASK;
		pid = (stats >> RT2870_REG_TX_STA_FIFO_PID_SHIFT) &
			RT2870_REG_TX_STA_FIFO_PID_MASK;
		retrycnt = (mcs < 0xf) ? (pid - mcs - 1) : 0;

		RT2870_DPRINTF(sc, RT2870_DEBUG_STATS,
			"%s: FIFO statistic: wcid=0x%02x, ok=%d, agg=%d, mcs=0x%02x, pid=0x%02x, retrycnt=%d\n",
			device_get_nameunit(sc->dev),
			wcid, ok, agg, mcs, pid, retrycnt);

		rt2870_amrr_tx_complete(&sc->amrr_node[wcid], ok, retrycnt);

		if (!ok)
			ifp->if_oerrors++;
	}
}

/*
 * rt2870_update_raw_counters
 */
static void rt2870_update_raw_counters(struct rt2870_softc *sc)
{
	uint32_t tmp;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_AGG_CNT);

	sc->tx_nonagg += tmp & 0xffff;
	sc->tx_agg += tmp >> 16;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_AGG_CNT0);

	sc->tx_ampdu += (tmp & 0xffff) / 1 + (tmp >> 16) / 2;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_AGG_CNT1);

	sc->tx_ampdu += (tmp & 0xffff) / 3 + (tmp >> 16) / 4;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_AGG_CNT2);

	sc->tx_ampdu += (tmp & 0xffff) / 5 + (tmp >> 16) / 6;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_AGG_CNT3);

	sc->tx_ampdu += (tmp & 0xffff) / 7 + (tmp >> 16) / 8;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_AGG_CNT4);

	sc->tx_ampdu += (tmp & 0xffff) / 9 + (tmp >> 16) / 10;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_AGG_CNT5);

	sc->tx_ampdu += (tmp & 0xffff) / 11 + (tmp >> 16) / 12;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_AGG_CNT6);

	sc->tx_ampdu += (tmp & 0xffff) / 13 + (tmp >> 16) / 14;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TX_AGG_CNT7);

	sc->tx_ampdu += (tmp & 0xffff) / 15 + (tmp >> 16) / 16;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_RX_STA_CNT0);

	sc->rx_crc_errors += tmp & 0xffff;
	sc->rx_phy_errors += tmp >> 16;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_RX_STA_CNT1);

	sc->rx_false_ccas += tmp & 0xffff;
	sc->rx_plcp_errors += tmp >> 16;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_RX_STA_CNT2);

	sc->rx_dup_packets += tmp & 0xffff;
	sc->rx_fifo_overflows += tmp >> 16;

	tmp = rt2870_io_mac_read(sc, RT2870_REG_TXRX_MPDU_DEN_CNT);

	sc->tx_mpdu_zero_density += tmp & 0xffff;
	sc->rx_mpdu_zero_density += tmp >> 16;
}

/*
 * rt2870_alloc_rx_ring
 */
static int rt2870_alloc_rx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_rx_ring *ring)
{
	struct rt2870_softc_rx_data *data;
	int i, error;

	STAILQ_INIT(&ring->active);
	STAILQ_INIT(&ring->done);

	for (i = 0; i < RT2870_SOFTC_RX_RING_DATA_COUNT; i++)
	{
		data = &ring->data[i];

		data->xfer = usbd_alloc_xfer(sc->usb_dev);
		if (data->xfer == NULL)
		{
			printf("%s: could not allocate Rx xfer\n",
				device_get_nameunit(sc->dev));
			error = ENOMEM;
			goto fail;
		}

		data->buf = usbd_alloc_buffer(data->xfer, RT2870_USB_RX_BULK_BUFLEN);
		if (data->buf == NULL)
		{
			printf("%s: could not allocate Rx buffer\n",
				device_get_nameunit(sc->dev));
			error = ENOMEM;
			goto fail;
		}
	}

	return 0;

fail:
	
	rt2870_free_rx_ring(sc, ring);

	return error;
}

/*
 * rt2870_reset_rx_ring
 */
static void rt2870_reset_rx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_rx_ring *ring)
{
	STAILQ_INIT(&ring->active);
	STAILQ_INIT(&ring->done);
}

/*
 * rt2870_free_rx_ring
 */
static void rt2870_free_rx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_rx_ring *ring)
{
	struct rt2870_softc_rx_data *data;
	int i;

	for (i = 0; i < RT2870_SOFTC_RX_RING_DATA_COUNT; i++)
	{
		data = &ring->data[i];

		if ((data->buf != NULL) && (data->xfer != NULL))
		{
			usbd_free_buffer(data->xfer);
			data->buf = NULL;
		}

		if (data->xfer != NULL)
		{
			usbd_free_xfer(data->xfer);
			data->xfer = NULL;
		}
	}
}

/*
 * rt2870_alloc_tx_ring
 */
static int rt2870_alloc_tx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_tx_ring *ring, int qid)
{
	struct rt2870_softc_tx_data *data;
	int i, error;

	STAILQ_INIT(&ring->inactive);
	STAILQ_INIT(&ring->active);
	STAILQ_INIT(&ring->done);

	ring->sc = sc;
	ring->queued = 0;
	ring->qid = qid;

	for (i = 0; i < RT2870_SOFTC_TX_RING_DATA_COUNT; i++)
	{
		data = &ring->data[i];

		data->xfer = usbd_alloc_xfer(sc->usb_dev);
		if (data->xfer == NULL)
		{
			printf("%s: could not allocate Tx xfer\n",
			    device_get_nameunit(sc->dev));
			error = ENOMEM;
			goto fail;
		}

		data->buf = usbd_alloc_buffer(data->xfer,
			RT2870_TX_DESC_SIZE + MJUMPAGESIZE);
		if (data->buf == NULL)
		{
			printf("%s: could not allocate Tx buffer\n",
			    device_get_nameunit(sc->dev));
			error = ENOMEM;
			goto fail;
		}

		memset(data->buf, 0, RT2870_TX_DESC_SIZE);

		STAILQ_INSERT_TAIL(&ring->inactive, data, next);
	}

	return 0;

fail:
	
	rt2870_free_tx_ring(sc, ring);

	return error;
}

/*
 * rt2870_reset_tx_ring
 */
static void rt2870_reset_tx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_tx_ring *ring)
{
	struct rt2870_softc_tx_data *data;
	int i;

	STAILQ_INIT(&ring->inactive);
	STAILQ_INIT(&ring->active);
	STAILQ_INIT(&ring->done);

	ring->queued = 0;

	for (i = 0; i < RT2870_SOFTC_TX_RING_DATA_COUNT; i++)
	{
		data = &ring->data[i];

		if (data->m != NULL)
		{
			m_free(data->m);
			data->m = NULL;
		}

		if (data->ni != NULL)
		{
			ieee80211_free_node(data->ni);
			data->ni = NULL;
		}

		STAILQ_INSERT_TAIL(&ring->inactive, data, next);
	}
}

/*
 * rt2870_free_tx_ring
 */
static void rt2870_free_tx_ring(struct rt2870_softc *sc,
	struct rt2870_softc_tx_ring *ring)
{
	struct rt2870_softc_tx_data *data;
	int i;

	for (i = 0; i < RT2870_SOFTC_TX_RING_DATA_COUNT; i++)
	{
		data = &ring->data[i];

		if (data->xfer != NULL)
		{
			usbd_free_xfer(data->xfer);
			data->xfer = NULL;
		}

		if (data->m != NULL)
		{
			m_free(data->m);
			data->m = NULL;
		}

		if (data->ni != NULL)
		{
			ieee80211_free_node(data->ni);
			data->ni = NULL;
		}
	}
}

/*
 * rt2870_reset_cmd_ring
 */
static void rt2870_reset_cmd_ring(struct rt2870_softc *sc,
	struct rt2870_softc_cmd_ring *ring)
{
	struct rt2870_softc_cmd *cmd;
	int i;

	STAILQ_INIT(&ring->inactive);
	STAILQ_INIT(&ring->active);

	ring->queued = 0;

	for (i = 0; i < RT2870_SOFTC_CMD_RING_CMD_COUNT; i++)
	{
		cmd = &ring->cmd[i];

		STAILQ_INSERT_TAIL(&ring->inactive, cmd, next);
	}
}

/*
 * rt2870_sysctl_attach
 */
static void rt2870_sysctl_attach(struct rt2870_softc *sc)
{
	struct sysctl_ctx_list *ctx;
	struct sysctl_oid *tree;
	struct sysctl_oid *stats;

	ctx = device_get_sysctl_ctx(sc->dev);
	tree = device_get_sysctl_tree(sc->dev);

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(tree), OID_AUTO,
		"tx_stbc", CTLFLAG_RW, &sc->tx_stbc, 0,
		"Tx STBC");

	/* statistic counters */

	stats = SYSCTL_ADD_NODE(ctx, SYSCTL_CHILDREN(tree), OID_AUTO,
		"stats", CTLFLAG_RD, 0, "statistic");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"interrupts", CTLFLAG_RD, &sc->interrupts, 0,
		"all interrupts");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_interrupts", CTLFLAG_RD, &sc->rx_interrupts, 0,
		"Rx interrupts");

	if (sc->usb_endpoints == (RT2870_SOFTC_TX_RING_COUNT + 1))
	{
		SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
			"tx_mgmt_interrupts", CTLFLAG_RD, &sc->tx_interrupts[5], 0,
			"Tx MGMT interrupts");

		SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
			"tx_hcca_interrupts", CTLFLAG_RD, &sc->tx_interrupts[4], 0,
			"Tx HCCA interrupts");
	}

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac3_interrupts", CTLFLAG_RD, &sc->tx_interrupts[3], 0,
		"Tx AC3 interrupts");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac2_interrupts", CTLFLAG_RD, &sc->tx_interrupts[2], 0,
		"Tx AC2 interrupts");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac1_interrupts", CTLFLAG_RD, &sc->tx_interrupts[1], 0,
		"Tx AC1 interrupts");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac0_interrupts", CTLFLAG_RD, &sc->tx_interrupts[0], 0,
		"Tx AC0 interrupts");

	if (sc->usb_endpoints == (RT2870_SOFTC_TX_RING_COUNT + 1))
	{
		SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
			"tx_mgmt_data_queued", CTLFLAG_RD, &sc->tx_ring[5].queued, 0,
			"Tx MGMT data queued");

		SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
			"tx_hcca_data_queued", CTLFLAG_RD, &sc->tx_ring[4].queued, 0,
			"Tx HCCA data queued");
	}

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac3_data_queued", CTLFLAG_RD, &sc->tx_ring[3].queued, 0,
		"Tx AC3 data queued");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac2_data_queued", CTLFLAG_RD, &sc->tx_ring[2].queued, 0,
		"Tx AC2 data queued");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac1_data_queued", CTLFLAG_RD, &sc->tx_ring[1].queued, 0,
		"Tx AC1 data queued");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac0_data_queued", CTLFLAG_RD, &sc->tx_ring[0].queued, 0,
		"Tx AC0 data queued");

	if (sc->usb_endpoints == (RT2870_SOFTC_TX_RING_COUNT + 1))
	{
		SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
			"tx_mgmt_data_queue_full", CTLFLAG_RD, &sc->tx_data_queue_full[5], 0,
			"Tx MGMT data queue full");

		SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
			"tx_hcca_data_queue_full", CTLFLAG_RD, &sc->tx_data_queue_full[4], 0,
			"Tx HCCA data queue full");
	}

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac3_data_queue_full", CTLFLAG_RD, &sc->tx_data_queue_full[3], 0,
		"Tx AC3 data queue full");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac2_data_queue_full", CTLFLAG_RD, &sc->tx_data_queue_full[2], 0,
		"Tx AC2 data queue full");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac1_data_queue_full", CTLFLAG_RD, &sc->tx_data_queue_full[1], 0,
		"Tx AC1 data queue full");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ac0_data_queue_full", CTLFLAG_RD, &sc->tx_data_queue_full[0], 0,
		"Tx AC0 data queue full");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_watchdog_timeouts", CTLFLAG_RD, &sc->tx_watchdog_timeouts, 0,
		"Tx watchdog timeouts");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_mbuf_alloc_errors", CTLFLAG_RD, &sc->rx_mbuf_alloc_errors, 0,
		"Rx mbuf allocation errors");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_queue_0_not_empty", CTLFLAG_RD, &sc->tx_queue_not_empty[0], 0,
		"Tx queue 0 not empty");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_queue_1_not_empty", CTLFLAG_RD, &sc->tx_queue_not_empty[1], 0,
		"Tx queue 1 not empty");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_beacons", CTLFLAG_RD, &sc->tx_beacons, 0,
		"Tx beacons");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_noretryok", CTLFLAG_RD, &sc->tx_noretryok, 0,
		"Tx successfull without retries");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_retryok", CTLFLAG_RD, &sc->tx_retryok, 0,
		"Tx successfull with retries");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_failed", CTLFLAG_RD, &sc->tx_failed, 0,
		"Tx failed");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_underflows", CTLFLAG_RD, &sc->tx_underflows, 0,
		"Tx underflows");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_zerolen", CTLFLAG_RD, &sc->tx_zerolen, 0,
		"Tx zero length");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_nonagg", CTLFLAG_RD, &sc->tx_nonagg, 0,
		"Tx non-aggregated");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_agg", CTLFLAG_RD, &sc->tx_agg, 0,
		"Tx aggregated");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ampdu", CTLFLAG_RD, &sc->tx_ampdu, 0,
		"Tx A-MPDU");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_mpdu_zero_density", CTLFLAG_RD, &sc->tx_mpdu_zero_density, 0,
		"Tx MPDU with zero density");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"tx_ampdu_sessions", CTLFLAG_RD, &sc->tx_ampdu_sessions, 0,
		"Tx A-MPDU sessions");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_packets", CTLFLAG_RD, &sc->rx_packets, 0,
		"Rx packets");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_ampdu", CTLFLAG_RD, &sc->rx_ampdu, 0,
		"Rx A-MPDU");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_ampdu_retries", CTLFLAG_RD, &sc->rx_ampdu_retries, 0,
		"Rx A-MPDU retries");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_mpdu_zero_density", CTLFLAG_RD, &sc->rx_mpdu_zero_density, 0,
		"Rx MPDU with zero density");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_ampdu_sessions", CTLFLAG_RD, &sc->rx_ampdu_sessions, 0,
		"Rx A-MPDU sessions");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_amsdu", CTLFLAG_RD, &sc->rx_amsdu, 0,
		"Rx A-MSDU");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_crc_errors", CTLFLAG_RD, &sc->rx_crc_errors, 0,
		"Rx CRC errors");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_phy_errors", CTLFLAG_RD, &sc->rx_phy_errors, 0,
		"Rx PHY errors");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_false_ccas", CTLFLAG_RD, &sc->rx_false_ccas, 0,
		"Rx false CCAs");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_plcp_errors", CTLFLAG_RD, &sc->rx_plcp_errors, 0,
		"Rx PLCP errors");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_dup_packets", CTLFLAG_RD, &sc->rx_dup_packets, 0,
		"Rx duplicate packets");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_fifo_overflows", CTLFLAG_RD, &sc->rx_fifo_overflows, 0,
		"Rx FIFO overflows");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_cipher_no_errors", CTLFLAG_RD, &sc->rx_cipher_no_errors, 0,
		"Rx cipher no errors");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_cipher_icv_errors", CTLFLAG_RD, &sc->rx_cipher_icv_errors, 0,
		"Rx cipher ICV errors");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_cipher_mic_errors", CTLFLAG_RD, &sc->rx_cipher_mic_errors, 0,
		"Rx cipher MIC errors");

	SYSCTL_ADD_INT(ctx, SYSCTL_CHILDREN(stats), OID_AUTO,
		"rx_cipher_invalid_key_errors", CTLFLAG_RD, &sc->rx_cipher_invalid_key_errors, 0,
		"Rx cipher invalid key errors");
}

static device_method_t rt2870_dev_methods[] =
{
	DEVMETHOD(device_probe,	rt2870_probe),
	DEVMETHOD(device_attach, rt2870_attach),
	DEVMETHOD(device_detach, rt2870_detach),
	{ 0, 0 }
};

static driver_t rt2870_driver =
{
	"rt2870",
	rt2870_dev_methods,
	sizeof(struct rt2870_softc),
};

static devclass_t rt2870_dev_class;

DRIVER_MODULE(rt2870, uhub, rt2870_driver, rt2870_dev_class,
	usbd_driver_load, 0);

MODULE_DEPEND(rt2870, usb, 1, 1, 1);
MODULE_DEPEND(rt2870, wlan, 1, 1, 1);
