
/*-
 * Copyright (c) 2009-2010 Alexander Egorenkov <egorenar@gmail.com>
 * Copyright (c) 2009 Damien Bergamini <damien.bergamini@free.fr>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _RT2870_SOFTC_H_
#define _RT2870_SOFTC_H_

#include <sys/param.h>
#include <sys/sysctl.h>
#include <sys/sockio.h>
#include <sys/mbuf.h>
#include <sys/kernel.h>
#include <sys/socket.h>
#include <sys/systm.h>
#include <sys/malloc.h>
#include <sys/taskqueue.h>
#include <sys/queue.h>
#include <sys/module.h>
#include <sys/bus.h>
#include <sys/endian.h>

#include <machine/bus.h>
#include <machine/resource.h>
#include <sys/rman.h>

#include <net/bpf.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <net/ethernet.h>
#include <net/if_dl.h>
#include <net/if_media.h>
#include <net/if_types.h>

#include <net80211/ieee80211_var.h>
#include <net80211/ieee80211_input.h>
#include <net80211/ieee80211_radiotap.h>
#include <net80211/ieee80211_regdomain.h>

#include <dev/usb/usb.h>
#include <dev/usb/usbdi.h>
#include <dev/usb/usbdi_util.h>
#include "usbdevs.h"

#include "rt2870_txwi.h"
#include "rt2870_amrr.h"

#define RT2870_SOFTC_LOCK(sc)								mtx_lock(&Giant)
#define RT2870_SOFTC_UNLOCK(sc)								mtx_unlock(&Giant)

#define RT2870_SOFTC_FLAGS_VALID							(1 << 0)
#define RT2870_SOFTC_FLAGS_UCODE_LOADED						(1 << 1)

#define RT2870_SOFTC_LED_OFF_COUNT							3

#define RT2870_SOFTC_RSSI_OFF_COUNT							3

#define RT2870_SOFTC_LNA_GAIN_COUNT							4

#define RT2870_SOFTC_TXPOW_COUNT							50

#define RT2870_SOFTC_TXPOW_RATE_COUNT						5

#define RT2870_SOFTC_TSSI_COUNT								9

#define RT2870_SOFTC_BBP_EEPROM_COUNT						8

#define RT2870_SOFTC_RSSI_DBM_COUNT							3

#define RT2870_SOFTC_STAID_COUNT							64

#define RT2870_SOFTC_TX_RING_COUNT							6

#define RT2870_SOFTC_RX_RING_DATA_COUNT						1

#define RT2870_SOFTC_TX_RING_DATA_COUNT						256

#define RT2870_SOFTC_CMD_DATA_LEN							256
#define RT2870_SOFTC_CMD_RING_CMD_COUNT						64

#define RT2870_SOFTC_RX_RADIOTAP_PRESENT					\
	((1 << IEEE80211_RADIOTAP_FLAGS) |						\
	 (1 << IEEE80211_RADIOTAP_RATE) |						\
	 (1 << IEEE80211_RADIOTAP_DBM_ANTSIGNAL) |				\
	 (1 << IEEE80211_RADIOTAP_DBM_ANTNOISE) |				\
	 (1 << IEEE80211_RADIOTAP_ANTENNA) |					\
	 (1 << IEEE80211_RADIOTAP_DB_ANTSIGNAL) |				\
	 (1 << IEEE80211_RADIOTAP_XCHANNEL))

#define RT2870_SOFTC_TX_RADIOTAP_PRESENT					\
	((1 << IEEE80211_RADIOTAP_FLAGS) |						\
	 (1 << IEEE80211_RADIOTAP_RATE) |						\
	 (1 << IEEE80211_RADIOTAP_XCHANNEL))


struct rt2870_softc_rx_data
{
	STAILQ_ENTRY(rt2870_softc_rx_data) next;
	usbd_xfer_handle xfer;
	uint8_t	*buf;
	uint16_t len;
};

typedef STAILQ_HEAD(, rt2870_softc_rx_data) rt2870_softc_rx_data_queue;

struct rt2870_softc_rx_ring
{
	int usb_ep;
	usbd_pipe_handle usb_pipe;
	struct rt2870_softc_rx_data data[RT2870_SOFTC_RX_RING_DATA_COUNT];
	rt2870_softc_rx_data_queue active;
	rt2870_softc_rx_data_queue done;
};

struct rt2870_softc_tx_data
{
	STAILQ_ENTRY(rt2870_softc_tx_data) next;
	struct ieee80211_node *ni;
	usbd_xfer_handle xfer;
	uint8_t	*buf;
	struct mbuf *m;
};

typedef STAILQ_HEAD(, rt2870_softc_tx_data) rt2870_softc_tx_data_queue;

struct rt2870_softc_tx_ring
{
	int usb_ep;
	usbd_pipe_handle usb_pipe;
	struct rt2870_softc *sc;
	struct rt2870_softc_tx_data data[RT2870_SOFTC_TX_RING_DATA_COUNT];
	rt2870_softc_tx_data_queue inactive;
	rt2870_softc_tx_data_queue active;
	rt2870_softc_tx_data_queue done;
	int queued;
	int qid;
};

struct rt2870_softc_cmd
{
	STAILQ_ENTRY(rt2870_softc_cmd) next;
	void (*cb)(struct rt2870_softc *sc, void *arg);
	uint8_t	data[RT2870_SOFTC_CMD_DATA_LEN];
};

typedef STAILQ_HEAD(, rt2870_softc_cmd) rt2870_softc_cmd_queue;

struct rt2870_softc_cmd_ring
{
	struct rt2870_softc_cmd cmd[RT2870_SOFTC_CMD_RING_CMD_COUNT];
	rt2870_softc_cmd_queue inactive;
	rt2870_softc_cmd_queue active;
	int	queued;
};

struct rt2870_softc_node
{
	struct ieee80211_node ni;

	uint8_t staid;

	int8_t last_rssi_dbm[RT2870_SOFTC_RSSI_DBM_COUNT];
};

struct rt2870_softc_rx_radiotap_header
{
	struct ieee80211_radiotap_header ihdr;
	uint8_t	flags;
	uint8_t	rate;
	int8_t dbm_antsignal;
	int8_t dbm_antnoise;
	uint8_t	antenna;
	uint8_t	antsignal;
	uint32_t chan_flags;
	uint16_t chan_freq;
	uint8_t chan_ieee;
	int8_t chan_maxpow;
};

struct rt2870_softc_tx_radiotap_header
{
	struct ieee80211_radiotap_header ihdr;
	uint8_t flags;
	uint8_t	rate;
	uint32_t chan_flags;
	uint16_t chan_freq;
	uint8_t chan_ieee;
	int8_t chan_maxpow;
};

struct rt2870_softc
{
	uint32_t flags;

	device_t dev;

	int usb_endpoints;
	usbd_device_handle usb_dev;
	usbd_interface_handle usb_iface;

	struct ifnet *ifp;
	int if_flags;

	struct ieee80211com ic;

	struct ieee80211_beacon_offsets	beacon_offsets;
	struct mbuf *beacon_mbuf;
	struct rt2870_txwi beacon_txwi;

	struct rt2870_amrr amrr;
	struct rt2870_amrr_node amrr_node[RT2870_SOFTC_STAID_COUNT];

	int (*newstate)(struct ieee80211com *ic,
		enum ieee80211_state nstate, int arg);

	void (*node_cleanup)(struct ieee80211_node *ni);

	void (*recv_action)(struct ieee80211_node *ni,
		const uint8_t *frm, const uint8_t *efrm);

	int (*send_action)(struct ieee80211_node *ni,
		int category, int action, uint16_t args[4]);

	int (*addba_response)(struct ieee80211_node *ni,
		struct ieee80211_tx_ampdu *tap,
		int status, int baparamset, int batimeout);

	void (*addba_stop)(struct ieee80211_node *ni,
		struct ieee80211_tx_ampdu *tap);

	uint32_t mac_rev;
	uint16_t eeprom_rev;
	uint8_t rf_rev;

	uint8_t mac_addr[IEEE80211_ADDR_LEN];

	uint8_t ntxpath;
	uint8_t nrxpath;

	int hw_radio_cntl;
	int tx_agc_cntl;
	int ext_lna_2ghz;
	int ext_lna_5ghz;

	uint8_t country_2ghz;
	uint8_t country_5ghz;

	uint8_t rf_freq_off;

	uint8_t led_cntl;
	uint16_t led_off[RT2870_SOFTC_LED_OFF_COUNT];

	int8_t rssi_off_2ghz[RT2870_SOFTC_RSSI_OFF_COUNT];
	int8_t rssi_off_5ghz[RT2870_SOFTC_RSSI_OFF_COUNT];

	int8_t lna_gain[RT2870_SOFTC_LNA_GAIN_COUNT];

	int8_t txpow1[RT2870_SOFTC_TXPOW_COUNT];
	int8_t txpow2[RT2870_SOFTC_TXPOW_COUNT];

	int8_t txpow_rate_delta_2ghz;
	int8_t txpow_rate_delta_5ghz;
	uint32_t txpow_rate_20mhz[RT2870_SOFTC_TXPOW_RATE_COUNT];
	uint32_t txpow_rate_40mhz_2ghz[RT2870_SOFTC_TXPOW_RATE_COUNT];
	uint32_t txpow_rate_40mhz_5ghz[RT2870_SOFTC_TXPOW_RATE_COUNT];

	int tx_agc_cntl_2ghz;
	int tx_agc_cntl_5ghz;

	uint8_t tssi_2ghz[RT2870_SOFTC_TSSI_COUNT];
	uint8_t tssi_step_2ghz;
	uint8_t tssi_5ghz[RT2870_SOFTC_TSSI_COUNT];
	uint8_t tssi_step_5ghz;

	struct
	{
		uint8_t	val;
		uint8_t	reg;
	} __packed bbp_eeprom[RT2870_SOFTC_BBP_EEPROM_COUNT];

	int sifs;

	uint8_t staid_mask[RT2870_SOFTC_STAID_COUNT / NBBY];

	struct task rx_done_task;
	int rx_process_limit;

	struct task tx_done_task;
	uint32_t tx_qid_pending_mask;

	struct task periodic_task;
	struct callout periodic_ch;
	unsigned long periodic_round;

	struct task cmd_task;

	struct taskqueue *taskqueue;

	struct rt2870_softc_rx_ring rx_ring;

	struct rt2870_softc_tx_ring tx_ring[RT2870_SOFTC_TX_RING_COUNT];
	int tx_ring_mgtqid;

	struct callout tx_watchdog_ch;
	int tx_timer;

	struct rt2870_softc_cmd_ring cmd_ring;

	struct bpf_if *drvbpf;

	union
	{
		struct rt2870_softc_rx_radiotap_header th;
		uint8_t	pad[64];
	} rxtapu;

#define rxtap	rxtapu.th

	int	rxtap_len;

	union
	{
		struct rt2870_softc_tx_radiotap_header th;
		uint8_t	pad[64];
	} txtapu;

#define txtap	txtapu.th

	int	txtap_len;

	/* statistic counters */

	unsigned long interrupts;
	unsigned long rx_interrupts;
	unsigned long tx_interrupts[RT2870_SOFTC_TX_RING_COUNT];

	unsigned long tx_data_queue_full[RT2870_SOFTC_TX_RING_COUNT];

	unsigned long tx_watchdog_timeouts;

	unsigned long rx_mbuf_alloc_errors;

	unsigned long tx_queue_not_empty[2];

	unsigned long tx_beacons;
	unsigned long tx_noretryok;
	unsigned long tx_retryok;
	unsigned long tx_failed;
	unsigned long tx_underflows;
	unsigned long tx_zerolen;
	unsigned long tx_nonagg;
	unsigned long tx_agg;
	unsigned long tx_ampdu;
	unsigned long tx_mpdu_zero_density;
	unsigned long tx_ampdu_sessions;

	unsigned long rx_packets;
	unsigned long rx_ampdu;
	unsigned long rx_ampdu_retries;
	unsigned long rx_mpdu_zero_density;
	unsigned long rx_ampdu_sessions;
	unsigned long rx_amsdu;
	unsigned long rx_crc_errors;
	unsigned long rx_phy_errors;
	unsigned long rx_false_ccas;
	unsigned long rx_plcp_errors;
	unsigned long rx_dup_packets;
	unsigned long rx_fifo_overflows;
	unsigned long rx_cipher_no_errors;
	unsigned long rx_cipher_icv_errors;
	unsigned long rx_cipher_mic_errors;
	unsigned long rx_cipher_invalid_key_errors;

	int tx_stbc;

#ifdef RT2870_DEBUG
	int debug;
#endif
};

#endif /* #ifndef _RT2870_SOFTC_H_ */
