
/*-
 * Copyright (c) 2009-2010 Alexander Egorenkov <egorenar@gmail.com>
 * Copyright (c) 2009 Damien Bergamini <damien.bergamini@free.fr>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _RT2870_TXINFO_H_
#define _RT2870_TXINFO_H_

#define RT2870_TXINFO_FLAGS_SHIFT						0
#define RT2870_TXINFO_FLAGS_MASK						0xf9
#define RT2870_TXINFO_FLAGS_USB_DMA_BURST				(1 << 7)
#define RT2870_TXINFO_FLAGS_USB_DMA_NEXT_VALID			(1 << 6)
#define RT2870_TXINFO_FLAGS_USE_LAST_ROUND				(1 << 3)
#define RT2870_TXINFO_FLAGS_WI_VALID					(1 << 0)

#define RT2870_TXINFO_QSEL_SHIFT						1
#define RT2870_TXINFO_QSEL_MASK							0x3
#define RT2870_TXINFO_QSEL_MGMT							0
#define RT2870_TXINFO_QSEL_HCCA							1
#define RT2870_TXINFO_QSEL_EDCA							2

struct rt2870_txinfo
{
	uint16_t len;
	uint8_t reserved;
	uint8_t qsel_flags;
} __packed;

#endif /* #ifndef _RT2870_TXINFO_H_ */
